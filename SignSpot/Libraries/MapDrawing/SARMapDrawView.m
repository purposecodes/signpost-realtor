//
//  SARMapDrawView.m
//  SARMapDrawView
//
//  Created by Saravanan on 03/05/15.
//  Copyright (c) 2015 Saravanan. All rights reserved.
//

#import "SARMapDrawView.h"

@interface SARMapDrawView (){
    BOOL isInitiallyLoaded;//Used to draw polygons from database at its initial launch
    
        NSMutableArray *arrClickedPoints;
     NSMutableArray *arrMarkers;
       GMSPolyline *polyline;
}

@property(nonatomic,strong)SARMapDrawView *mapDrawView;

@end

@implementation SARMapDrawView
@synthesize mapView = mapView;

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initialize];
}

-(void)initialize{
    
    
    arrClickedPoints = [NSMutableArray new];
    arrMarkers =  [NSMutableArray new];
    isInitiallyLoaded = YES;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:13.08
                                                            longitude:80.20
                                                                 zoom:18];//Hardcoding to some Location in Chennai
    
    if (!mapView)
        mapView = [GMSMapView mapWithFrame:self.bounds camera:camera];
    
    //mapView.camera = camera;
    mapView.delegate = self;
   // mapView.mapType = kGMSTypeHybrid;
    mapView.myLocationEnabled = NO;
    mapView.settings.myLocationButton = NO;
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    gesture.numberOfTapsRequired = 1;
    [self addGestureRecognizer:gesture];
    
    
}

-(void)resetPoints{
    
    for (GMSMarker *marker in arrMarkers)
        marker.map = nil;
    [arrClickedPoints removeAllObjects];
    [arrMarkers removeAllObjects];
}
-(void)tapped:(UITapGestureRecognizer*)recognizer{
    
    if (!self.isDrawingPolygon)return;
    if (!polyline) polyline = [GMSPolyline polylineWithPath:nil];
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D coordinate = [self.mapView.projection coordinateForPoint:point];
    [arrClickedPoints addObject: [[SARCoordinate alloc] initWithCoordinate:coordinate]];
    [self handleCoordinateForShape];
    [self addImageOnClickedPoints:coordinate];
  
}

-(void)addImageOnClickedPoints:(CLLocationCoordinate2D)location{
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.position = location;
    marker.icon = [UIImage imageNamed:@"DrawPolygonCorner.png"];
    if (arrMarkers.count > 0) marker.icon = [UIImage imageNamed:@"PloygonClearCorner.png"];
    
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    marker.map = mapView;
    marker.tappable = false;
    [arrMarkers addObject:marker];
}

-(void)handleCoordinateForShape{

    NSInteger numberOfPoints = [arrClickedPoints count];
    GMSMutablePath *path = [GMSMutablePath path];
    CLLocationCoordinate2D points[numberOfPoints];
    for (NSInteger i = 0; i < numberOfPoints; i++){
        SARCoordinate *coordinateObject = arrClickedPoints[i];
        points[i] = coordinateObject.coordinate;
        [path addCoordinate:coordinateObject.coordinate];
    }
    polyline.path = path;
    polyline.strokeColor = [UIColor colorWithRed:255/255.f green:0/255.f blue:18/255.f alpha:1];
    polyline.strokeWidth = 4;
    polyline.tappable = YES;
    polyline.map = mapView;
    [polyline setTappable:NO];
}


#pragma mark - Other Methods
-(void)enableDrawing{
    self.isDrawingPolygon = YES;
    self.disableInteraction = YES;
    [arrClickedPoints removeAllObjects];
}

-(void)disableDrawing{
    
    [self completeDrawing];
    self.isDrawingPolygon = NO;
    self.disableInteraction = NO;
    self.coordinates = nil;
    if (self.polygonDrawnBlock) {
        self.polygonDrawnBlock(polyline);
    }
    
}
-(void)completeDrawing{
    
    if (arrClickedPoints.count > 0) {
        SARCoordinate *coordinateObject = arrClickedPoints[0];
        [arrClickedPoints addObject: [[SARCoordinate alloc] initWithCoordinate:coordinateObject.coordinate]];
        [self handleCoordinateForShape];
    }
}

-(void)changeUserInteraction{
    if (self.disableInteraction) {
        self.mapView.userInteractionEnabled = NO;
    }
    else{
        self.mapView.userInteractionEnabled = YES;
    }
}


#pragma mark - Animation Methods
-(void)scrollMapUpToValue:(CGFloat)yValue{
    
    [mapView animateWithCameraUpdate:[GMSCameraUpdate scrollByX:0 Y:yValue]];
}

-(void)scrollMapDownToValue:(CGFloat)yValue{
    [mapView animateWithCameraUpdate:[GMSCameraUpdate scrollByX:0 Y:-yValue]];
}

#pragma mark - GMSMapView Delegates
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    if (self.MapViewIdleAtCameraPositionBlock) {
        self.MapViewIdleAtCameraPositionBlock(position);
    }
    
}

#pragma mark - Setters
-(void)setDisableInteraction:(BOOL)disableInteraction{
    _disableInteraction = disableInteraction;
    [self changeUserInteraction];
    if (disableInteraction == NO) {
        if (self.ViewEnabledBlock) {
            self.ViewEnabledBlock();
        }
    }
}

@end
