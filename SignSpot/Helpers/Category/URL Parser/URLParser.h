//
//  URLParser.h
//  SignPost
//
//  Created by Purpose Code on 06/03/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLParser : NSObject {
    NSArray *variables;
}

@property (nonatomic, retain) NSArray *variables;

- (id)initWithURLString:(NSString *)url;
- (NSString *)valueForVariable:(NSString *)varName;

@end
