

#import "Constants.h"


NSString * const StoryboardForSlider = @"StoryBoardForSlider";
NSString * const StoryboardForLogin = @"Login&RegisterStoryBoard";
NSString * const ProductDetailsStoryBoard = @"ProductDetails";
NSString * const MapDetailsAndListingStoryBoard = @"MapDetailsAndListing";
NSString * const CustomizeSignStoryBoard = @"Canvas";
NSString * const MyAccountStoryBoard = @"MyAccount";


NSString * const CommonFont = @"HelveticaNeueLTStd-Lt";
NSString * const CommonFontBold = @"HelveticaNeueLTStd-Bd";
NSString * const BaseURLString = @"http://purposecodes.com/signpost/api.php?action=";
NSString * const ExternalWebPageURL = @"http://signpostusa.com/";

NSString * const GoogleAutoSearchAPIForPlaceCoordinate = @"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@";

NSString * const GoogleMapAPIKey = @"AIzaSyDLo-Q_ozqGPPl3UZKbyTK_FNfe-GFj7jg";

NSString * const StoryBoardIdentifierForLoginPage = @"LoginPage";
NSString * const StoryBoardIdentifierForMenuPage = @"MenuPage";
NSString * const StoryBoardIdentifierForHomePage = @"HomePage";
NSString * const StoryBoardIdentifierForRegistrationPage = @"RegistrationPage";
NSString * const StoryBoardIdentifierForTemplateListingView = @"TemplateListingView";
NSString * const StoryBoardIdentifierForTemplateDetailView = @"TemplateDetailView" ;
NSString * const StoryBoardIdentifierForSpecificationFilter = @"SpecificationFilter";
NSString * const StoryBoardIdentifierForReviewsListingPage = @"ReviewsListingView";
NSString * const StoryBoardIdentifierForTemplateOnMapPage = @"TemplateOnMapView";
NSString * const StoryBoardIdentifierForCartListingPage = @"CartListingPage";
NSString * const StoryBoardIdentifierForCustomSignBoard = @"CustomizeSignBoard";
NSString * const StoryBoardIdentifierForBillingAddressList = @"BillingAddressList";
NSString * const StoryBoardIdentifierForCreateBillingAddress = @"CreateBillingAddress";
NSString * const StoryBoardIdentifierForNotificationsListing = @"NotificationsListing";
NSString * const StoryBoardIdentifierForSummary = @"Summary";
NSString * const StoryBoardIdentifierForMyOrders = @"MyOrders";
NSString * const StoryBoardIdentifierForMyAccount = @"MyAccount";
NSString * const StoryBoardIdentifierForOrderDetails = @"OrderDetails";
NSString * const StoryBoardIdentifierForUserTemplates = @"UserTemplate";
NSString * const StoryBoardIdentifierForWebBrowser = @"WebBrowser";
NSString * const StoryBoardIdentifierForCustomizeSignWebPage = @"CustomizeSignWebPage";



