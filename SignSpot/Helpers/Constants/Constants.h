//
//  Utility.h
//  SignSpot
//
//  Created by Purpose Code on 09/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

extern NSString * const StoryboardForSlider;
extern NSString * const StoryboardForLogin;
extern NSString * const ProductDetailsStoryBoard;
extern NSString * const MapDetailsAndListingStoryBoard;
extern NSString * const CustomizeSignStoryBoard;
extern NSString * const MyAccountStoryBoard;

extern NSString * const CommonFont;
extern NSString * const CommonFontBold;
extern NSString * const BaseURLString;
extern NSString * const ExternalWebPageURL;

extern NSString * const GoogleMapAPIKey;

extern NSString * const StoryBoardIdentifierForLoginPage;
extern NSString * const StoryBoardIdentifierForRegistrationPage;

extern NSString * const StoryBoardIdentifierForMenuPage ;
extern NSString * const StoryBoardIdentifierForHomePage ;

extern NSString * const StoryBoardIdentifierForTemplateListingView;
extern NSString * const StoryBoardIdentifierForTemplateDetailView;
extern NSString * const StoryBoardIdentifierForSpecificationFilter;
extern NSString * const StoryBoardIdentifierForReviewsListingPage;

extern NSString * const StoryBoardIdentifierForTemplateOnMapPage;
extern NSString * const StoryBoardIdentifierForCartListingPage;

extern NSString * const StoryBoardIdentifierForCustomSignBoard;
extern NSString * const StoryBoardIdentifierForBillingAddressList;
extern NSString * const StoryBoardIdentifierForCreateBillingAddress;
extern NSString * const StoryBoardIdentifierForNotificationsListing;
extern NSString * const StoryBoardIdentifierForSummary;
extern NSString * const StoryBoardIdentifierForMyOrders;
extern NSString * const StoryBoardIdentifierForMyAccount;
extern NSString * const StoryBoardIdentifierForOrderDetails;
extern NSString * const StoryBoardIdentifierForUserTemplates;
extern NSString * const StoryBoardIdentifierForWebBrowser;
extern NSString * const StoryBoardIdentifierForCustomizeSignWebPage;

