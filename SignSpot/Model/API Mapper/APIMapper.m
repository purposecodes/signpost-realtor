//
//  APIMapper.m
//  SignSpot
//
//  Created by Purpose Code on 11/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "APIMapper.h"
#import "Constants.h"

@implementation APIMapper

+ (void)registerUserWithName:(NSString*)userName userEmail:(NSString*)email phoneNumber:(NSString*)phone countryID:(NSString*)country stateID:(NSString*)stateID cityID:(NSString*)cityID userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *task, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@register",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"firstname": userName,
                             @"email": email,
                             @"phone": phone,
                             @"country_id": country,
                             @"state_id": stateID,
                             @"city_id": cityID,
                             @"password": password,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure (operation,error);
    }];

    
}

+ (void)loginUserWithUserName:(NSString*)userName userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                      failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@login",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"email": userName,
                             @"password": password,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
    
}

+ (void)updateUserProfileWithName:(NSString*)userName userID:(NSString*)userID userEmail:(NSString*)email phoneNumber:(NSString*)phone countryID:(NSString*)country stateID:(NSString*)stateID cityID:(NSString*)cityID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@updateprofile",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    if (userName.length > 0)
        [params setObject:userName forKey:@"firstname"];
    
    if (userID.length > 0)
        [params setObject:userID forKey:@"user_id"];
    
    if (email.length > 0)
        [params setObject:email forKey:@"email"];
    
    if (phone.length > 0)
        [params setObject:phone forKey:@"phone"];
    
    if (country.length > 0)
        [params setObject:country forKey:@"country_id"];

    if (stateID.length > 0)
        [params setObject:stateID forKey:@"state_id"];

    if (cityID.length > 0)
        [params setObject:cityID forKey:@"city_id"];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure (operation,error);
    }];
    

}

+ (void)updateUserSelectedCompanyWith:(NSString*)userID company:(NSString*)companyID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@userinfo",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"company_id": companyID,
                             @"user_id": userID
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    

    
}

+ (void)forgotPasswordWithEmail:(NSString*)email success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@forgotpassword",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"email": email,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    

}

+ (void)getAllProductsWithUserID:(NSString*)userID pageNumber:(NSString*)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@getallproducts",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no" : pageNumber,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}

+ (void)getAllCategoriesOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                          failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@category",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
       
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}

+ (void)getTemplateDetailsWithTemplateID:(NSString*)templateID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                 failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@preview",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"template_id" : templateID,
                              @"company_id" : companyID
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)getUserTemplateDetailsWithTemplateID:(NSString*)templateID userID:(NSString*)userID cartID:(NSInteger)cartID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@preview",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"template_id" : templateID,
                             @"company_id" : companyID,
                             @"cart_id" : [NSNumber numberWithInteger:cartID]
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}

+ (void)getCalculatedPriceForTheTemplateWithTemplateID:(NSString*)templateID userID:(NSString*)userID signTypeID:(NSString*)signTypeID signSizeID:(NSString*)signSizeID printType:(NSInteger)printType companyID:(NSString*)companyID quantity:(NSInteger)quantity Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *_companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@getprice",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"signtemplate_id" : templateID,
                             @"signtype_id" : signTypeID,
                             @"print_type" : [NSNumber numberWithInteger:printType],
                             @"signsize_id" : signSizeID,
                             @"company_id" : _companyID,
                             @"qty"        :[NSNumber numberWithInteger:quantity]
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
}


+ (void)postUserReviewWithTemplateID:(NSString*)templateID userID:(NSString*)userID reviewTitle:(NSString*)title review:(NSString*)review rating:(float)rating  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@addreview",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"signtemplate_id" : templateID,
                             @"review_title" : title,
                             @"review_details" : review,
                             @"company_id" : companyID,
                             @"rate" : [NSNumber numberWithFloat:rating]
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}

+ (void)getAllReviewsForTemplate:(NSString*)templateID userID:(NSString*)userID  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@getallreviews",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"signtemplate_id" : templateID,
                             @"company_id" : companyID
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)addTheProductToCartWith:(NSString*)templateID userID:(NSString*)userID cartID:(NSInteger)cartID signTypeID:(NSString*)signTypeID printType:(NSInteger)printType signSizeID:(NSString*)signSizeID categoryID:(NSString*)categoryID quantity:(NSInteger)quantity price:(NSString*)price jsonString:(NSDictionary*)json image:(NSData*)imgData Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@addtocart",BaseURLString];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:templateID forKey:@"signtemplate_id"];
    [params setObject:signTypeID forKey:@"signtype_id"];
    [params setObject:[NSNumber numberWithInteger:printType] forKey:@"print_type"];
    [params setObject:signSizeID forKey:@"signsize_id"];
    [params setObject:categoryID forKey:@"category_id"];
    [params setObject:[NSNumber numberWithInteger:quantity] forKey:@"sign_qty"];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:price forKey:@"sign_price"];
    [params setObject:companyID forKey:@"company_id"];
    if (jsonString.length > 0) [params setObject:jsonString forKey:@"json_string"];
    
    if (cartID > 0) {
         [params setObject:[NSNumber numberWithInteger:cartID] forKey:@"cart_id"];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imgData
                                    name:@"sign_image"
                                fileName:@"TemplateImage" mimeType:@"image/jpeg"];
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure (operation,error);
    }];
    
    
}

+ (void)getAllProductsInCartWith:(NSString*)companyID userID:(NSString*)userID  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@viewcart",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"company_id": companyID,
                             @"user_id" : userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)removeItemFromCartWith:(NSString*)cartID userID:(NSString*)userID  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *companyID = [User sharedManager].companyID;
    NSString *urlString = [NSString stringWithFormat:@"%@removecart",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"cart_id": cartID,
                             @"user_id" : userID,
                             @"company_id": companyID
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}
+ (void)updateCartWith:(NSString*)companyID cartID:(NSInteger)cartID userID:(NSString*)userID mapID:(NSInteger)mapID address:(NSString*)address title:(NSString*)title description:(NSString*)description annotations:(NSMutableArray*)annotations polygonPath:(NSString*)polygonPath images:(NSMutableArray*)images screenShot:(UIImage*)screenShot Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure progress:(void (^)( long long totalBytesWritten,long long totalBytesExpectedToWrite))progress{
    
    /*
    
        NSString *urlString = [NSString stringWithFormat:@"%@updatemap",BaseURLString];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setObject:userID forKey:@"user_id"];
        [params setObject:address forKey:@"address"];
        [params setObject:description forKey:@"location_desc"];
        [params setObject:title forKey:@"location_title"];
        [params setObject:[NSNumber numberWithInteger:cartID] forKey:@"cart_id"];
        if (annotations.count) [params setObject:annotations forKey:@"annotations"];
        if (polygonPath.length)[params setObject:polygonPath forKey:@"map_path"];
        if (mapID > 0)[params setObject:[NSNumber numberWithInteger:mapID] forKey:@"map_id"];
        AFHTTPRequestOperation *operation = [manager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSInteger index = 0;
        for (UIImage *image in images) {
            NSData *imageData = UIImageJPEGRepresentation(image,1);
            if (imageData.length)
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image_file%ld",(long)index] fileName:@"Plan" mimeType:@"image/jpeg"];
            index ++;
            
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
        
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        
        progress(totalBytesWritten,totalBytesExpectedToWrite);
        
    }];*/
    
    NSString *urlString = [NSString stringWithFormat:@"%@updatemap",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    [params setObject:address forKey:@"address"];
    [params setObject:description forKey:@"location_desc"];
    [params setObject:title forKey:@"location_title"];
    if (images) {
        [params setObject:[NSNumber numberWithInteger:images.count] forKey:@"image_count"];
    }
    [params setObject:[NSNumber numberWithInteger:cartID] forKey:@"cart_id"];
    if (annotations.count) [params setObject:annotations forKey:@"annotations"];
    if (polygonPath.length)[params setObject:polygonPath forKey:@"map_path"];
    if (mapID > 0)[params setObject:[NSNumber numberWithInteger:mapID] forKey:@"map_id"];
    [manager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (screenShot) {
            NSData *imageData = UIImageJPEGRepresentation(screenShot,1);
            if (imageData.length)
                [formData appendPartWithFileData:imageData name:@"map_image" fileName:@"Map" mimeType:@"image/jpeg"];
        }
        
        NSInteger index = 0;
        for (UIImage *image in images) {
            NSData *imageData = UIImageJPEGRepresentation(image,1);
            if (imageData.length)
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image_file%ld",(long)index] fileName:@"Plan" mimeType:@"image/jpeg"];
            index ++;
            
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure (operation,error);
    }];

    
    
}

+ (void)getSavedMapDetailsWith:(NSString*)productID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@savedmap",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"signtemplate_id": productID,
                             @"user_id" : userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    

    
}


+ (void)getAllSavedBillingAdresses:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@getallbillingAddress",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id" : userID,
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}
+ (void)createNewBillingAddressWithDict:(NSMutableDictionary*)params Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@checkout",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)getAllStatesUnderCountryWithID:(NSString*)countryID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@states",BaseURLString];
    NSDictionary *params = @{@"country_id" : countryID,
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}
+ (void)getAllCitiesUnderstateWithID:(NSString*)stateID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@cities",BaseURLString];
    NSDictionary *params = @{@"state_id" : stateID,
                             };
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}

+ (void)removeBillingAddressWithID:(NSString*)billID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@removeaddress",BaseURLString];
    NSDictionary *params = @{@"bill_id" : billID,
                             @"user_id" : userID
                             };

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}
+ (void)getBaseTemplateSpecificationsWith:(NSString*)templateId cartID:(NSInteger)cartID isUserTemplate:(BOOL)isUserTemplate Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSString *urlString = [NSString stringWithFormat:@"%@androidcustomize",BaseURLString];
    if (isUserTemplate) {
        urlString = [NSString stringWithFormat:@"%@usercustomize",BaseURLString];
        if (cartID > 0)
            [params setObject:[NSNumber numberWithInteger:cartID] forKey:@"cart_id"];
        
    }
    [params setObject:templateId forKey:@"signtemplate_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}


+ (void)loadMapWithCartID:(NSString*)cartID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@viewmap",BaseURLString];
    NSDictionary *params = @{@"cart_id" : cartID,
                             @"user_id" : userID
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)updateUserPaswordWithOldPwd:(NSString*)oldPwd newPwd:(NSString*)newPwd userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@changepassword",BaseURLString];
    NSDictionary *params = @{@"oldpass" : oldPwd,
                             @"newpass" : newPwd,
                             @"user_id" : userID
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}
+ (void)getUserTemplatesWith:(NSString*)userID  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@library",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
    
}

+ (void)loadAllInstallerProductsWithUserID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@getpoints",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}
+ (void)loaaDrivingDirectionsWithURL:(NSString*)url Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    NSString *urlString = url;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    [manager POST:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}

+ (void)getAllOrdersWithUserID:(NSString*)userID PageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@order",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no": [NSNumber numberWithInteger:pageNumber]
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}

+ (void)getOrderDetails:(NSString*)userID orderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@orderview",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"order_id": orderID
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}
+ (void)cancelAnOrderWith:(NSString*)userID orderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@cancelOrder",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"order_id": orderID
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}
+ (void)removeSignBoradWith:(NSString*)userID orderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@removalsignboard",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"order_id": orderID
                             };
    
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)showSummaryDetailsWithOrderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@summary",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"orderId": orderID,
                             @"user_id": [User sharedManager].userId
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

    
}
+ (void)getAllMyNotificationsWith:(NSString*)userID pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@notification",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"page_no": [NSNumber numberWithInteger:pageNumber]
                             };
    
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
    
}
+ (void)deleteUserTemplateWithID:(NSString*)templateID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@removeusertemplate",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"user_id": userID,
                             @"delete_id": templateID,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)getWebContentWithType:(NSString*)type success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@webcontent",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"type": type,
                             };
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];

}

+ (void)updateSurveyImageWhileReviewWith:(NSString*)mapID cartID:(NSString*)cartID userID:(NSString*)userID deletedIDs:(NSMutableArray*)deletedIDs newImages:(NSArray*)newImages success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"%@updateplanimage",BaseURLString];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userID forKey:@"user_id"];
    if (newImages.count) {
        [params setObject:[NSNumber numberWithInteger:newImages.count] forKey:@"image_count"];
        
    }if (deletedIDs.count) {
        [params setObject:[NSString stringWithFormat:@"%@",[deletedIDs componentsJoinedByString:@","]] forKey:@"image_id"];
    }
    
    [params setObject:mapID forKey:@"map_id"];
    [params setObject:cartID forKey:@"cart_id"];
    [manager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    NSInteger index = 0;
        for (UIImage *image in newImages) {
            NSData *imageData = UIImageJPEGRepresentation(image,1);
            if (imageData.length)
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image_file%ld",(long)index] fileName:@"Plan" mimeType:@"image/jpeg"];
            index ++;
            
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (operation,error);
    }];
}

@end
