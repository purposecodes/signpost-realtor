//
//  APIMapper.h
//  SignSpot
//
//  Created by Purpose Code on 11/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface APIMapper : NSObject

+ (void)registerUserWithName:(NSString*)userName userEmail:(NSString*)email phoneNumber:(NSString*)phone countryID:(NSString*)country stateID:(NSString*)stateID cityID:(NSString*)cityID userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loginUserWithUserName:(NSString*)userName userPassword:(NSString*)password success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateUserProfileWithName:(NSString*)userName userID:(NSString*)userID userEmail:(NSString*)email phoneNumber:(NSString*)phone countryID:(NSString*)country stateID:(NSString*)stateID cityID:(NSString*)cityID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateUserSelectedCompanyWith:(NSString*)userID company:(NSString*)companyID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;


+ (void)forgotPasswordWithEmail:(NSString*)email success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                      failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;


+ (void)getAllProductsWithUserID:(NSString*)userID pageNumber:(NSString*)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;


+ (void)getAllCategoriesOnsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getTemplateDetailsWithTemplateID:(NSString*)templateID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getCalculatedPriceForTheTemplateWithTemplateID:(NSString*)templateID userID:(NSString*)userID signTypeID:(NSString*)signTypeID signSizeID:(NSString*)signSizeID printType:(NSInteger)printType companyID:(NSString*)companyID quantity:(NSInteger)quantity Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)postUserReviewWithTemplateID:(NSString*)templateID userID:(NSString*)userID reviewTitle:(NSString*)title review:(NSString*)review rating:(float)rating  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllReviewsForTemplate:(NSString*)templateID userID:(NSString*)userID  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)addTheProductToCartWith:(NSString*)templateID userID:(NSString*)userID cartID:(NSInteger)cartID signTypeID:(NSString*)signTypeID printType:(NSInteger)printType signSizeID:(NSString*)signSizeID categoryID:(NSString*)categoryID quantity:(NSInteger)quantity price:(NSString*)price jsonString:(NSDictionary*)json image:(NSData*)imgData Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllProductsInCartWith:(NSString*)companyID userID:(NSString*)userID  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeItemFromCartWith:(NSString*)cartID userID:(NSString*)userID  Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;


+ (void)updateCartWith:(NSString*)companyID cartID:(NSInteger)cartID userID:(NSString*)userID mapID:(NSInteger)mapID address:(NSString*)address title:(NSString*)title description:(NSString*)description annotations:(NSMutableArray*)annotations polygonPath:(NSString*)polygonPath images:(NSMutableArray*)images screenShot:(UIImage*)screenShot Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure progress:(void (^)( long long totalBytesWritten,long long totalBytesExpectedToWrite))progress;

+ (void)getSavedMapDetailsWith:(NSString*)productID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllSavedBillingAdresses:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)createNewBillingAddressWithDict:(NSMutableDictionary*)params Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllStatesUnderCountryWithID:(NSString*)countryID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllCitiesUnderstateWithID:(NSString*)stateID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeBillingAddressWithID:(NSString*)billID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getBaseTemplateSpecificationsWith:(NSString*)templateId cartID:(NSInteger)cartID isUserTemplate:(BOOL)isUserTemplate Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loadMapWithCartID:(NSString*)cartID userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateUserPaswordWithOldPwd:(NSString*)oldPwd newPwd:(NSString*)newPwd userID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserTemplatesWith:(NSString*)userID  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getUserTemplateDetailsWithTemplateID:(NSString*)templateID userID:(NSString*)userID cartID:(NSInteger)cartID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loadAllInstallerProductsWithUserID:(NSString*)userID Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)loaaDrivingDirectionsWithURL:(NSString*)url Onsuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllOrdersWithUserID:(NSString*)userID PageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getOrderDetails:(NSString*)userID orderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)cancelAnOrderWith:(NSString*)userID orderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)removeSignBoradWith:(NSString*)userID orderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)showSummaryDetailsWithOrderID:(NSString*)orderID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getAllMyNotificationsWith:(NSString*)userID pageNumber:(NSInteger)pageNumber success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)deleteUserTemplateWithID:(NSString*)templateID userID:(NSString*)userID success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)getWebContentWithType:(NSString*)type success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;

+ (void)updateSurveyImageWhileReviewWith:(NSString*)orderID cartID:(NSString*)cartID userID:(NSString*)userID deletedIDs:(NSMutableArray*)deletedIDs newImages:(NSArray*)newImages success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *task, NSError *error))failure;


@end
