//
//  CountryListView.h
//  SignSpot
//
//  Created by Purpose Code on 10/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


typedef enum{
    
    eCountryList = 0,
    eStateList = 1,
    eCityList = 2,
    
}eLocationList;


#import <UIKit/UIKit.h>

@protocol LocationSelectionDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Close' Button.
 */

-(void)closeLocationListingPopUpAfterADelay:(float)delay;

/*!
 *This method is invoked when user selects a country.The selected Country Details sends back to Registration page
 */


-(void)sendSelectedLocationsWith:(NSDictionary*)locationInfo type:(eLocationList)locType;

@end



@interface LocationListView : UIView <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>{
    
    UITableView *tableView;
    NSMutableArray *arrCountries;
    NSInteger indexOfSelectedRow;
    eLocationList locType;
    BOOL isDataAvailable;
    
}

@property (nonatomic,weak)  id<LocationSelectionDelegate>delegate;
@property (nonatomic,weak) NSString *selectedID;

-(void)setUp;
-(void)loadLocationsOn:(eLocationList)typeOfLocation;
@end
