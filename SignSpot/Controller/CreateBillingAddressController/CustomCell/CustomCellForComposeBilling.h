//
//  CustomCellForComposeBilling.h
//  SignSpot
//
//  Created by Purpose Code on 08/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellForComposeBilling : UITableViewCell

@property (nonatomic,weak) IBOutlet  UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet  UITextField *txtField;

@end
