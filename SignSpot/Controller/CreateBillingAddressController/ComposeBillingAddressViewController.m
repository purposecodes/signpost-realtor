//
//  BillingAddressListViewControlller.m
//  SignSpot
//
//  Created by Purpose Code on 08/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


typedef enum{
    
    eName = 0,
    eAddress1 = 1,
    eAddress2 = 2,
    eCountry = 3,
    eState = 4,
    eCity = 5,
    ePinCode = 6,
    eMobile = 7,
    eEmail = 8,
    
    
}EFieldInfo;



#define kCellHeight             60;
#define kHeightForHeader        40;
#define kHeightForFooter        50;
#define kMaxReviewLength        150
#define kMaxReviewTitleLength   50

#define kCountryID              @"CountryID"
#define kStateID                @"StateID"

#import "ComposeBillingAddressViewController.h"
#import "CustomCellForComposeBilling.h"
#import "Constants.h"
#import "LocationListView.h"
#import "NotificationsListingViewController.h"
#import "BillingAddressListViewControlller.h"
#import "SummaryViewController.h"

@interface ComposeBillingAddressViewController ()<LocationSelectionDelegate,BillingListSelectionDelegate>{
    
    IBOutlet UITableView *tableView;
    UIView *inputAccView;
    NSMutableDictionary *dictInfo;
    NSInteger indexForTextFieldNavigation;
    NSInteger totalRequiredFieldCount;
    
    LocationListView *locationsListView;
    UITextField *activeTextField;
    NSString *selectedLocationID;
    NSString *billID;
    NSMutableArray *arrAdress;
    BOOL isDataAvailable;
    
}

@end

@implementation ComposeBillingAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self loadAllBillingAddressesIfAny];

    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    totalRequiredFieldCount = 9;
    dictInfo = [NSMutableDictionary new];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.allowsSelection = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
   
  
    
    
}

-(void)loadAllBillingAddressesIfAny{
    
    [self showLoadingScreenWithTitle:@"Loading.."];
    [APIMapper getAllSavedBillingAdresses:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        tableView.hidden = false;
        if ( NULL_TO_NIL( [responseObject objectForKey:@"resultarray"])) {
            arrAdress = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"resultarray"]];
            if (arrAdress.count > 0){
                isDataAvailable = true;
                [self editSelectedBillWithDetails: [arrAdress firstObject]];
            }else [self setDefaultValues];
            
        }
        else [self setDefaultValues];
        [tableView reloadData];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        tableView.hidden = false;
        [self hideLoadingScreen];
    }];
}


-(void)setDefaultValues{
    
    NSDictionary *countryInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"231",@"id",@"United States",@"name", nil];
    [dictInfo setObject:countryInfo forKey:[NSNumber numberWithInteger:eCountry]];
    NSDictionary *cityInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"46384",@"id",@"Houston",@"name", nil];
    [dictInfo setObject:cityInfo forKey:[NSNumber numberWithInteger:eCity]];
    NSDictionary *stateInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"3970",@"id",@"Texas",@"name", nil];
    [dictInfo setObject:stateInfo forKey:[NSNumber numberWithInteger:eState]];
    [dictInfo setObject:@"AVISON YOUNG" forKey:[NSNumber numberWithInteger:eName]];
    [dictInfo setObject:@"Williams Tower,2800 Post Oak Blvd" forKey:[NSNumber numberWithInteger:eAddress1]];
    [dictInfo setObject:@"77056" forKey:[NSNumber numberWithInteger:ePinCode]];
    [dictInfo setObject:@"+1 713-993-7700" forKey:[NSNumber numberWithInteger:eMobile]];
    [dictInfo setObject:@"Avison-Young@purposecodes.com" forKey:[NSNumber numberWithInteger:eEmail]];
}
#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return totalRequiredFieldCount;
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"reuseIdentifier";
    CustomCellForComposeBilling *cell = (CustomCellForComposeBilling *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.txtField.text = @"";
    cell.txtField.keyboardType = UIKeyboardTypeDefault;
    NSInteger caseCalue  = [indexPath row];
    cell.txtField.tag = caseCalue;
    
    if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:caseCalue]])){
        
        if ([[dictInfo objectForKey:[NSNumber numberWithInteger:caseCalue]]isKindOfClass:[NSDictionary class]]) {
            cell.txtField.text = (NSString*)[[dictInfo objectForKey:[NSNumber numberWithInteger:caseCalue]]objectForKey:@"name"];
        }else{
            cell.txtField.text = (NSString*)[dictInfo objectForKey:[NSNumber numberWithInteger:caseCalue]];
        }
    }
    
    cell = [self configureCellWithCaseValue:caseCalue cell:cell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kCellHeight;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        CustomCellForComposeBilling *cell = (CustomCellForComposeBilling*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.txtField.tag data:textField.text];
    }
   
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    activeTextField = textField;
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        indexForTextFieldNavigation = indexPath.row;
    }
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
    
    /*! For country listing popup !*/
    
    if (indexPath.row == eCountry) {
        //[self showAllCountries];
        return NO;
    }
    /*! For State listing popup !*/
    else if (indexPath.row == eState) {
        [self showAllStatesUnderCountry];
        return NO;
    }
    /*! For City listing popup !*/
    else if (indexPath.row == eCity) {
        [self showAllCitiesUnderState];
        return NO;
    }
    
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
   
}

#pragma mark - Common Actions


-(CustomCellForComposeBilling*)configureCellWithCaseValue:(NSInteger)caseValue cell:(CustomCellForComposeBilling*)cell{
    
    cell.txtField.keyboardType = UIKeyboardTypeDefault;
    switch (caseValue) {
        case eName:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Name";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"UserIcon.png"];
            break;
        case eAddress1:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Address 1";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"AddressIcon.png"];
            break;
            
        case eAddress2:
            
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Address 2";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"MapIcon.png"];
            break;
            
        case eCity:
            
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"City";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"CityIcon.png"];
            break;
            
        case eState:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"State";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"StateIcon.png"];
            break;
            
        case eCountry:
            
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Country";
            }
            cell.txtField.text = @"United States";
            cell.imgIcon.image = [UIImage imageNamed:@"CountryIcon.png"];
            break;
            
        case ePinCode:
            
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"ZIP code";
            }
            cell.txtField.keyboardType = UIKeyboardTypeNumberPad;
            cell.imgIcon.image = [UIImage imageNamed:@"PincodeIcon.png"];
            break;
            
        case eMobile:
            
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Mobile";
            }
             cell.txtField.keyboardType = UIKeyboardTypeNumberPad;
            cell.imgIcon.image = [UIImage imageNamed:@"MobileIcon.png"];
            break;
            
        case eEmail:
            
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Email";
            }
             cell.txtField.keyboardType = UIKeyboardTypeEmailAddress;
            cell.imgIcon.image = [UIImage imageNamed:@"EmailIcon.png"];
            break;
            
        default:
            break;
    }
    
    [cell.txtField reloadInputViews];

    return cell;

}

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 0.8];
    
    UIButton *btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    [btnPrev setFrame: CGRectMake(0.0, 1.0, 80.0, 38.0)];
    [btnPrev setTitle: @"PREVIOUS" forState: UIControlStateNormal];
    [btnPrev setBackgroundColor: [UIColor getHeaderOffBlackColor]];
    btnPrev.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    btnPrev.layer.cornerRadius = 5.f;
    btnPrev.layer.borderWidth = 1.f;
    btnPrev.layer.borderColor = [UIColor clearColor].CGColor;
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 1.0f, 80.0f, 38.0f)];
    [btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor getHeaderOffBlackColor]];
    btnNext.layer.cornerRadius = 5.f;
    btnNext.layer.borderWidth = 1.f;
    btnNext.layer.borderColor = [UIColor clearColor].CGColor;
    btnNext.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 1.0f, 85.0f, 38.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor clearColor].CGColor;
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    
    
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}

-(void)gotoPrevTextfield{
    
    if (indexForTextFieldNavigation - 1 < 0) indexForTextFieldNavigation = 0;
    else {
        indexForTextFieldNavigation -= 1;
        if (indexForTextFieldNavigation == eCountry) {
            indexForTextFieldNavigation -= 1;
        }
    }
    [self gotoTextField];
   
}

-(void)gotoNextTextfield{
    
    if (indexForTextFieldNavigation + 1 < totalRequiredFieldCount) indexForTextFieldNavigation += 1;
    if (indexForTextFieldNavigation == eCountry) {
        indexForTextFieldNavigation += 1;
    }
    [self gotoTextField];
}

-(void)gotoTextField{
    
    CustomCellForComposeBilling *nextCell = (CustomCellForComposeBilling *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
    if (!nextCell) {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        nextCell = (CustomCellForComposeBilling *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
        
    }
    [nextCell.txtField becomeFirstResponder];

}

-(void)doneTyping{
    [self.view endEditing:YES];
    
}

-(void)getTextFromField:(NSInteger)type data:(NSString*)string{
    
    if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:type]]))
        [dictInfo removeObjectForKey:[NSNumber numberWithInteger:type]];
    
    if ([string length])
        [dictInfo setObject:string forKey:[NSNumber numberWithInteger:type]];
    
}


#pragma mark - Button Actions

-(IBAction)loadSavedAddress:(id)sender{
    
    BillingAddressListViewControlller *billingAdres = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForBillingAddressList];
    billingAdres.delegate = self;
    [[self navigationController]pushViewController:billingAdres animated:YES];

}

-(void)editSelectedBillWithDetails:(NSDictionary*)_billingInfo{
    
    /*! Delegate when selected a bill from the biling list */
    
    
    NSInteger tag = 0;
    NSString *value;
  
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_id"])) {
        
        billID = [_billingInfo objectForKey:@"bill_id"];
    }
    
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_name"])) {
        
        value = [_billingInfo objectForKey:@"bill_name"];
        if (value.length) [dictInfo setObject:value forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_address1"])) {
        
        value = [_billingInfo objectForKey:@"bill_address1"];
        if (value.length) [dictInfo setObject:value forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_address2"])) {
        
        value = [_billingInfo objectForKey:@"bill_address2"];
        if (value.length) [dictInfo setObject:value forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"country_name"])) {
        
        NSMutableDictionary *loc = [NSMutableDictionary new];
        [loc setObject:[_billingInfo objectForKey:@"country_name"] forKey:@"name"];
        [loc setObject:[_billingInfo objectForKey:@"bill_country"] forKey:@"id"];
        [dictInfo setObject:loc forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"state_name"])) {
        
        value = [_billingInfo objectForKey:@"state_name"];
        NSMutableDictionary *loc = [NSMutableDictionary new];
        [loc setObject:[_billingInfo objectForKey:@"state_name"] forKey:@"name"];
        [loc setObject:[_billingInfo objectForKey:@"bill_state"] forKey:@"id"];
        [dictInfo setObject:loc forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"city_name"])) {
        
        NSMutableDictionary *loc = [NSMutableDictionary new];
        [loc setObject:[_billingInfo objectForKey:@"city_name"] forKey:@"name"];
        [loc setObject:[_billingInfo objectForKey:@"bill_city"] forKey:@"id"];
        [dictInfo setObject:loc forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_pin"])) {
        
        value = [_billingInfo objectForKey:@"bill_pin"];
        if (value.length) [dictInfo setObject:value forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_mobile"])) {
        
        value = [_billingInfo objectForKey:@"bill_mobile"];
        if (value.length) [dictInfo setObject:value forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    if ( NULL_TO_NIL([_billingInfo objectForKey:@"bill_email"])) {
        
        value = [_billingInfo objectForKey:@"bill_email"];
        if (value.length) [dictInfo setObject:value forKey:[NSNumber numberWithInteger:tag]];
        tag ++;
    }
    
    [tableView reloadData];

    
}

-(IBAction)saveAndContinueApplied:(id)sender{
    
    [self.view endEditing:YES];
   
   BOOL isFilled = [self checkFieldsAreNonEmpty];
    if (isFilled) {
        
        [self checkAllFieldsAreValidOnSuccess:^{
            
            [self createNewBillingAddress];
            
        } failure:^(NSString *title, NSString *message) {
            
            [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            
        }];
        
        
    }else{
        
        // Show missed field as selected by scrolling the table
        
        for (NSInteger i = 0; i < totalRequiredFieldCount; i ++) {
            
            if (![dictInfo objectForKey:[NSNumber numberWithInteger:i]]) {
                
                if (i != eAddress2) {
                   
                    double delayInSeconds = 0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        CustomCellForComposeBilling *nextCell = (CustomCellForComposeBilling *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                        if (!nextCell) {
                            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                            nextCell = (CustomCellForComposeBilling *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i  inSection:0]];
                        }
                        
                        [nextCell.txtField becomeFirstResponder];
                    });
                    
                    break;
                }
               
            }
        }

    }
   
}

-(void)createNewBillingAddress{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    NSArray *keys = [dictInfo allKeys];
    for (id num in keys) {
        NSInteger caseValue = [num integerValue];
        switch (caseValue) {
            case eName:
                [params setObject:[dictInfo objectForKey:num] forKey:@"bill_name"];
                break;
                
            case eAddress1:
                [params setObject:[dictInfo objectForKey:num] forKey:@"bill_address1"];
                break;
                
            case eAddress2:
                [params setObject:[dictInfo objectForKey:num] forKey:@"bill_address2"];
                break;
                
            case eCountry:
                [params setObject:[self getSavedLocIDFrom:num] forKey:@"bill_country"];
                break;
                
            case eState:
                [params setObject:[self getSavedLocIDFrom:num] forKey:@"bill_state"];
                break;
                
            case eCity:
                [params setObject:[self getSavedLocIDFrom:num] forKey:@"bill_city"];
                break;
                
            case ePinCode:
                [params setObject:[dictInfo objectForKey:num] forKey:@"bill_pin"];
                break;
                
            case eMobile:
                [params setObject:[dictInfo objectForKey:num] forKey:@"bill_mobile"];
                break;
                
            case eEmail:
                [params setObject:[dictInfo objectForKey:num] forKey:@"bill_email"];
                break;
                
            default:
                break;
        }
    }
    [params setObject:[User sharedManager].shippingID forKey:@"shipping_id"];
    [params setObject:[User sharedManager].userId forKey:@"user_id"];
    if (billID.length)[params setObject:billID forKey:@"bill_id"];
    if (_cartIDs.length > 0)[params setObject:_cartIDs forKey:@"cart_id"];
    
    [self showLoadingScreenWithTitle:@"Saving.."];
    [APIMapper createNewBillingAddressWithDict:params Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        [self showSummaryPageWith:responseObject];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
    }];
    
}

-(NSNumber*)getSavedLocIDFrom:(NSNumber*)num{
    
    NSNumber *locID ;
    if ( NULL_TO_NIL([dictInfo objectForKey:num])) {
        NSDictionary *countryDetails = [dictInfo objectForKey:num];
        if (countryDetails && [countryDetails objectForKey:@"id"])
            locID = [countryDetails objectForKey:@"id"];
    }
    return locID;
}

-(void)showSummaryPageWith:(NSDictionary*)responds{
    
    SummaryViewController *summary = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForSummary];
    [[self navigationController]pushViewController:summary animated:YES];
    [summary showSummaryDetailsWithInfo:responds];
}

#pragma mark - Country , State , State listing popups


-(void)showAllCountries{
    
    [self.view endEditing:YES];
    if (!locationsListView) {
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
    }
    [locationsListView setUp];
    locationsListView.selectedID = selectedLocationID;
    [locationsListView loadLocationsOn:eCountryList];
}


-(void)showAllStatesUnderCountry{
    
   [self.view endEditing:YES];
    if (!locationsListView) {
        
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [locationsListView setUp];
    [locationsListView loadLocationsOn:eStateList];
 
}

-(void)showAllCitiesUnderState{
    
    [self.view endEditing:YES];
    NSString *locID;
    BOOL isIDAvailable = false;
    
    if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:eState]])) {
        
        NSDictionary *country = [dictInfo objectForKey:[NSNumber numberWithInteger:eState]];
        if ([country objectForKey:@"id"]) {
            
            locID = [country objectForKey:@"id"];
            isIDAvailable = true;
        }
    }
    if (!isIDAvailable){
        [[[UIAlertView alloc] initWithTitle:@"City" message:@"Please choose a State." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    if (!locationsListView) {
        
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [locationsListView setUp];
    locationsListView.selectedID = locID;
    [locationsListView loadLocationsOn:eCityList];
 
}

-(void)closeLocationListingPopUpAfterADelay:(float)delay{
    
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [locationsListView removeFromSuperview];
        locationsListView = nil;
    }];
}

-(void)sendSelectedLocationsWith:(NSDictionary*)locationInfo type:(eLocationList)locType{

    // CallBack when user tapped any Country from country list
    if ( NULL_TO_NIL( [locationInfo objectForKey:@"name"])) {
        selectedLocationID = [locationInfo objectForKey:@"id"];
        [dictInfo setObject:locationInfo forKey:[NSNumber numberWithInteger:activeTextField.tag]];
    }
    
//    if (locType == eCountryList) {
//        
//        if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:eState]]))
//            [dictInfo removeObjectForKey:[NSNumber numberWithInteger:eState]];
//        
//        if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:eCity]]))
//            [dictInfo removeObjectForKey:[NSNumber numberWithInteger:eCity]];
//        
//    }
    if (locType == eStateList) {
                
        if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:eCity]]))
            [dictInfo removeObjectForKey:[NSNumber numberWithInteger:eCity]];
        
    }
    
   
    
    [tableView reloadData];
}


-(void)checkAllFieldsAreValidOnSuccess:(void (^)())success failure:(void (^)(NSString *title,NSString *message))failure{
    
    if ( NULL_TO_NIL([dictInfo objectForKey:[NSNumber numberWithInteger:eEmail]])){
        
        NSString *email = [dictInfo objectForKey:[NSNumber numberWithInteger:eEmail]];
        if (![email isValidEmail]) failure(@"Email",@"Please enter a valid Email");else success();
        
    }
    
}

-(BOOL)checkFieldsAreNonEmpty{
    
    NSArray *allKeys = [dictInfo  allKeys];
    if (allKeys.count == totalRequiredFieldCount) {
        return YES;
    }else{
        if (allKeys.count >= totalRequiredFieldCount - 1) {
            if (![dictInfo objectForKey:[NSNumber numberWithInteger:eAddress2]]) return YES;
            else return NO;
        }
    }
    
    return NO;
}

-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
    
}

-(void)showLoadingScreenWithTitle:(NSString*)title{
         
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
}
-(void)hideLoadingScreen{
         
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
