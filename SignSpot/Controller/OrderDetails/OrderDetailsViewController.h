//
//  OrderDetailsViewController.h
//  SignSpot
//
//  Created by Purpose Code on 27/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailsViewController : UIViewController

@property (nonatomic,assign)BOOL showBackBtn;
-(void)showOrderDetailsWithInfo:(NSDictionary*)details;
-(void)orderDetailsWithOrderID:(NSString*)orderID;

@end
