//
//  SummaryViewController.m
//  SignSpot
//
//  Created by Purpose Code on 09/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eMap = 0,
    eProducts = 1,
    eBillingAddress = 3,
    eDeliveryAddress = 2,
    eOrderInfo = 4
   
}eHeaderType;

typedef enum{
    
    eTypePending    = 1,
    eTypeCancelled  = 3,
    eTypeCompleted  = 2
    
    
}eOrderType;


#define kSectionCount                   5
#define kCellHeightWhenOrderInfo        760
#define kCellHeightForProducts          265
#define kCellHeightForAddress           110
#define kCellHeightForOrder             120
#define kSuccessCode                    200
#define kMinimumCellCount               1
#define kFooterHeight                   50
#define kHeightForHeader                50
#define kCellHeightForMap               250
#define kCellHeightForDeliveryAddress   100

#import "OrderDetailsViewController.h"
#import "ProductCustomCell.h"
#import "AdderssCustomCell.h"
#import "OrderInfoCustomCell.h"
#import "Constants.h"
#import "MyAccountViewController.h"
#import "MapCustomTableViewCell.h"
#import "DeliveryAdderssCustomCell.h"
#import "PhotoBrowser.h"

@interface OrderDetailsViewController () <PhotoBrowserDelegate> {
    
    NSDictionary *_dictProductInfo;
    IBOutlet UITableView *tableView;
    IBOutlet GMSMapView *mapView;
    IBOutlet UIView *vwDatePicker;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIButton *btnBack;
    
    NSDictionary *dictProducts;
    NSDictionary *dictAddress;
    NSDictionary *dictMapInfo;
    NSDictionary *dictOrderInfo;
    NSArray *arrImages;
    
    BOOL isDataAvailable;
    NSInteger grandTotal;
    
    PhotoBrowser *photoBrowser;
    
    
}

@end

@implementation OrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    
    isDataAvailable = true;
    vwDatePicker.alpha = 0.f;
    datePicker.minimumDate = [NSDate date];
    tableView.allowsSelection = NO;
    btnBack.hidden = false;  //If coming After payment section , no need to show back icon.Else show back
    if (!_showBackBtn) btnBack.hidden = true;
    
}

-(void)orderDetailsWithOrderID:(NSString*)orderID{
    
    [self showLoadingScreen];
    [APIMapper showSummaryDetailsWithOrderID:orderID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([[[responseObject objectForKey:@"header"] objectForKey:@"code"]integerValue] == kSuccessCode) {
            
            if (NULL_TO_NIL([[[responseObject objectForKey:@"summary"] objectForKey:@"order"] objectForKey:@"orderno"])) {
                NSString *orderNo = [[[responseObject objectForKey:@"summary"] objectForKey:@"order"] objectForKey:@"orderno"];
                [self showAlertWithTitle:@"Summary" message:[NSString stringWithFormat:@"Your order (#%@) has been placed successfully.",orderNo]];
            }
            [self showOrderDetailsWithInfo:responseObject];
            
        }
        
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         [self showAlertWithTitle:@"Summary" message:@"Failed to load order deatils.Please check your Orders Section"];
        [self hideLoadingScreen];
    }];
    
}

-(void)showOrderDetailsWithInfo:(NSDictionary*)details{
    
    _dictProductInfo = [NSDictionary new];
    _dictProductInfo = details;
    dictMapInfo =[NSDictionary new];
    dictAddress =[NSDictionary new];
    dictProducts =[NSDictionary new];
    dictOrderInfo =[NSDictionary new];
    [self showProductsAndDetails];
}
-(void)showProductsAndDetails{
    
    if (_dictProductInfo) {
        
        if (NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"map"]))
            dictMapInfo = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"map"];
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"billing"] ))
            dictAddress = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"billing"];
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"cartitems"]) )
            dictProducts = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"cartitems"];
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"order"]) )
            dictOrderInfo = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"order"];
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"header"]  objectForKey:@"grand_total"] ))
            grandTotal = [[[_dictProductInfo objectForKey:@"header"]  objectForKey:@"grand_total"] integerValue];
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"image"]) )
            arrImages = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"image"];
        
        
    }
    
    [tableView reloadData];
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kMinimumCellCount;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Items Available."];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == eProducts) return kCellHeightForProducts;
    if (indexPath.section == eBillingAddress) return kCellHeightForAddress;
    if (indexPath.section == eOrderInfo) return kCellHeightForOrder;
    if (indexPath.section == eMap) return kCellHeightForMap;
    if (indexPath.section == eDeliveryAddress) return kCellHeightForDeliveryAddress;
    return kCellHeightForProducts;
    
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    UITableViewCell *cell;
    
    if (indexPath.section == eProducts) {
        static NSString *CellIdentifier = @"ProductCustomCell";
        ProductCustomCell *cell = (ProductCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            NSDictionary *product = dictProducts;
            if (NULL_TO_NIL([product objectForKey:@"signtemplate_title"]))
                cell.lblTemplateName.text = [product objectForKey:@"signtemplate_title"];
            if (NULL_TO_NIL([product objectForKey:@"print_type"]))
                cell.lblPrintType.text = [NSString stringWithFormat:@"Print Type : %@",[product objectForKey:@"print_type"]];
            if (NULL_TO_NIL([product objectForKey:@"size"]))
                cell.lblBoardSize.text = [NSString stringWithFormat:@"Board Size : %@",[product objectForKey:@"size"]];
            if (NULL_TO_NIL([product objectForKey:@"signtype_title"]))
                cell.lblSignType.text = [NSString stringWithFormat:@"Sign Type : %@",[product objectForKey:@"signtype_title"]];
            if (NULL_TO_NIL([product objectForKey:@"template_price"]))
                cell.lblPrice.text = [NSString stringWithFormat:@"Price : $%.02f",[[product objectForKey:@"template_price"] floatValue]];
            if (NULL_TO_NIL([dictProducts objectForKey:@"estimated_date"]))
                cell.lblDate.text = [product objectForKey:@"estimated_date"];
            if (NULL_TO_NIL([dictProducts objectForKey:@"sign_qty"]))
               cell.lblQuantity.text = [NSString stringWithFormat:@"Quantity : %@",[product objectForKey:@"sign_qty"]];
        
            if ( NULL_TO_NIL([product objectForKey:@"sub_total"]))
                cell.lblSubTotal.text = [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"sub_total"] floatValue]];
            if ( NULL_TO_NIL([product objectForKey:@"tax"]))
                cell.lblTax.text =  [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"tax"] floatValue]];
            if ( NULL_TO_NIL([product objectForKey:@"installation_charge"]))
                cell.lblInstallationChrg.text = [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"installation_charge"] floatValue]];;
            if ( NULL_TO_NIL([product objectForKey:@"grand_total"]))
                cell.lblGrandTotal.text = [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"grand_total"] floatValue]];
        
        NSInteger status = eTypePending;
        
        if (NULL_TO_NIL([dictOrderInfo objectForKey:@"order_status"]))
            status = [[dictOrderInfo objectForKey:@"order_status"] integerValue];
        
        if (status == eTypePending) {
             cell.lblEstimatedDaetHeading.text = @"Est. Installation Date :";
             cell.lblDate.text = [dictOrderInfo objectForKey:@"estimated_date"];
        }
        if (status == eTypeCancelled) {
            cell.lblEstimatedDaetHeading.text = @"Cancelled Date :";
            cell.lblDate.text = [dictOrderInfo objectForKey:@"order_canceldate"];
        }
        if (status == eTypeCompleted) {
            cell.lblEstimatedDaetHeading.text = @"Installed Date :";
            cell.lblDate.text = [dictOrderInfo objectForKey:@"installed_date"];
        }
       
        [cell.indicator stopAnimating];
        if ( NULL_TO_NIL([product objectForKey:@"signtemplate_image"])) {
                [cell.indicator startAnimating];
                NSString *url = [product objectForKey:@"signtemplate_image"];
                if (url.length)
               [cell.templateImage sd_setImageWithURL:[NSURL URLWithString:url]
                                      placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                 [cell.indicator stopAnimating];}];
            }
        
        cell.tagForBtn = indexPath.row;
        return cell;
    }
    else if (indexPath.section == eBillingAddress) {
        static NSString *CellIdentifier = @"AddressCell";
        AdderssCustomCell *cell = (AdderssCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if ( NULL_TO_NIL([dictAddress objectForKey:@"full_address"]))
            cell.lblAddress.text = [dictAddress objectForKey:@"full_address"];
        if ( NULL_TO_NIL([dictAddress objectForKey:@"bill_mobile"]))
            cell.lblPhoneNumber.text = [dictAddress objectForKey:@"bill_mobile"];
        if ( NULL_TO_NIL([dictAddress objectForKey:@"bill_name"]))
            cell.lblName.text = [dictAddress objectForKey:@"bill_name"];
        return  cell;
    }
    else if (indexPath.section == eDeliveryAddress) {
        static NSString *CellIdentifier = @"DeliveryAdderssCustomCell";
        DeliveryAdderssCustomCell *cell = (DeliveryAdderssCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if ( NULL_TO_NIL([dictMapInfo objectForKey:@"location_address"]))
            cell.lblAddress.text = [dictMapInfo objectForKey:@"location_address"];
        if ( NULL_TO_NIL([dictMapInfo objectForKey:@"location_title"]))
            cell.lblName.text = [dictMapInfo objectForKey:@"location_title"];
        
        return  cell;
    }
    else if (indexPath.section == eOrderInfo) {
        static NSString *CellIdentifier = @"OrderInfoCustomCell";
        OrderInfoCustomCell *cell = (OrderInfoCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if ( NULL_TO_NIL([dictOrderInfo objectForKey:@"estimated_date"]))
            cell.lblEstimatedDate.text = [dictOrderInfo objectForKey:@"estimated_date"];
        if ( NULL_TO_NIL([dictOrderInfo objectForKey:@"grand_total"]))
            cell.lblGrandTotal.text = [NSString stringWithFormat:@"$%.02f",[[dictOrderInfo objectForKey:@"grand_total"] floatValue]];
        if ( NULL_TO_NIL([dictOrderInfo objectForKey:@"order_date"]))
            cell.lblOrderDate.text = [dictOrderInfo objectForKey:@"order_date"];
        if ( NULL_TO_NIL([dictOrderInfo objectForKey:@"orderno"]))
            cell.lblOrderNo.text = [dictOrderInfo objectForKey:@"orderno"];
        if ( NULL_TO_NIL([dictOrderInfo objectForKey:@"total_qty"]))
            cell.lblQuantity.text = [dictOrderInfo objectForKey:@"total_qty"];
        if ( NULL_TO_NIL([dictOrderInfo objectForKey:@"transaction_id"]))
            cell.lblTransactionID.text = [dictOrderInfo objectForKey:@"transaction_id"];
        return  cell;
    }
    if (indexPath.section == eMap) {
        static NSString *CellIdentifier = @"MapCustomCell";
        MapCustomTableViewCell *cell = (MapCustomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (dictMapInfo)
            [cell loadMapDetailsWithDetails:dictMapInfo];
        
        
        return  cell;
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) {
        return nil;
    }
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor clearColor];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    lblTitle.backgroundColor = [UIColor whiteColor];
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.font = [UIFont fontWithName:CommonFontBold size:14];
    lblTitle.textColor = [UIColor blackColor];
    
    if (section == eProducts) {
        
        if (arrImages.count) {
            
            UIButton *btnGallery = [UIButton buttonWithType: UIButtonTypeCustom];
            [btnGallery addTarget: self action: @selector(showGallery) forControlEvents: UIControlEventTouchUpInside];
            btnGallery.translatesAutoresizingMaskIntoConstraints = NO;
            [vwHeader addSubview:btnGallery];
            [btnGallery setImage:[UIImage imageNamed:@"Gallery_Image.png"] forState:UIControlStateNormal];
            [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[btnGallery]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnGallery)]];
            
            [btnGallery addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                                   attribute:NSLayoutAttributeWidth
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeWidth
                                                                  multiplier:1.0
                                                                    constant:60]];
            
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:-5]];
            
            UILabel *lblCount = [UILabel new];
            lblCount.translatesAutoresizingMaskIntoConstraints = NO;
            [vwHeader addSubview:lblCount];
            lblCount.font = [UIFont fontWithName:CommonFontBold size:14];
            lblCount.textColor = [UIColor whiteColor];
            lblCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrImages.count];
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblCount
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant: 0]];
            
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblCount
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:-15]];
        }
    
    }
    
    
    if (section == eProducts)lblTitle.text = @"  PRODUCT";
    if (section == eBillingAddress)lblTitle.text = @"  BILLING ADDRESS";
    if (section == eDeliveryAddress)lblTitle.text = @"  DELIVERY ADDRESS";
    if (section == eOrderInfo)lblTitle.text = @"  ORDER DETAILS";
    if (section == eMap)lblTitle.text = @"  MAP";
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}


-(IBAction)showExpandedMap:(id)sender{
    
    if ( NULL_TO_NIL([dictMapInfo objectForKey:@"map_image"])) {
        NSString *url = [dictMapInfo objectForKey:@"map_image"];
        NSMutableArray *arrMapImage = [[NSMutableArray alloc] initWithObjects:[NSURL URLWithString:url], nil];
        [self presentGalleryWithImages:arrMapImage andIndex:0];
    }
    
}


-(IBAction)continueShoppingApplied{
    
       [[self navigationController]popToRootViewControllerAnimated:YES];
}

-(void)GoToMyAccount{
    
    MyAccountViewController *myAccount = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForMyAccount];
    [[self navigationController]pushViewController:myAccount animated:YES];

}
#pragma mark - Photo Browser & Deleagtes

-(IBAction)showGallery{
    
    [self presentGalleryWithImages:arrImages andIndex:0];
}



- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}



-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading..";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


-(void)showAlertWithTitle:(NSString*)title message:(NSString*)message{
    
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
