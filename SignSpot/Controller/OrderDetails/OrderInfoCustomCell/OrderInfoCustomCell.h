//
//  AdderssCustomCell.h
//  SignSpot
//
//  Created by Purpose Code on 27/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderInfoCustomCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblOrderNo;
@property (nonatomic,strong) IBOutlet UILabel *lblQuantity;
@property (nonatomic,strong) IBOutlet UILabel *lblEstimatedDate;
@property (nonatomic,strong) IBOutlet UILabel *lblGrandTotal;
@property (nonatomic,strong) IBOutlet UILabel *lblOrderDate;
@property (nonatomic,strong) IBOutlet UILabel *lblTransactionID;

@end
