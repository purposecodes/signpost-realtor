//
//  MyCartListingViewController.m
//  SignSpot
//
//  Created by Purpose Code on 27/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


#define kHeaderHeight               40
#define kFooterHeight               45
#define kSectionCount               1
#define kDefaultCellHeight          161
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kEmptyHeaderAndFooter       0

#import "MyCartListingViewController.h"
#import "Constants.h"
#import "ComposeBillingAddressViewController.h"
#import "PlotTemplateOnMapViewController.h"


@interface MyCartListingViewController (){
    
    IBOutlet UITableView *tableView;
    NSMutableArray *arrProducts;
    NSInteger grandTotal;
    IBOutlet UILabel *lblbadge;
    UIRefreshControl *refreshControl;
    BOOL isPageRefresing;
    BOOL isDataAvailable;
    BOOL isCheckOutEnabled;
}

@end

@implementation MyCartListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
        // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self loadAllProductsInCart];
}
-(void)setUp{
    
    arrProducts = [NSMutableArray new];
    isCheckOutEnabled = true;
    
    tableView.allowsSelection = NO;
    tableView.hidden = true;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [tableView addSubview:refreshControl];
    tableView.alwaysBounceVertical = YES;

    
    lblbadge.layer.cornerRadius = 10.0;
    lblbadge.layer.borderWidth = 1.f;
    lblbadge.layer.borderColor = [UIColor clearColor].CGColor;
    lblbadge.clipsToBounds = YES;
    lblbadge.hidden = false;
    lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }

    
}
-(void)loadAllProductsInCart{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    isPageRefresing = true;
    [arrProducts removeAllObjects];
    [self showLoadingScreenWithTitle:@"Loading.."];
    [APIMapper getAllProductsInCartWith:[User sharedManager].companyID userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        tableView.hidden = false;
        isPageRefresing = false;
        [refreshControl endRefreshing];
        [self updateSharedManagerWith:responseObject];
        [self hideLoadingScreen];
        [self getAllProductsInCartWith:responseObject];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         isCheckOutEnabled = false;
         tableView.hidden = false;
         isPageRefresing = false;
         [refreshControl endRefreshing];
         [tableView reloadData];
         [self hideLoadingScreen];
        
    }];
    
}

-(void)getAllProductsInCartWith:(NSDictionary*)responds{
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (responds && [[responds objectForKey:@"cartitems"] isKindOfClass:[NSArray class]]) {
            NSArray *carts = [responds objectForKey:@"cartitems"];
            for (NSDictionary *dict in carts)
                [arrProducts addObject:dict];
        }
         isDataAvailable = true;
        if (!arrProducts.count) isDataAvailable = false;
        
        if (responds && [[responds objectForKey:@"header"] objectForKey:@"grand_total"] ) {
            grandTotal = [[[responds objectForKey:@"header"] objectForKey:@"grand_total"] integerValue];
        }
        
        if ([[responds objectForKey:@"header"] objectForKey:@"checkout_enable"]) {
            isCheckOutEnabled = [[[responds objectForKey:@"header"] objectForKey:@"checkout_enable"] boolValue];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
            [tableView reloadData];
           
        });
    });

}

-(void)updateSharedManagerWith:(NSDictionary*)responseObject{
    
    // Update cart count for the user
    
    lblbadge.hidden = false;
    if ([[responseObject objectForKey:@"header"] objectForKey:@"cartcount"]) {
        
        NSInteger cartCount = [[[responseObject objectForKey:@"header"] objectForKey:@"cartcount"] integerValue];
        [User sharedManager].cartCount = cartCount;
        lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
        [Utility saveUserObject:[User sharedManager] key:@"USER"];
        lblbadge.hidden = false;
        if ([User sharedManager].cartCount <= 0) {
            lblbadge.hidden = true;
        }

    }if ([[responseObject objectForKey:@"header"] objectForKey:@"grand_total"]) {
        grandTotal = [[[responseObject objectForKey:@"header"] objectForKey:@"grand_total"] integerValue];
    }
    
}

-(void)refreshData{
    
    [self loadAllProductsInCart];
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    if (!isDataAvailable) return kMinimumCellCount;
    return [arrProducts count];
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Items Available."];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return kDefaultCellHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable)  return nil;
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor whiteColor];
    vwHeader.layer.borderWidth = 1.f;
    vwHeader.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblTitle]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.text = [NSString stringWithFormat:@"My Cart (%lu)",(unsigned long)arrProducts.count];
    lblTitle.font = [UIFont fontWithName:CommonFontBold size:14];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor blackColor];
    return vwHeader;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (!isDataAvailable)  return nil;
    UIView *vwFooter = [UIView new];
    vwFooter.backgroundColor = [UIColor getBackgroundOffWhiteColor];
    UIButton* btnCart = [UIButton new];
    btnCart.backgroundColor = [UIColor blackColor];
    btnCart.translatesAutoresizingMaskIntoConstraints = NO;
    [btnCart setTitle:[NSString stringWithFormat:@"$%ld",(long)grandTotal] forState:UIControlStateNormal];
    [btnCart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCart.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [vwFooter addSubview:btnCart];
    
    UIButton* btnBuy = [UIButton new];
    btnBuy.backgroundColor = [UIColor getThemeColor];
    btnBuy.translatesAutoresizingMaskIntoConstraints = NO;
    [btnBuy setTitle:@"CHECK OUT" forState:UIControlStateNormal];
    [btnBuy setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnBuy.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnBuy addTarget:self action:@selector(checkOutAction) forControlEvents:UIControlEventTouchUpInside];
    [vwFooter addSubview:btnBuy];
    
    
    NSDictionary *views = NSDictionaryOfVariableBindings(btnCart,btnBuy);
    NSArray *horizontalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[btnCart]-5-[btnBuy]-0-|" options:0 metrics:nil views:views];
    NSArray *equalWidthConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[btnCart(==btnBuy)]" options:0 metrics:nil views:views];
    NSArray *verticalConstraints1 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnCart]-0-|" options:0 metrics:nil views:views];
    NSArray *verticalConstraints2 =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[btnBuy]-0-|" options:0 metrics:nil views:views];
    [vwFooter addConstraints:horizontalConstraints];
    [vwFooter addConstraints:equalWidthConstraints];
    [vwFooter addConstraints:verticalConstraints1];
    [vwFooter addConstraints:verticalConstraints2];
    
    return vwFooter;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    

    if (!isDataAvailable)  return kEmptyHeaderAndFooter;
    return kHeaderHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (!isDataAvailable)  return kEmptyHeaderAndFooter;
    return  kFooterHeight;
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    
    static NSString *CellIdentifier = @"CartListingCell";
    CartListingCell *cell = (CartListingCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CartListingCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.vwBackground.backgroundColor = [UIColor whiteColor];
    cell.vwBackground.layer.borderWidth = 1.f;
    cell.vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    cell.backgroundColor = [UIColor clearColor];
    cell.tagForCell = indexPath.row;
    cell.delegate = self;

    if (indexPath.row < arrProducts.count) {
        
        NSDictionary *productInfo = arrProducts[[indexPath row]];
        if (productInfo && [productInfo objectForKey:@"signtemplate_title"])
             cell.lblSignTemplateName.text = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"signtemplate_title"]];
        
        if (productInfo && [productInfo objectForKey:@"signtype_title"])
            cell.lblSignType.text = [NSString stringWithFormat:@"Sign Type : %@",[productInfo objectForKey:@"signtype_title"]];
        
        if (productInfo && [productInfo objectForKey:@"size"])
            cell.lblSignSize.text = [NSString stringWithFormat:@"Board Size : %@",[productInfo objectForKey:@"size"]];
        
        if (productInfo && [productInfo objectForKey:@"print_type"])
            cell.lblPrintType.text = [NSString stringWithFormat:@"Print Type : %@",[productInfo objectForKey:@"print_type"]];
        
        if (productInfo && [productInfo objectForKey:@"sign_qty"])
            cell.lblQuantity.text = [NSString stringWithFormat:@"Qantity : %@",[productInfo objectForKey:@"sign_qty"]];
        
        if (productInfo && [productInfo objectForKey:@"sub_total"])
            cell.lblPrice.text = [NSString stringWithFormat:@"$%@",[productInfo objectForKey:@"sub_total"]];
        
        if (productInfo && [productInfo objectForKey:@"total_review"])
            cell.lblReviewCount.text = [NSString stringWithFormat:@"(%@)",[productInfo objectForKey:@"total_review"]];
        
        if (productInfo && [productInfo objectForKey:@"rating"])
            cell.starRatingImage.rating = [[productInfo objectForKey:@"rating"] floatValue];
    
        NSString *urlImage;
        if (productInfo && [productInfo objectForKey:@"signtemplate_image"])
             urlImage = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"signtemplate_image"]];

        [cell.indicator stopAnimating];
        if (urlImage.length) {
            [cell.indicator startAnimating];
            [cell.imgTemplate sd_setImageWithURL:[NSURL URLWithString:urlImage]
                                placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           [cell.indicator stopAnimating];
                                           
                                           [UIView transitionWithView:cell.imgTemplate
                                                             duration:.5f
                                                              options:UIViewAnimationOptionTransitionCrossDissolve
                                                           animations:^{
                                                               cell.imgTemplate.image = image;
                                                           } completion:nil];
                                           
                                       }];
        }

    }
    
    
    return cell;
    
}

#pragma mark - Cart Cell Deleagtes Actions

-(void)mapButtonTappedWithTag:(NSInteger)tag{
    
   [Utility removeAViewControllerFromNavStackWithType:([PlotTemplateOnMapViewController class]) from:[self.navigationController viewControllers]];
    PlotTemplateOnMapViewController *mapView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MapDetailsAndListingStoryBoard Identifier:StoryBoardIdentifierForTemplateOnMapPage];
    mapView.isCumingFromCart = true;
    if (tag < arrProducts.count) {
        
        NSDictionary *details = arrProducts[tag];
        NSMutableDictionary *dictMapInfo = [NSMutableDictionary new];
        if (details && [details objectForKey:@"cart_id"])
            [dictMapInfo setObject:[details objectForKey:@"cart_id"] forKey:@"cart_id"];
        if (details && [details objectForKey:@"location_address"])
            [dictMapInfo setObject:[details objectForKey:@"location_address"] forKey:@"location_address"];
        if (details && [details objectForKey:@"location_details"])
            [dictMapInfo setObject:[details objectForKey:@"location_details"] forKey:@"location_details"];
        if (details && [details objectForKey:@"location_latitude"])
            [dictMapInfo setObject:[details objectForKey:@"location_latitude"] forKey:@"location_latitude"];
        if (details && [details objectForKey:@"location_longitude"])
            [dictMapInfo setObject:[details objectForKey:@"location_longitude"] forKey:@"location_longitude"];
        if (details && [details objectForKey:@"location_title"])
            [dictMapInfo setObject:[details objectForKey:@"location_title"] forKey:@"location_title"];
        if (details && [details objectForKey:@"map_id"])
            [dictMapInfo setObject:[details objectForKey:@"map_id"] forKey:@"map_id"];
        if (details && [details objectForKey:@"map_path"])
            [dictMapInfo setObject:[details objectForKey:@"map_path"] forKey:@"map_path"];
        if (details && [details objectForKey:@"sign_qty"])
            [dictMapInfo setObject:[details objectForKey:@"sign_qty"] forKey:@"sign_qty"];
 
        mapView.savedMapDetails = dictMapInfo;
    }
    [[self navigationController]pushViewController:mapView animated:YES];
    
    
    
}


-(void)deleteSelectedCellWithTag:(NSInteger)tag{
    
    if (tag < arrProducts.count) {
        NSDictionary *details = arrProducts[tag];
        if ([details objectForKey:@"cart_id"]) {
            NSString *cartID = [details objectForKey:@"cart_id"];
            [self showLoadingScreenWithTitle:@"Deleting.."];
            [APIMapper removeItemFromCartWith:cartID userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [self hideLoadingScreen];
                [self loadAllProductsInCart];
               
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                if (error) {
                    NSString *errorString = [error localizedDescription];
                    [[[UIAlertView alloc] initWithTitle:@"Review" message:errorString  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                }
                 [self hideLoadingScreen];
            }];
        }
    }
}

#pragma mark - Common Actions

-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


-(IBAction)checkOutAction{
    
    if (!isCheckOutEnabled) {
          [[[UIAlertView alloc] initWithTitle:@"CHECK OUT" message:@"Please add Map to all items in cart or Delete those items without Map." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    NSString *cartIDs = [self getAllCartIDs];
    ComposeBillingAddressViewController *billingAdres = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForCreateBillingAddress];
    billingAdres.cartIDs = cartIDs;
    [[self navigationController]pushViewController:billingAdres animated:YES];
    
}

-(NSString*)getAllCartIDs{
    
    NSMutableString * cartIDs = [NSMutableString new];
    for (NSDictionary *dict in arrProducts) {
        
        if ([dict objectForKey:@"cart_id"]) {
            [cartIDs appendFormat:@"%@,",[dict objectForKey:@"cart_id"]];
        }
    }
    return cartIDs;
    
}
-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
