//
//  MyCartListingViewController.h
//  SignSpot
//
//  Created by Purpose Code on 27/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartListingCell.h"

@interface MyCartListingViewController : UIViewController <CartCellDelegate>

@end
