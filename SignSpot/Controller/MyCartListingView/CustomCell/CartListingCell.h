//
//  CustomCellForType.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"


@protocol CartCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Share' Button.
 */

-(void)mapButtonTappedWithTag:(NSInteger)tag;

/*!
 *This method is invoked when user taps the 'Delete' Button.
 */


-(void)deleteSelectedCellWithTag:(NSInteger)tag;



@end


@interface CartListingCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *vwBackground;
@property (nonatomic,weak) IBOutlet UIImageView *imgTemplate;
@property (nonatomic,weak) IBOutlet UILabel *lblSignTemplateName;
@property (nonatomic,weak) IBOutlet UILabel *lblSignType;
@property (nonatomic,weak) IBOutlet UILabel *lblSignSize;
@property (nonatomic,weak) IBOutlet UILabel *lblPrintType;
@property (nonatomic,weak) IBOutlet UILabel *lblQuantity;
@property (nonatomic,weak) IBOutlet UILabel *lblPrice;
@property (nonatomic,weak) IBOutlet UILabel *lblReviewCount;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic,weak) IBOutlet  EDStarRating *starRatingImage;
@property (nonatomic,weak)  id<CartCellDelegate>delegate;
@property (nonatomic,assign) NSInteger tagForCell;



@end
