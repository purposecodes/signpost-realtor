//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CartListingCell.h"

@implementation CartListingCell

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
    _starRatingImage.starImage = [UIImage imageNamed:@"RatingStarGrey.png"];
    _starRatingImage.starHighlightedImage = [UIImage imageNamed:@"RatingStarFilled.png"];
    _starRatingImage.maxRating = 5.0;
    _starRatingImage.horizontalMargin = 0;
    _starRatingImage.editable = NO;
    _starRatingImage.displayMode=EDStarRatingDisplayHalf;

}


-(IBAction)deleteButtonTapped:(UIGestureRecognizer*)gesture{
    
    if (gesture && gesture.view) {
        
        UIView *view = gesture.view;
        [[self delegate]deleteSelectedCellWithTag:self.tagForCell];
        [UIView animateWithDuration:.1 animations:^{
            view.layer.backgroundColor = [UIColor getThemeColor].CGColor;
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:.1 animations:^{
                view.layer.backgroundColor = [UIColor whiteColor].CGColor;
            } completion:^(BOOL finished) {
                
            }];
            
        }];
    }
}

-(IBAction)mapButtonTapped:(UIGestureRecognizer*)gesture{
    
     if (gesture && gesture.view) {
         UIView *view = gesture.view;
         [[self delegate]mapButtonTappedWithTag:self.tagForCell];
         [UIView animateWithDuration:.1 animations:^{
             view.layer.backgroundColor = [UIColor getThemeColor].CGColor;
         } completion:^(BOOL finished) {
             
             [UIView animateWithDuration:.1 animations:^{
                 view.layer.backgroundColor = [UIColor whiteColor].CGColor;
             } completion:^(BOOL finished) {
                 
             }];
             
         }];

     }
    
   
}






@end
