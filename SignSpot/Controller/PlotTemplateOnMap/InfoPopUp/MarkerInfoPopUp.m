//
//  ReviewPopUp.m
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


typedef enum{
    
    eCellPlaceName = 0,
    eCellAddress = 1,
    eCellDescription = 2,
    eCellImage = 3,

}ETemplateInfo;



#define kCellHeightNormal           80;
#define kCellHeightForDescription   120;
#define kCellHeightForImage         70;
#define kCellHeightForAddress       120;
#define kHeightForHeader            45;
#define kHeightForFooter            .001;
#define kMaxReviewLength            150
#define kMaxReviewTitleLength       50


#import "MarkerInfoPopUp.h"
#import "Constants.h"
#import "CustomCellForMarkerInfo.h"
#import "MultipleImageCollectionViewCell.h"
#import "CustomeImagePicker.h"
#import "PhotoBrowser.h"

@interface MarkerInfoPopUp() <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,CustomeImagePickerDelegate,PhotoBrowserDelegate>{
    
    NSInteger indexForTextFieldNavigation;
    PhotoBrowser *photoBrowser;

   
}

@end

@implementation MarkerInfoPopUp



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setUp{
    
    [self loadUI];
    [self setUpFooter];
}

-(void)loadUI{
    
    
    
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
    indexForTextFieldNavigation = 0;
    
    // Tableview Setup
    
    tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    tableView.translatesAutoresizingMaskIntoConstraints = NO;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableView.bounds.size.width, 0.01f)];
    tableView.layer.borderColor = [UIColor clearColor].CGColor;
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableView.bounds.size.width, 0.01f)];
    tableView.backgroundColor = [UIColor colorWithRed:212/255.f green:225/255.f blue:232.f/255 alpha:1];
    tableView.allowsSelection = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableView];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[tableView]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tableView)]];
    
    centerConstraint = [NSLayoutConstraint constraintWithItem:tableView
                                                    attribute:NSLayoutAttributeCenterY
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                    attribute:NSLayoutAttributeCenterY
                                                   multiplier:1.0
                                                     constant:-25.0];
    
    [self addConstraint:centerConstraint];

    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:tableView
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0
                                                      constant:420]];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    // Tap Gesture SetUp
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopUp)];
    gesture.numberOfTapsRequired = 1;
    gesture.delegate = self;
    [self addGestureRecognizer:gesture];
    
 
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifierForMarker";
    CustomCellForMarkerInfo *cell = (CustomCellForMarkerInfo *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellForMarkerInfo" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.lblTitle.hidden = false;
    cell.txtDescription.hidden = true;
    cell.txtField.hidden = true;
    cell.txtField.tag = indexPath.row ;
    cell.btnUpload.hidden = true;
    cell.collectionView.delegate = self;
    cell.collectionView.hidden = true;
    
    if (indexPath.row == eCellPlaceName) {
        
        cell.txtField.hidden = false;
        cell.lblTitle.text = @"Place Name";
        cell.txtField.text = _strPlaceName;
    }
    else if (indexPath.row == eCellAddress) {
        
        cell.txtDescription.hidden = false;
        cell.lblTitle.text = @"Address";
        cell.txtDescription.text = _strAddress;
    }
    else if (indexPath.row == eCellDescription) {
        
        cell.lblTitle.text = @"Description";
        cell.txtDescription.hidden = false;
        cell.txtDescription.text = _strDescription;
    }
    else if (indexPath.row == eCellImage) {
        [cell.collectionView reloadData];
        [cell.btnUpload setTitle:@"UPLOAD IMAGE" forState:UIControlStateNormal];
        [cell.btnUpload setTitle:@"UPLOAD IMAGE" forState:UIControlStateSelected];
        cell.lblTitle.hidden = true;
        cell.btnUpload.hidden = false;
        if (_arrUploadImages.count)cell.collectionView.hidden = false;
        [cell.btnUpload addTarget:self action:@selector(showImagePicker:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == eCellDescription)
        return kCellHeightForDescription;
    
    if (indexPath.row == eCellAddress)
        return kCellHeightForAddress;
    
    if (indexPath.row == eCellImage){
        if (_arrUploadImages.count) {
            return 155;
        }
        return 55;
    }
    
    
    return kCellHeightNormal;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return  kHeightForFooter;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor getThemeColor];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    
    lblTitle.text = @"ADD MARKER";
    
    lblTitle.font = [UIFont fontWithName:CommonFont size:16];
    lblTitle.textColor = [UIColor whiteColor];
    
    return vwHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  1;
}

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    return  _arrUploadImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MultipleImageCollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"MultipleImageCollectionViewCell" forIndexPath:indexPath];
    if (indexPath.row < _arrUploadImages.count) {
        cell.imgThumb.image = _arrUploadImages[indexPath.row];
    }
    cell.btnThumbDelete.tag = indexPath.row;
    [cell.btnThumbDelete addTarget:self action:@selector(deleteSelectedImage:) forControlEvents:UIControlEventTouchUpInside];
  
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(95, 100);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self presentGalleryWithImages:_arrUploadImages andIndex:indexPath.row];
}

#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        CustomCellForMarkerInfo *cell = (CustomCellForMarkerInfo*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.txtField.tag data:textField.text];
    }
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    }
    indexForTextFieldNavigation = indexPath.row;
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
    }
    else if([[textField text] length] >= kMaxReviewTitleLength)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return true;
}



#pragma mark - UITextView delegate methods


-(BOOL)textViewShouldBeginEditing:(UITextView *)textField {
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    }
    indexForTextFieldNavigation = indexPath.row;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    return YES;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textField{
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    [tableView setContentOffset:contentOffset animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textField{
    
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textField{
    
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        CustomCellForMarkerInfo *cell = (CustomCellForMarkerInfo*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.txtField.tag data:textField.text];
    }
    
    
    return YES;
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
    }
    else if([[textView text] length] >= kMaxReviewLength)
    {
        return NO;
    }
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:tableView])
        return NO;
    return YES;
}

#pragma mark - Image Picker Methods

-(IBAction)showImagePicker:(id)sender{
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    CustomeImagePicker *cip = [[CustomeImagePicker alloc] init];
    cip.delegate = self;
    [cip setHideSkipButton:NO];
    [cip setHideNextButton:NO];
    [cip setMaxPhotos:MAX_ALLOWED_PICK];
    cip.isPhotos = true;
    [delegate.window.rootViewController presentViewController:cip animated:YES completion:^{
    }];
    
}

-(void)imageSelected:(NSArray*)arrayOfGallery isPhoto:(BOOL)isPhoto;
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLoadingScreen];
        }); // Main Queue to Display the Activity View
        __block NSInteger imageCount = 0;
        
        for(NSString *imageURLString in arrayOfGallery)
        {
            // Asset URLs
            ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
            [assetsLibrary assetForURL:[NSURL URLWithString:imageURLString] resultBlock:^(ALAsset *asset) {
                ALAssetRepresentation *representation = [asset defaultRepresentation];
                CGImageRef imageRef = [representation fullScreenImage];
                UIImage *image = [UIImage imageWithCGImage:imageRef];
                if (imageRef) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (!_arrUploadImages) _arrUploadImages = [NSMutableArray new];
                        [_arrUploadImages addObject:image];
                    });
                    imageCount ++;
                    
                    if (imageCount == arrayOfGallery.count) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [tableView performSelector:@selector(reloadData) withObject:self afterDelay:0];
                            [self hideLoadingScreen];
                        });
                    }
                }
                
                // Valid Image URL
            } failureBlock:^(NSError *error) {
            }];
        } // All Images I got
        
        
    });
    
    
}

-(IBAction)deleteSelectedImage:(UIButton*)sender{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Delete"
                                          message:@"Do you want to delete the image?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction
                               actionWithTitle:@"DELETE"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction *action)
                               {
                                   
                                   if (sender.tag < _arrUploadImages.count) {
                                       [_arrUploadImages removeObjectAtIndex:sender.tag];
                                   }
                                   [tableView reloadData];
                                   
                                   
                               }];
    
    UIAlertAction *cancel = [UIAlertAction
                               actionWithTitle:@"CANCEL"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                               }];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    [delegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
    }];

}




#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}


#pragma mark - Common Methods


-(void)getTextFromField:(NSInteger)type data:(NSString*)string{
    
    switch (type) {
        case eCellPlaceName:
            _strPlaceName = string;
            break;
        case eCellAddress:
            _strAddress = string;
            break;
        case eCellDescription:
             _strDescription = string;
            break;
        default:
            break;
    }

}

-(void)createInputAccessoryView{
   
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, 40.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 0.8];
    
    UIButton *btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    [btnPrev setFrame: CGRectMake(0.0, 0.0, 80.0, 40.0)];
    [btnPrev setTitle: @"PREVIOUS" forState: UIControlStateNormal];
    [btnPrev setBackgroundColor: [UIColor getHeaderOffBlackColor]];
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 0.0f, 80.0f, 40.0f)];
    [btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor getHeaderOffBlackColor]];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 1.0f, 80.0f, 38.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    
    btnPrev.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    btnNext.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}

-(void)gotoPrevTextfield{
    
    if (indexForTextFieldNavigation - 1 < 0) indexForTextFieldNavigation = 0;
    else indexForTextFieldNavigation -= 1;
    [self gotoTextField];
    
}

-(void)gotoNextTextfield{
    
    if (indexForTextFieldNavigation + 1 < 3) indexForTextFieldNavigation += 1;
    [self gotoTextField];
}

-(void)gotoTextField{
    
    CustomCellForMarkerInfo *nextCell = (CustomCellForMarkerInfo *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
    if (!nextCell) {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        nextCell = (CustomCellForMarkerInfo *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
        
    }
    if (indexForTextFieldNavigation == 0) {
        [nextCell.txtField becomeFirstResponder];
    }else{
        [nextCell.txtDescription becomeFirstResponder];
    }
    
    
}



-(void)doneTyping{
    [self endEditing:YES];
    
}


-(void)saveMarker{
    
    [self endEditing:YES];
    [self checkAllFieldsAreValidOnSuccess:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[NSThread currentThread] isMainThread]){
                if (self.delegate) {
                    if ([self.delegate respondsToSelector:@selector(saveMarkerWithDetails:address:description:image:)])
                        [[self delegate]saveMarkerWithDetails:_strPlaceName address:_strAddress description:_strDescription image:_arrUploadImages];
                }
                
            }
            else{
            }
        });
     
    } failure:^(NSString *error) {
        
        // Field validation error block
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add Marker" message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
       
    }];

}

-(void)removeMarker{
    
    [self endEditing:YES];
    if ([self.delegate respondsToSelector:@selector(removeMarkerClickedFromPopUp)])
        [[self delegate]removeMarkerClickedFromPopUp];
}


-(IBAction)closePopUp{
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(closeInfoPopUpAfterADelay:)])
            [[self delegate]closeInfoPopUpAfterADelay:.2];
    }


}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}

-(void)setUpFooter{
    
    UIView *vwFooter = [UIView new];
    vwFooter.backgroundColor = [UIColor colorWithRed:212/255.f green:225/255.f blue:232.f/255 alpha:1];
    vwFooter.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:vwFooter];
     NSArray *width =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[vwFooter]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwFooter)];
    [self addConstraints:width];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:vwFooter
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:tableView
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0
                                                                  constant:0.0]];
    [vwFooter addConstraint:[NSLayoutConstraint constraintWithItem:vwFooter
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0
                                                      constant:50.0]];
    
    UIButton* btnSave = [UIButton new];
    btnSave.backgroundColor = [UIColor getThemeColor];
    btnSave.translatesAutoresizingMaskIntoConstraints = NO;
    [btnSave setTitle:@"SAVE" forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSave.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnSave addTarget:self action:@selector(saveMarker) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* btnCancel = [UIButton new];
    btnCancel.backgroundColor = [UIColor getThemeColor];
    btnCancel.translatesAutoresizingMaskIntoConstraints = NO;
    [btnCancel setTitle:@"REMOVE" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnCancel addTarget:self action:@selector(removeMarker) forControlEvents:UIControlEventTouchUpInside];
    
   // [vwFooter addSubview:btnCancel];
    [vwFooter addSubview:btnSave];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(btnSave);
    
    NSArray *horizontalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[btnSave]-5-|" options:0 metrics:nil views:views];
    NSArray *verticalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[btnSave]-5-|" options:0 metrics:nil views:views];
    
    [vwFooter addConstraints:horizontalConstraints];
    [vwFooter addConstraints:verticalConstraints];
    
}

-(void)checkAllFieldsAreValidOnSuccess:(void (^)())success failure:(void (^)(NSString *error))failure{
    
    NSString *errorMessage;
    BOOL isValid = false;
    if ((_strDescription.length) && (_strPlaceName.length) && (_strAddress.length) > 0){
        
        isValid = true;
        
    }else{
        
        errorMessage = @"Please fill all the fields.";
    }
    if (isValid)success();
    else failure(errorMessage);
    
}



-(void)dealloc{
    
}

@end
