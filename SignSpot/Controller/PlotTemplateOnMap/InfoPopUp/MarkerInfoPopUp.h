//
//  ReviewPopUp.h
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@protocol MarkerInfoPopUpDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Close' Button.
 */

-(void)closeInfoPopUpAfterADelay:(float)delay;

/*!
 *This method is invoked when user tap "Save Marker"
 */

-(void)saveMarkerWithDetails:(NSString*)name address:(NSString*)address description:(NSString*)description image:(NSMutableArray*)images;

/*!
 *This method is invoked when user tap "Remove Marker"
 */

-(void)removeMarkerClickedFromPopUp;





@end



@interface MarkerInfoPopUp : UIView <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UITextViewDelegate,UITextFieldDelegate,EDStarRatingProtocol>{
    
    UITableView *tableView;
    float constantForCenterConstraint;
    NSLayoutConstraint *centerConstraint;
    BOOL isAlreadyLifted;
    UIView *inputAccView;
    
}

@property (nonatomic,weak)  id<MarkerInfoPopUpDelegate>delegate;
@property (nonatomic,strong) NSString *strAddress;
@property (nonatomic,strong) NSString *strPlaceName;
@property (nonatomic,strong) NSString *strDescription;
@property (nonatomic,retain) NSMutableArray *arrUploadImages;


-(void)setUp;

@end
