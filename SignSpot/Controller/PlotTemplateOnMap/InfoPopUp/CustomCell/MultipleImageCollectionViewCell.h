//
//  MultipleImageCollectionViewCell.h
//  SignPost
//
//  Created by Purpose Code on 29/12/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultipleImageCollectionViewCell : UICollectionViewCell

 @property (nonatomic,strong) IBOutlet UIImageView *imgThumb;
 @property (nonatomic,strong) IBOutlet UIButton *btnThumbDelete;


@end
