//
//  CustomCellForRateStar.h
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"


@interface CustomCellForMarkerInfo : UITableViewCell

@property (nonatomic,weak) IBOutlet  UITextField *txtField;
@property (nonatomic,weak) IBOutlet  UILabel *lblTitle;
@property (nonatomic,weak) IBOutlet  UITextView *txtDescription;
@property (nonatomic,weak) IBOutlet  UIButton *btnUpload;
@property (nonatomic,weak) IBOutlet  UICollectionView *collectionView;


@end
