//
//  CustomCellForRateStar.m
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CustomCellForMarkerInfo.h"

@implementation CustomCellForMarkerInfo{
    
}

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}

-(void)setUp{
    

    UINib *cellNib = [UINib nibWithNibName:@"MultipleImageCollectionViewCell" bundle:nil];
    [_collectionView registerNib:cellNib forCellWithReuseIdentifier:@"MultipleImageCollectionViewCell"];
    
    self.txtDescription.layer.borderWidth = 1.0f;
    self.txtDescription.layer.borderColor = [[UIColor getSeperatorColor] CGColor];
    
    self.txtField.layer.borderWidth = 1.0f;
    self.txtField.layer.borderColor = [[UIColor getSeperatorColor] CGColor];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
