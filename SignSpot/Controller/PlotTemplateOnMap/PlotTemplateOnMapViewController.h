//
//  PlotTemplateOnMapViewController.h
//  SignSpot
//
//  Created by Purpose Code on 26/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SARMapDrawView.h"
#import <CoreLocation/CoreLocation.h>

@interface PlotTemplateOnMapViewController : UIViewController<CLLocationManagerDelegate,UISearchBarDelegate,GMSMapViewDelegate>


@property (nonatomic,assign) NSInteger cartID;
@property (nonatomic,strong) NSDictionary *savedMapDetails; // if from cart
@property (nonatomic,assign) BOOL  isCumingFromCart;
@property (nonatomic,assign) NSInteger  numberOfBoardsRequired;

@end
