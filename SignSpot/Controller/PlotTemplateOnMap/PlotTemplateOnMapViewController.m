//
//  PlotTemplateOnMapViewController.m
//  SignSpot
//
//  Created by Purpose Code on 26/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })


#define MERCATOR_RADIUS 85445659.44705395
#define MAX_GOOGLE_LEVELS 20
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define kSuccessCode 200

#import "PlotTemplateOnMapViewController.h"
#import "Constants.h"
#import "MyCartListingViewController.h"
#import "MarkerInfoPopUp.h"
#import "ComposeBillingAddressViewController.h"
#import "UICircularSlider.h"

@import GoogleMaps;

@interface PlotTemplateOnMapViewController ()<MarkerInfoPopUpDelegate,UIGestureRecognizerDelegate>{
    
    IBOutlet UILabel *lblbadge;
    IBOutlet UILabel *lblLocName;
    IBOutlet GMSMapView *mapView;
    IBOutlet UIView *vwNavigation;
    IBOutlet SARMapDrawView *mapDrawView;
    IBOutlet UIButton *btnAddToCart;
    IBOutlet UIButton *btnAddInfo;
    IBOutlet UISegmentedControl *segmentControll;
    IBOutlet UIView *vwSliderOverlay;
    IBOutlet UICircularSlider *circularSlider;
    
    IBOutlet NSLayoutConstraint *widthConstraint;
    IBOutlet UIView *vwCartParent;
    IBOutlet UIView *vwCheckOutParent;
    IBOutlet UIView *vwBtnHolder;
    IBOutlet UIButton *btnPen;
    
    UILongPressGestureRecognizer *longPressGesture;
    NSMutableArray *arrMarker;
    CLLocationManager* locationManager;
    
    GMSCircle *circle;
    GMSPolyline *customlineDraw;
    MarkerInfoPopUp *markerInfoPopUp;
    GMSMarker *activeMarker;
    float zoomLevel;
    NSInteger mapID;
    NSInteger zoomAdjustment;
    UIImage *screenShot;
    BOOL shouldContinueSave;
    BOOL shouldByPassSave;
    
    /*! Marker details to show in popup !*/
    NSString *strMarkerPlaceName;
    NSString *strMarkerAddress;
    NSString *strMarkerDescription;
    NSMutableArray *imgToUpload;
    
}

@property (nonatomic,strong)  GMSPolygon *drawnPolygon;
@property (nonatomic,strong)  GMSPolyline *drawnPolyline;


@end

@implementation PlotTemplateOnMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setUp];
    [self getCurrentUserLocation];
    [self addGestureRecogniserToMapView];
    [self drawPolyLine];
    // Do any additional setup after loading the view.
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    lblbadge.hidden = false;
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }
     lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
}

-(void)setUp{
    
    imgToUpload = [NSMutableArray new];
    vwSliderOverlay.hidden = true;
    circularSlider.minimumValue = 1;
    circularSlider.maximumValue = 360;
    circularSlider.continuous = YES;
    
    self.navigationController.navigationBarHidden = true;
    zoomAdjustment = 2;
    zoomLevel = kGMSMaxZoomLevel - zoomAdjustment;
    strMarkerPlaceName   = [NSString new];
    strMarkerAddress     = [NSString new];
    strMarkerDescription = [NSString new];
    arrMarker = [NSMutableArray new];
    
    lblbadge.layer.cornerRadius = 10.0;
    lblbadge.layer.borderWidth = 1.f;
    lblbadge.layer.borderColor = [UIColor clearColor].CGColor;
    lblbadge.clipsToBounds = YES;
    [btnAddInfo setEnabled:false];
    vwNavigation.layer.borderWidth = 1.f;
    vwNavigation.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    segmentControll.backgroundColor = [UIColor whiteColor];
    segmentControll.layer.cornerRadius = 5;

    if (_isCumingFromCart){
    
        [self loadSavedMapFromCart];
        [self changeProperties];
   }
 
   
}

- (IBAction)pickPlace:(id)sender {
    
    CLLocationCoordinate2D center;
//    if (marker)
//    center = CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude);
//    else
    center = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                  center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                  center.longitude - 0.001);
    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                         coordinate:southWest];
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
    GMSPlacePicker *_placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        
        [self clearMap];
        float latitude = place.coordinate.latitude;
        float longitude = place.coordinate.longitude;
        if (place.formattedAddress.length){
            strMarkerAddress =  place.formattedAddress;
            
            NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:strMarkerAddress];
            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineSpacing:5];
            [attrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, strMarkerAddress.length)];
            lblLocName.attributedText = attrString;
            
            zoomLevel = kGMSMaxZoomLevel - zoomAdjustment;;
            
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                                    longitude:longitude
                                                                         zoom:zoomLevel
                                                                      bearing:10
                                                                 viewingAngle:10];
            mapView.camera = camera;
        }
        
        
    }];
}

-(IBAction)changeMapView{
    
    if ([segmentControll selectedSegmentIndex] == 0) mapView.mapType =  kGMSTypeSatellite;
    else mapView.mapType =  kGMSTypeNormal;
    
    
}
#pragma mark - Load saved map from webservice

-(void)changeProperties{
    
    [btnAddToCart setTitle:@"UPDATE CART" forState:UIControlStateNormal];
    vwCheckOutParent.hidden = true;
    [vwBtnHolder removeConstraint:widthConstraint];
    widthConstraint = [NSLayoutConstraint constraintWithItem:vwCartParent
                                                   attribute:NSLayoutAttributeWidth
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:vwBtnHolder
                                                   attribute:NSLayoutAttributeWidth
                                                  multiplier:1
                                                    constant:0];
    [vwBtnHolder addConstraint:widthConstraint];
    
}

-(void)loadSavedMapFromCart{
    
    NSString *address;
    float latitude;
    float longitude;
    
    if ( NULL_TO_NIL([_savedMapDetails objectForKey:@"cart_id"])) {
        _cartID = [[_savedMapDetails objectForKey:@"cart_id"] integerValue];
    }
    
    if ( NULL_TO_NIL( [_savedMapDetails objectForKey:@"map_id"]))
        mapID = [[_savedMapDetails objectForKey:@"map_id"] integerValue];
    
    if ( NULL_TO_NIL( [_savedMapDetails objectForKey:@"sign_qty"]))
        _numberOfBoardsRequired = [[_savedMapDetails objectForKey:@"sign_qty"] integerValue];
    
    
    
    if (mapID > 0) {
        
        // Map enabled . Else  Map Not added for that item in cart.
        
        if (NULL_TO_NIL([_savedMapDetails objectForKey:@"map_path"])) {
            NSString *polylinePath =  [_savedMapDetails objectForKey:@"map_path"];
            if (polylinePath.length)
                [self drawPolyLineFromSavedPath:polylinePath];
        }
       
        if ( NULL_TO_NIL([_savedMapDetails objectForKey:@"location_address"]))
            address = [_savedMapDetails objectForKey:@"location_address"];
        
        if (NULL_TO_NIL([_savedMapDetails objectForKey:@"location_latitude"]))
            latitude = [[_savedMapDetails objectForKey:@"location_latitude"] floatValue];
        
        if (NULL_TO_NIL([_savedMapDetails objectForKey:@"location_longitude"]))
            longitude = [[_savedMapDetails objectForKey:@"location_longitude"]floatValue];
        
        if (NULL_TO_NIL([_savedMapDetails objectForKey:@"location_address"]))
            strMarkerAddress = [_savedMapDetails objectForKey:@"location_address"];
        
        if (NULL_TO_NIL([_savedMapDetails objectForKey:@"location_title"]))
            strMarkerPlaceName = [_savedMapDetails objectForKey:@"location_title"];
        
        if (NULL_TO_NIL([_savedMapDetails objectForKey:@"location_details"]))
            strMarkerDescription = [_savedMapDetails objectForKey:@"location_details"];
        
        [self plotAnnotationOnMapWithInfo:address withPoint:CLLocationCoordinate2DMake(latitude, longitude)];
    }
    
    
    
}



-(void)drawPolyLineFromSavedPath:(NSString*)polygonPath{
    
    GMSPath *oldpath = [GMSPath pathFromEncodedPath:polygonPath];
    GMSMutablePath *path = [GMSMutablePath path];
    
    for (int i = 0; i < oldpath.count; i++) {
        CLLocationCoordinate2D loc = [oldpath coordinateAtIndex:i];
        [path addCoordinate:loc];
        
    }
    _drawnPolyline  = [GMSPolyline polylineWithPath:path];
    _drawnPolyline.strokeColor = [UIColor blueColor];
    _drawnPolyline.strokeWidth = 4;
    _drawnPolyline.tappable = NO;
    _drawnPolyline.map = mapView;
    _drawnPolyline.zIndex = 0;
    [_drawnPolyline setTappable:NO];

}


-(void)loadMap{
    
    
    // ONLY IF MAP WANTS TO LOAD USER LOCATION
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude
                                         longitude:locationManager.location.coordinate.longitude
                                              zoom:zoomLevel];
    if (!_isCumingFromCart)mapView.camera = camera;
    mapView.delegate  = self;
    mapView.settings.consumesGesturesInView = NO;
    mapView.mapType = kGMSTypeSatellite;
    
}




#pragma mark - Get user current location


-(void)getCurrentUserLocation{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if ([locations lastObject]) {
        
        [locationManager stopUpdatingLocation];
        
        [self loadMap];
        
        
    }
}

- (void)addGestureRecogniserToMapView{
    
    UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(plotAnnotationOnMapWithTouch:)];
    gesture.minimumPressDuration = .5;
    gesture.delegate = self;
    [mapView addGestureRecognizer:gesture];
    longPressGesture = gesture;
    
}

#pragma mark - Plot annotation on Map

-(void)plotAnnotationOnMapWithInfo:(NSString*)information withPoint:(CLLocationCoordinate2D)location{
    
    if (arrMarker.count == _numberOfBoardsRequired) {
        return;
    }
    GMSMarker *marker = [[GMSMarker alloc] init];
   // marker.snippet = information;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.position = location;
    marker.map = mapView;
    marker.draggable = YES;
    marker.icon = [UIImage imageNamed:@"MapPointer.png"];
    mapView.accessibilityElementsHidden = NO;
    [btnAddInfo setEnabled:YES];
    marker.zIndex = 1;
    [arrMarker addObject:marker];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.latitude
                                                            longitude:location.longitude
                                                                 zoom:zoomLevel
                                                              bearing:10
                                                         viewingAngle:10];
    
    mapView.camera = camera;
    [self getAddressFromPlottedPoint];
    
}

-(void)getAddressFromPlottedPoint{
    
    if (arrMarker.count) {
        if (arrMarker.count > 1) return;
        
        GMSMarker *_marker  = arrMarker[0];
        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(_marker.position.latitude, _marker.position.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
            if ([response results].count) {
                GMSAddress* addressObj = [[response results] firstObject];
                /*
                NSLog(@"thoroughfare=%@", addressObj.thoroughfare);
                NSLog(@"locality=%@", addressObj.locality);
                NSLog(@"subLocality=%@", addressObj.subLocality);
                NSLog(@"administrativeArea=%@", addressObj.administrativeArea);
                NSLog(@"postalCode=%@", addressObj.postalCode);
                NSLog(@"country=%@", addressObj.country);
                NSLog(@"lines=%@", addressObj.lines);*/
                strMarkerPlaceName = addressObj.subLocality;
                if (!addressObj.subLocality) strMarkerPlaceName = addressObj.locality;
                if (addressObj.thoroughfare)  strMarkerPlaceName = addressObj.thoroughfare;
                if (addressObj.lines) strMarkerAddress = [addressObj.lines componentsJoinedByString:@","] ;
                
            }
           
        }];
    }
    

}

-(void)plotAnnotationOnMapWithTouch:(UILongPressGestureRecognizer*)gesture{
    
   
    if (gesture && gesture.view) {
        
        if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateFailed|| gesture.state == UIGestureRecognizerStateEnded)
        {
            //if (isDraggingPointer)return; // SHOULD NOT ADD POINTER WHILE DRAGGING THE POINTER , WHICH ALSO WORKS WHILE LONG PRESS.
            CGPoint point = [gesture locationInView:mapView];
            CLLocationCoordinate2D endPoint = [mapView.projection coordinateForPoint:point];
            [self plotAnnotationOnMapWithInfo:nil withPoint:endPoint];
        }
      
    }
}

-(void)mapView:(GMSMapView *)_mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    zoomLevel = mapView.camera.zoom;
     shouldByPassSave = false;
    // handle you zoom related logic
}

-(BOOL) mapView:(GMSMapView *)_mapView didTapMarker:(GMSMarker *)_marker
{
    activeMarker = _marker;
    [self showOptions];
    return NO;
}

- (void) mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker
{
    longPressGesture.enabled = false;
}

- (void) mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker
{
    longPressGesture.enabled = true;
}

- (void) mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker
{
}


#pragma mark - Rotate and Delete Annotatin

-(void)showOptions{
    
    UINavigationController *nav = self.navigationController;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"MARKER"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *rotate = [UIAlertAction actionWithTitle:@"Set Sign Face Direction (Rotate)"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         
                                                         [self setUpSlider];
                                                         
                                                         
                                                         
                                                     }];
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         
                                                         [self removeMarkerClickedFromPopUp];
                                                         
                                                     }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    
    
    [alert addAction:rotate];
    [alert addAction:delete];
    [alert addAction:cancel];
    [nav presentViewController:alert animated:YES completion:nil];
    
}

-(void)setUpSlider{
    
    vwSliderOverlay.hidden = false;
    CGPoint point = [mapView.projection pointForCoordinate:activeMarker.position];
    circularSlider.center = CGPointMake(point.x, point.y);
    circularSlider.value = activeMarker.rotation;
}
- (IBAction)updateProgress:(UICircularSlider *)sender {
    
   float progress = translateValueFromSourceIntervalToDestinationInterval(sender.value, sender.minimumValue, sender.maximumValue, 0.0, 360.0);
    activeMarker.rotation = progress;
}

- (IBAction)closeSliderPopUp{
     vwSliderOverlay.hidden = true;
}

#pragma mark - Marker Info Adding PopUp Methods


-(IBAction)showMarkerInfoPopUp{
    
    if (!markerInfoPopUp) {
        
        markerInfoPopUp = [MarkerInfoPopUp new];
      
        [self.view addSubview:markerInfoPopUp];
        markerInfoPopUp.delegate = self;
        
        markerInfoPopUp.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[markerInfoPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(markerInfoPopUp)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[markerInfoPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(markerInfoPopUp)]];
        
        markerInfoPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            markerInfoPopUp.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [self.view endEditing:YES];
    [markerInfoPopUp setUp];
    
    for (GMSMarker *marker in arrMarker) {
        if (strMarkerAddress.length) {
            markerInfoPopUp.strAddress = strMarkerAddress;
           // marker.snippet = strMarkerAddress;
        }else{
          //  markerInfoPopUp.strAddress = marker.snippet;
        }
    }
    markerInfoPopUp.strPlaceName = strMarkerPlaceName;
    markerInfoPopUp.strDescription = strMarkerDescription;
    
    if (imgToUpload.count) {
        markerInfoPopUp.arrUploadImages = [NSMutableArray arrayWithArray:imgToUpload];
    }
    
    
}

-(void)closeInfoPopUpAfterADelay:(float)delay{
    
    if (mapView.selectedMarker) {
         mapView.selectedMarker = nil;
    }
   
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        markerInfoPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [markerInfoPopUp removeFromSuperview];
        markerInfoPopUp = nil;
       
    }];
    
    
}

-(void)removeMarkerClickedFromPopUp{
    
    if (arrMarker.count) {
        activeMarker.map = nil;
        [arrMarker removeObjectIdenticalTo:activeMarker];
    }
    if (arrMarker.count <= 0) {
        strMarkerPlaceName = @"";
        strMarkerAddress = @"";
        strMarkerDescription = @"";
        [imgToUpload removeAllObjects];
        [btnAddInfo setEnabled:NO];
    }
    
  [self closeInfoPopUpAfterADelay:.2];
}
-(void)saveMarkerWithDetails:(NSString*)name address:(NSString*)address description:(NSString*)description image:(NSMutableArray*)images{
    
    strMarkerPlaceName = name;
    strMarkerAddress = address;
    strMarkerDescription = description;
    for (GMSMarker *marker in arrMarker) {
       // marker.snippet = address;
    }
    [imgToUpload removeAllObjects];
    for (UIImage *image in images) {
        [imgToUpload addObject:image];
    }
    
    [self closeInfoPopUpAfterADelay:.2];
}


#pragma mark - Map Polygon shape

-(void)drawPolyLine{
   
    __weak PlotTemplateOnMapViewController *weakSelf = self;
     mapDrawView.mapView = mapView;
    mapDrawView.polygonDrawnBlock = ^(GMSPolyline *polyline){
        
        weakSelf.drawnPolyline = polyline;
    };
    mapDrawView.MapViewIdleAtCameraPositionBlock = ^(GMSCameraPosition *cameraPosition){
    };
    mapDrawView.MapViewDidTapOverlayBlock = ^(GMSPolygon *polygon_Tapped){
    };
    mapDrawView.ViewEnabledBlock = ^(){
    };
}


#pragma mark - Cart Adding and Updating actions


-(IBAction)addTheProductToCart{
    
    // Add/Update  to cart applied
    if (_isCumingFromCart)
        [self updateCartIsByCheckOutAction:NO];
    else
        [self updateCartIsByCheckOutAction:YES];
   
}

-(IBAction)saveAndCheckOut:(id)sender{
    
    // save and Checkout button applied
    
    [self updateCartIsByCheckOutAction:YES];
    
}

-(void)updateCartIsByCheckOutAction:(BOOL)isByCheckout{
    
    [self checkAllFieldsAreValidOnSuccess:^{
        
        [self showLoadingScreenWithTitle:@"Saving.."];
         shouldContinueSave = true;
        [self focusMapToShowAllMarkers];
        
    } failure:^(NSString *title, NSString *message,BOOL showPopUp) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle: UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       if (showPopUp)[self showMarkerInfoPopUp];
                                           
                                       
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];

        
    }];
 
    
    
}

-(void)checkAllFieldsAreValidOnSuccess:(void (^)())success failure:(void (^)(NSString *title,NSString *message,BOOL showPopUp))failure{
    
    NSString *errorMsg;
    NSString *title;
    if (arrMarker.count != _numberOfBoardsRequired) {
        
        errorMsg = [NSString stringWithFormat:@"Please add all your (%ld) locations by long pressing your finger on map.",(long)_numberOfBoardsRequired];
        title = @"Info";
        failure(title,errorMsg,NO);
        return;
    }
    if ((strMarkerAddress.length <= 0) || (strMarkerPlaceName.length <= 0) || (strMarkerDescription.length <= 0)) {
        errorMsg = @"Please Fill the Address details for adding the SignBoard";
        title = @"Save Map";
        failure(title,errorMsg,YES);
        return;
        
    }
    success();
}

- (void)focusMapToShowAllMarkers
{
    if (arrMarker.count) {
        CLLocationCoordinate2D myLocation = ((GMSMarker *)arrMarker.firstObject).position;
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
        
        for (GMSMarker *marker in arrMarker)
            bounds = [bounds includingCoordinate:marker.position];
        
        [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
    }
    
    if (shouldByPassSave) {
        shouldByPassSave = false;
        shouldContinueSave = false;
        [self captureMapScreen];
    }
    
}

- (void)mapViewSnapshotReady:(GMSMapView *)mapView{
    
    if (!shouldByPassSave) {
        if (shouldContinueSave){
            shouldContinueSave = false;
            shouldByPassSave = false;
            [self captureMapScreen];
        }
        
    }
    
}

- (void)captureMapScreen {
    
    UIGraphicsBeginImageContextWithOptions(mapView.frame.size, YES, 0.0f);
    [mapView.layer renderInContext:UIGraphicsGetCurrentContext()];
    screenShot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self continueSaving];
}

-(void)continueSaving{
    
    [self updateCartWithNewMapDetails:^{
        
        [self loadCheckOutPage];
        
    } failure:^{
        
    }];
}

-(void)updateCartWithNewMapDetails:(void (^)())success failure:(void (^)())failure{
    
    NSMutableArray *loc = [NSMutableArray new];
    for (GMSMarker *marker in arrMarker) {
        
        NSString *str = [NSString stringWithFormat:@"%f,%f,%f",marker.position.latitude,marker.position.longitude,marker.rotation];
        if (str.length)[loc addObject:str];

    }
   
    [APIMapper updateCartWith:[User sharedManager].companyID cartID:_cartID userID:[User sharedManager].userId mapID:mapID address:strMarkerAddress title:strMarkerPlaceName description:strMarkerDescription annotations:loc polygonPath:self.drawnPolyline.path.encodedPath images:imgToUpload screenShot:screenShot Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject)  {
        
        NSDictionary *responds = (NSDictionary*)responseObject;
        if ((responds && [responds objectForKey:@"header"])) {
            if ([[responds objectForKey:@"header"] objectForKey:@"shipping_id"]) {
                [User sharedManager].shippingID = [[responds objectForKey:@"header"] objectForKey:@"shipping_id"];
            }
            NSInteger statusCode = [[[responds objectForKey:@"header"] objectForKey:@"code"] integerValue];
            if (statusCode == kSuccessCode) {
                
                mapID = [[[responds objectForKey:@"header"] objectForKey:@"map_id"] integerValue];
                success();
            }
            else{
                
                NSString *errorString = @"Failed to save.";
                [[[UIAlertView alloc] initWithTitle:@"Save Map" message:errorString  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                failure();
            }
        }
        
        [self hideLoadingScreen];
       
        
      } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
        if (error) {
            NSString *errorString = [error localizedDescription];
            [[[UIAlertView alloc] initWithTitle:@"Save Map" message:errorString  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        failure();
        
      }progress:^(long long totalBytesWritten, long long totalBytesExpectedToWrite) {
          
      }];
    
    

    
}




- (IBAction)penButtonTapped:(UIButton*)sender {
    
    UIButton *btn = (UIButton*)sender;
  
    if (!mapDrawView.isDrawingPolygon)
    {
        // We're starting the drawing of our polyline/polygon
        
        btn.tag = 1;
        [btn setImage:[UIImage imageNamed:@"Close.png"] forState:UIControlStateNormal];
        [self removePolyLine];
        [mapDrawView resetPoints];
        [mapDrawView enableDrawing];
    }
    else
    {
        btn.tag = 0;
        [btn setImage:[UIImage imageNamed:@"PolygonDrawIcon.png"] forState:UIControlStateNormal];
        [mapDrawView disableDrawing];
    }
}

-(void)removePolyLine{
    
    _drawnPolyline.map = nil;
    _drawnPolyline = nil;
    
}
-(void)removeMarker{
    
    [arrMarker removeAllObjects];
    activeMarker = nil;
    strMarkerPlaceName = @"";
    strMarkerAddress = @"";
    strMarkerDescription = @"";
    imgToUpload = nil;
    [btnAddInfo setEnabled:NO];
}

-(IBAction)clearMap{
    
    [mapView clear];
    [self removeMarker];
    [self removePolyLine];
    [mapDrawView enableDrawing];
    [self penButtonTapped:btnPen];
    
    
}


-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)loadCheckOutPage{
    
    shouldByPassSave = true;
    ComposeBillingAddressViewController *billingAdres = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForCreateBillingAddress];
    billingAdres.cartIDs = [NSString stringWithFormat:@"%ld",(long)self.cartID];
    [[self navigationController]pushViewController:billingAdres animated:YES];
   

}

-(void)loadCartListingPage{
    
    [Utility removeAViewControllerFromNavStackWithType:([MyCartListingViewController class]) from:[self.navigationController viewControllers]];
    MyCartListingViewController *cartPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MapDetailsAndListingStoryBoard Identifier:StoryBoardIdentifierForCartListingPage];
    [[self navigationController]pushViewController:cartPage animated:YES];
    
}

-(void)updateSharedManagerWith:(NSDictionary*)responseObject{
    
    // Update cart count for the user
    
    if ( NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"cartcount"])){
        
        [User sharedManager].cartCount = [[[responseObject objectForKey:@"header"] objectForKey:@"cartcount"] integerValue];
        [Utility saveUserObject:[User sharedManager] key:@"USER"];
    }
    
}



-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


-(void)dealloc{
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
