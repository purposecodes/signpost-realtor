//
//  BillingAddressListViewControlller.h
//  SignSpot
//
//  Created by Purpose Code on 08/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BillingListSelectionDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Edit' Button.
 */

-(void)editSelectedBillWithDetails:(NSDictionary*)details;




@end

@interface BillingAddressListViewControlller : UIViewController

@property (nonatomic,weak)  id<BillingListSelectionDelegate>delegate;

@end
