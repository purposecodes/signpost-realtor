//
//  BillingAddressListViewControlller.m
//  SignSpot
//
//  Created by Purpose Code on 08/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kCellHeight             125;
#define kHeightForHeader        40;
#define kNumberOfSections       1;

#import "BillingAddressListViewControlller.h"
#import "CustomCellForBillingList.h"
#import "ComposeBillingAddressViewController.h"
#import "Constants.h"

@interface BillingAddressListViewControlller () <BillingListCellDelegate>{
    
    IBOutlet UITableView *tableView;
    NSMutableArray *arrAdress;
    BOOL isDataAvailable;
}

@end

@implementation BillingAddressListViewControlller

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self loadAllBillingAddressesIfAny];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    arrAdress = [NSMutableArray new];
    isDataAvailable = false;
    tableView.hidden = true;
   // tableView.allowsSelection = NO;
}

-(void)loadAllBillingAddressesIfAny{
    
     [self showLoadingScreenWithTitle:@"Loading.."];
     [APIMapper getAllSavedBillingAdresses:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        tableView.hidden = false;
        if ( NULL_TO_NIL( [responseObject objectForKey:@"resultarray"])) {
            arrAdress = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"resultarray"]];
            if (arrAdress.count > 0) isDataAvailable = true;
            [tableView reloadData];
        }
         [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
         tableView.hidden = false;
         [self hideLoadingScreen];
    }];
}
#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumberOfSections;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (!isDataAvailable) {
        return 1;
    }
    return arrAdress.count;
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!isDataAvailable) {
        
        /*****! No listing found , default cell !**************/
        
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:_tableView withTitle:@"No Address found."];
        return cell;
        
    }
    
    static NSString *CellIdentifier = @"reuseIdentifier";
    CustomCellForBillingList *cell = (CustomCellForBillingList *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellForBillingList" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [self configureCell:cell indexPath:indexPath];
    cell.cellTag = indexPath.row;
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kCellHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor getBackgroundOffWhiteColor];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    
    if (isDataAvailable) {
         lblTitle.text = [NSString stringWithFormat:@"Total %lu saved Address.",(unsigned long)arrAdress.count];
    }

    lblTitle.font = [UIFont fontWithName:CommonFont size:16];
    lblTitle.textColor = [UIColor blackColor];
    
    return vwHeader;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([indexPath row] < arrAdress.count)
        [self editButtonTapped:indexPath.row];
}



-(void)configureCell:(CustomCellForBillingList*)cell indexPath:(NSIndexPath*)indexPath{
    
    if (indexPath.row < arrAdress.count ) {
        
        NSDictionary *info = arrAdress[[indexPath row]];
        if ( NULL_TO_NIL([info objectForKey:@"bill_name"])) {
             cell.lblName.text =  [info objectForKey:@"bill_name"];
        }
        
        if ( NULL_TO_NIL([info objectForKey:@"bill_mobile"])) {
            cell.lblPhoneNumber.text = [info objectForKey:@"bill_mobile"];
        }
        
        if ( NULL_TO_NIL([info objectForKey:@"full_address"])) {
            NSString *address =[info objectForKey:@"full_address"];
            NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:address];
            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineSpacing:5];
            [attrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, address.length)];
            cell.lblAddress.attributedText = attrString;
        }
    }
}

#pragma mark - Cell Delegate


-(void)editButtonTapped:(NSInteger)tag{
    
    if (tag < arrAdress.count) {
        
        NSDictionary *billingInfo = arrAdress[tag];
        [self.delegate editSelectedBillWithDetails:billingInfo];
        [self goBack:nil];
    }

    
}
-(void)deleteButtonTapped:(NSInteger)tag{
    
    if (tag < arrAdress.count) {
        
        NSDictionary *billInfo = arrAdress[tag];
        if ( NULL_TO_NIL( [billInfo objectForKey:@"bill_id"])) {
            NSString *billID = [billInfo objectForKey:@"bill_id"];
            [self showLoadingScreenWithTitle:@"Deleting.."];
            [APIMapper removeBillingAddressWithID:billID userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                if (tag < arrAdress.count)
                    [arrAdress removeObjectAtIndex:tag];
                if (!arrAdress.count) {
                    isDataAvailable = false;
                }
                [tableView reloadData];
                [self hideLoadingScreen];
            
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
                [self hideLoadingScreen];
            }];
            
        }
        
    }
    
}



#pragma mark - Common Actions


-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
