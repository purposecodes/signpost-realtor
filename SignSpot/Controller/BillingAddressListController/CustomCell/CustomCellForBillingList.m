//
//  CustomCellForBillingList.m
//  SignSpot
//
//  Created by Purpose Code on 08/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CustomCellForBillingList.h"

@implementation CustomCellForBillingList

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)editButtonTaped:(id)sender{
    
    [[self delegate]editButtonTapped:self.cellTag];
    
}

-(IBAction)deleteButtonTaped:(id)sender{
    [[self delegate]deleteButtonTapped:self.cellTag];
}

@end
