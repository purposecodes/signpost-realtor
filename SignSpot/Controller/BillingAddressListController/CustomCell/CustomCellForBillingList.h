//
//  CustomCellForBillingList.h
//  SignSpot
//
//  Created by Purpose Code on 08/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BillingListCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Edit' Button.
 */

-(void)editButtonTapped:(NSInteger)tag;

/*!
 *This method is invoked when user taps the 'Delete' Button.
 */


-(void)deleteButtonTapped:(NSInteger)tag;



@end



@interface CustomCellForBillingList : UITableViewCell

@property (nonatomic,weak)      IBOutlet  UILabel *lblName;
@property (nonatomic,weak)      IBOutlet  UILabel *lblPhoneNumber;
@property (nonatomic,weak)      IBOutlet  UILabel *lblAddress;
@property (nonatomic,assign)    NSInteger    cellTag;
@property (nonatomic,weak)  id<BillingListCellDelegate>delegate;

@end
