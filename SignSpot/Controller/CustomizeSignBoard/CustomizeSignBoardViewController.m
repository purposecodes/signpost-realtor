//
//  ViewController.m
//  Custom Sign
//
//  Created by Purpose Code on 28/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)


typedef enum{
    
    eToolFont       = 0,
    eToolTextColor  = 1
    

}eDesignTool;

#define kSuccessCode            200
#define kShapeHeight            70
#define kShapeWidth             70
#define kFontSizeStep           1
#define kSliderStepSize         24
#define kPickerCellHeight       40
#define kPickerComponentWidth   100
#define kExtraTextPadding       10
#define kFontNumbers            61

#define kNumberOfTools          2

#define kTextDetailsInfo            @"TextDetails"

#define kXPosition                  @"left"
#define kYPosition                  @"top"

#define kColor                      @"color"
#define kTitle                      @"title"
#define kAngle                      @"transform"
#define kFontSize                   @"font-size"
#define kFontColor                  @"color"
#define kFontFamily                 @"font-family"
#define kTextTransform              @"text-transform"




#import "CustomizeSignBoardViewController.h"
#import "KKColorListPicker.h"
#import "UIIMageViewWithID.h"
#import "UIViewWithColor.h"
#import "PlotTemplateOnMapViewController.h"
#import "Constants.h"
#import "UITextFieldWithColorInfo.h"

@interface CustomizeSignBoardViewController ()<KKColorListViewControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>{
    
    IBOutlet UIView *vwaCanvasBgWithBorder;
    IBOutlet UIViewWithColor *vwBackground;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIImageView *imgBackGround;
    IBOutlet UIPickerView *picker;
    IBOutlet UIView *pickerBackgrnd;
    IBOutlet UICollectionView *toolCollectionView;
    
    BOOL isBorderAvailable;
    BOOL shouldTrigger;
    float canvasWidth;
    float canvasHeight;
    float firstX, firstY;
    float paddingOnCanvas;
    id currentSelectedView;
    NSInteger initialFontSize;
    
    UIView *containerView;
    UITextField *accesryTextField;
    NSString *lstFontName;
    NSArray *allFonts;
    NSMutableDictionary *dictObjSpecifications;
    
    CGSize lstShapeSize;
    NSInteger objectTag;
    
    
}

@end

@implementation CustomizeSignBoardViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUp];
    [self loadBaseTemplateSpecifications];
    [self setUpGrowingTextView];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
}

-(void)setUp{
    
    
   // _cartID = -1;
    paddingOnCanvas       = 0;
    shouldTrigger         = true;
    dictObjSpecifications = [NSMutableDictionary new];
    
    pickerBackgrnd.hidden = true;
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,picker.frame.size.width,44)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"Apply" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor getThemeColor]];
    [button.titleLabel setFont:[UIFont fontWithName:CommonFont size:14]];
    button.frame = CGRectMake(0, 0, 100, 35);
    UIBarButtonItem* buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button addTarget:self action:@selector(doneClicked) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = 5.f;
    
    UIButton* btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnCancel setBackgroundColor:[UIColor getThemeColor]];
    [btnCancel.titleLabel setFont:[UIFont fontWithName:CommonFont size:14]];
    btnCancel.frame = CGRectMake(0, 0, 100, 35);
    UIBarButtonItem* buttonItemCancel = [[UIBarButtonItem alloc] initWithCustomView:btnCancel];
    [btnCancel addTarget:self action:@selector(cancelClicked) forControlEvents:UIControlEventTouchUpInside];
    btnCancel.layer.cornerRadius = 5.f;
    
    toolBar.items = @[buttonItem,buttonItemCancel];
    [pickerBackgrnd addSubview:toolBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}




#pragma mark - Load Template from API and display


-(void)loadBaseTemplateSpecifications{
    
    [self showLoadingScreenWithTitle:@"Loading.."];
    
        [APIMapper getBaseTemplateSpecificationsWith:self.productID cartID:_cartID isUserTemplate:_isFromUserTemplate Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self parseReponds:responseObject];
            [self hideLoadingScreen];
            
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
            [self hideLoadingScreen];
            
        }];
}

-(void)parseReponds:(NSDictionary*)responseObject{
    
    [self getCanvasSize];
    
    if ([[responseObject objectForKey:@"resultarray"] isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *result = [responseObject objectForKey:@"resultarray"];
        if (NULL_TO_NIL([result objectForKey:@"bg"]))
            [self addReqiuredBGByInfo:[result objectForKey:@"bg"]];
        
        if (NULL_TO_NIL([result objectForKey:@"text"]))
            [self addRequiredTextsByInfo:[result objectForKey:@"text"]];

    }
    
    [self hideLoadingScreen];

}



-(void)addRequiredTextsByInfo:(NSArray*) specifications{
    
    float xValue = 0, yValue = 0;
    
    for (NSDictionary *details in specifications) {
        
        UITextFieldWithColorInfo *textField = [UITextFieldWithColorInfo new];
        [vwBackground addSubview:textField];
        textField.delegate = self;
        textField.textColor = [self colorWithHexString:@"#666666"];
        textField.userInteractionEnabled = true;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        NSInteger fontSize = 25;
        NSString *fontFamily = @"Verdana";
        if ( NULL_TO_NIL([details objectForKey:kFontSize]))
            fontSize = [[details objectForKey:kFontSize] integerValue];
       
        fontSize = fontSize / 2.2;
        objectTag ++;
        textField.tag = objectTag;
        [dictObjSpecifications setObject:textField forKey:[NSNumber numberWithInteger:objectTag]];
     
        if ( NULL_TO_NIL([details objectForKey:kColor])) {
            NSString *bgColor =[details objectForKey:kColor];
            textField.textColor = [self colorWithHexString:bgColor];
        }
        if ( NULL_TO_NIL([details objectForKey:kFontFamily])) {
            fontFamily = [details objectForKey:kFontFamily];
        }
        
        if ( NULL_TO_NIL([details objectForKey:kXPosition])) {
            NSInteger value =[[details objectForKey:kXPosition] integerValue];
            xValue = [self getXPositionFromPercentageBy:value];
            
        }
        if ( NULL_TO_NIL([details objectForKey:kYPosition])) {
            NSInteger value =[[details objectForKey:kYPosition] integerValue];
            yValue = [self getYPositionFromPercentageBy:value];
            
        }
        if ( NULL_TO_NIL([details objectForKey:kTitle])) {
            textField.text = [details objectForKey:kTitle];
        }
        textField.isPhoneNumber = false;
//        if ([self numberValidation:textField.text]) {
//            textField.isPhoneNumber = true;
//        }
        if ( NULL_TO_NIL([details objectForKey:kTextTransform])) {
            NSString *string = textField.text;
            NSString *transform = [details objectForKey:kTextTransform];
            if ([transform isEqualToString:@"uppercase"])
               string=  [string uppercaseString];
            else if ([transform isEqualToString:@"lowercase"])
              string=  [string lowercaseString];
            textField.text = string;
        }
        
        if ( NULL_TO_NIL([details objectForKey:kAngle])) {
            textField.isTransformed = true;
        }
        
        textField.layer.anchorPoint = CGPointMake(0, 0);
        textField.font = [UIFont fontWithName:fontFamily size:fontSize];
        textField.frame = CGRectMake(xValue, yValue,10, 10);
        textField.textAlignment = NSTextAlignmentCenter;
        [self adjustTextFieldFrameRespectToFontSizeWith:textField shouldApplyRotation:YES];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        panGesture.delegate = self;
        [textField addGestureRecognizer:panGesture];
       
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        tapGesture.delegate = self;
        tapGesture.numberOfTapsRequired = 1;
        [textField addGestureRecognizer:tapGesture];
        
        if ( NULL_TO_NIL([details objectForKey:kAngle])) {
            textField.transform = CGAffineTransformIdentity;
            float angle = [[details objectForKey:kAngle] floatValue];
           [textField setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(angle))];
           textField.rotation = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(angle));
            
        }

    }
}




-(void)addReqiuredBGByInfo:(NSDictionary*) bgDetails{
    
    vwBackground.backgroundColor = [UIColor whiteColor];
    
        if (NULL_TO_NIL([bgDetails objectForKey:@"bgimage"])) {
            
            [activityIndicator startAnimating];
            NSString *url = [bgDetails objectForKey:@"bgimage"];
            [imgBackGround sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                  [activityIndicator stopAnimating];
            }];
        }
 
}


#pragma mark - Color Picker Actions

- (void)colorListController:(KKColorListViewController *)controller didSelectColor:(KKColor *)color isForBorder:(BOOL)isBorder isTextColor:(BOOL)isTextColor
{
    if (isBorder) {
        
        vwBackground.layer.borderWidth  = 5.f;
        paddingOnCanvas = 5;
        vwBackground.layer.borderColor = [color uiColor].CGColor;
        [self resetAllSelections];
        return;
        
    }
    if (isTextColor) {
        
        if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
            UITextFieldWithColorInfo *selectedTextField = (UITextFieldWithColorInfo*)currentSelectedView;
            selectedTextField.textColor =  [color uiColor];
        }
         [self resetAllSelections];
        return;
        
    }
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        
        UITextFieldWithColorInfo *selectedTextField = (UITextFieldWithColorInfo*)currentSelectedView;
        selectedTextField.backgroundColor =  [color uiColor];
        selectedTextField.isBGColorEnabled = true;
    }
    else if ([currentSelectedView isKindOfClass:[UIImageViewWithID class]]) {
        
        UIImageViewWithID *selectedView = (UIImageViewWithID*)currentSelectedView;
        selectedView.image = [selectedView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [selectedView setTintColor: [color uiColor]];
        selectedView.backgroundColor = [UIColor clearColor];
    }
    else if ([currentSelectedView isKindOfClass:[UIViewWithColor class]]) {
        
        UIViewWithColor *selectedView = (UIViewWithColor*)currentSelectedView;
        selectedView.backgroundColor = [color uiColor];
        selectedView.bgColor = [color uiColor];
    }
    
   

}
- (void)colorListPickerDidComplete:(KKColorListViewController *)controller{
    
    [self resetAllSelections];
    
}



#pragma mark - TextField  delegates

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGesture
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextFieldWithColorInfo *)textField{
    
    [textField resignFirstResponder];
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
         UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        if (txtField.tag > 0) {
            if (txtField.isTransformed) {
                txtField.transform = txtField.rotation;
            }
        }
    }
    [self resetAllSelections];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextFieldWithColorInfo *)textField{
    pickerBackgrnd.hidden = true;
    if (textField.tag < 0) {
        return YES;
    }
    return NO;
}

- (BOOL)textField:(UITextFieldWithColorInfo *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;{
    
    if (![string isEqualToString:@""]) {
        if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
            UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
            if (txtField.isPhoneNumber) {
                [self convertUSFoneNumberFormatWithField:textField];
            }
        }
        if (textField.frame.size.width > vwBackground.frame.size.width - 10) {
            return NO;
        }
    }
   
    return YES;
}

- (void)textFieldDidEndEditing:(UITextFieldWithColorInfo *)textField{
    
}
-(BOOL)textFieldShouldEndEditing:(UITextFieldWithColorInfo *)textField{
    
     if (textField.tag > 0) {
         
         if (textField.isTransformed) {
             textField.layer.anchorPoint = CGPointMake(0, 0);
             textField.transform = textField.rotation;
             
         }
     }
   
   
    return YES;
}

#pragma mark - Font Pickerview delegates

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    NSInteger count = 0;
    if (component == 0) {
         return kFontNumbers;
    }
    return count;
}

-(NSArray *)getAllFonts{
    
    NSArray *fonts  = [[NSArray alloc]initWithObjects:@"BadMofo",@"BandungHardcoreGP",@"Blockletter",@"CantateBeveled",@"Draconis-Bold",@"Draconis-Italic",@"PistolGripPump",@"SpartacoAcademy",@"Arial-Black",@"CenturyGothic",@"Gunplay-Regular",@"Hp-Impact",@"MisterBelvedere",@"Tahoma-Bold",@"Tahoma",nil];
    
    return fonts;
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *title = @"";
    
    if (component == 0) {
        return [NSString stringWithFormat:@"%ld",(long)row + kFontSizeStep];
        
    }else{
        return [NSString stringWithFormat:@"%ld",(long) row + kFontSizeStep];
    }
    return title;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component ;{
    
    if (component == 0) {
        return kPickerComponentWidth;
    }
    return kPickerComponentWidth;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return kPickerCellHeight;
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    float size ;
    size = row + kFontSizeStep;
    
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        
        UITextFieldWithColorInfo *selectedTextField = (UITextFieldWithColorInfo*)currentSelectedView;
        selectedTextField.font = [UIFont fontWithName:selectedTextField.font.fontName size:size];
        [self adjustTextFieldFrameRespectToFontSizeWith:selectedTextField shouldApplyRotation:NO];
  
    }

}

-(IBAction)doneClicked{
    
    pickerBackgrnd.hidden = true;
     UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
    
    if (txtField.tag > 0) {
        if (txtField.isTransformed) {
            txtField.transform = txtField.rotation;
        }

    }
    
    [self.view endEditing:YES];
    [self resetAllSelections];
}

-(IBAction)cancelClicked{
    
    pickerBackgrnd.hidden = true;
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        
        UITextFieldWithColorInfo *selectedTextField = (UITextFieldWithColorInfo*)currentSelectedView;
        selectedTextField.font = [UIFont fontWithName:selectedTextField.font.fontName size:initialFontSize];
        [self adjustTextFieldFrameRespectToFontSizeWith:selectedTextField shouldApplyRotation:YES];
        
    }
    
     [self resetAllSelections];
}



#pragma mark - CollectionView delegates with Drag and Drop Effect


-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    return kNumberOfTools;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell=[_collectionView dequeueReusableCellWithReuseIdentifier:@"ToolCollectionView" forIndexPath:indexPath];
    [self configureDesigningToolCollectionViewWithCell:cell andTag:indexPath.row];
    return cell;
    
    
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(40, 40);
    
}

- (CGFloat)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
        return 0;
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0,0,0);
}


-(void)configureDesigningToolCollectionViewWithCell:(UICollectionViewCell*)cell andTag:(NSInteger)tag{
    
    
    UIButton *btnAdded;
    if (![[cell.contentView viewWithTag:1] isKindOfClass:[UIImageView class]]) {
        
        UIButton *btnTool = [UIButton new];
        btnTool.frame = CGRectMake(0, 0, 40, 40);
        btnTool.tag = 1;
        [cell.contentView addSubview:btnTool];
        btnAdded = btnTool;
        [btnAdded addTarget:self action:@selector(designingToolSelectedWith:) forControlEvents:UIControlEventTouchUpInside];
    
    }else{
        
        UIButton *btn = [cell.contentView viewWithTag:1];
        btnAdded = btn;
        
    }
    switch (tag) {
       
        case eToolFont:
             [btnAdded setImage:[UIImage imageNamed:@"CreateFont.png"] forState:UIControlStateNormal];
            break;
            
            case eToolTextColor:
            [btnAdded setImage:[UIImage imageNamed:@"TextColorIcon.png"] forState:UIControlStateNormal];
            break;
            
            
        default:
            break;
    }
    btnAdded.tag = tag;
   
    

}

#pragma mark - Designing Cell Actions

-(void)designingToolSelectedWith:(UIButton*)btn{
    
    NSInteger selection = btn.tag;
    switch (selection) {
            
        case eToolFont:
            if (currentSelectedView) [self showAllFonts];
            break;
            
        case eToolTextColor:
            if (currentSelectedView)[self addTextColor];
            break;
            
        default:
            break;
    }
  
}


-(void)addTextColor{
    
    [self.view endEditing:YES];
    KKColorListViewController *controller = [[KKColorListViewController alloc] initWithSchemeType:KKColorsSchemeTypeCrayola isForBorder:NO isTextColor:YES];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)showAllFonts{
    
    pickerBackgrnd.hidden = false;
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        int fontSize = txtField.font.pointSize;
        initialFontSize = txtField.font.pointSize;
        fontSize -= 1;
        if (fontSize < kFontNumbers && fontSize > 0) {
            [picker selectRow:fontSize inComponent:0 animated:YES];
        }
        
        if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
            UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
            if (txtField.tag > 0) {
                if (txtField.isTransformed) {
                    if (CGAffineTransformIsIdentity(txtField.transform)) {
                        // not rotated
                    } else {
                        // rotated
                        txtField.transform = CGAffineTransformIdentity;
                    }
                }
            }
            
        }

    }
    
    [self.view endEditing:YES];
}



#pragma mark - Customization Methods


-(IBAction)handlePanGesture:(UIPanGestureRecognizer *)sender{
    
    [self.view bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:vwBackground];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        [self.view endEditing:YES];
        [self resetOldView];
        shouldTrigger = false;
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y - 20;
        [self setSelectionWithView:sender.view];
        pickerBackgrnd.hidden = true;;
    }
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
    [[sender view] setCenter:translatedPoint];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        CGFloat velocityX = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:vwBackground].x);
        CGFloat finalX = translatedPoint.x; //+ (.2*[(UIPanGestureRecognizer*)sender velocityInView:vwBackground].x);
        CGFloat finalY =  translatedPoint.y; //+ (.2*[(UIPanGestureRecognizer*)sender velocityInView:vwBackground].y);
        
        UITextFieldWithColorInfo *selectedTextField = (UITextFieldWithColorInfo*)currentSelectedView;
        BOOL shouldcheck = true;
        if (selectedTextField.tag > 0) {
            if (selectedTextField.isTransformed) {
                if (CGAffineTransformIsIdentity(selectedTextField.transform)) {
                    // not rotated
                } else {
                    shouldcheck = false;
                    // rotated
                    
                }
            }
        }
        
        if (shouldcheck) {
            if ([sender view].frame.origin.x  <  0) {
                //finalX =  ([sender view].frame.size.width / 2) + paddingOnCanvas ;
                finalX = 0;
                
            } else if ([sender view].frame.origin.x + [sender view].frame.size.width  > vwBackground.frame.size.width ) {
                //finalX =   vwBackground.frame.size.width -  [sender view].frame.size.width / 2 -  paddingOnCanvas ;
                finalX =   vwBackground.frame.size.width -  [sender view].frame.size.width  ;
            }
            else if ([sender view].frame.origin.y < 0) {
               // finalY =  [sender view].frame.size.height / 2 + paddingOnCanvas ;
                 finalY =  0 ;
                
            }else if ([sender view].frame.origin.y + [sender view].frame.size.height  > vwBackground.frame.size.height ) {
                //finalY =   vwBackground.frame.size.height -  [sender view].frame.size.height / 2 - paddingOnCanvas;
                finalY =   vwBackground.frame.size.height -  [sender view].frame.size.height;
                
            }else{
                
                if (finalY < 0 || finalX < 0 || finalX > vwBackground.frame.size.width || finalY > vwBackground.frame.size.height) {
                    finalX =  paddingOnCanvas;
                    finalY =  paddingOnCanvas;
                    
                }
                
            }
        }
        
       
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [[sender view] setCenter:CGPointMake(finalX, finalY)];
        [UIView commitAnimations];
        
    }
    
    
}



-(IBAction)handleTapGesture:(UITapGestureRecognizer*)gesture{
    
    
    
    if (shouldTrigger) {
        
        [self resetOldView];
        [self setSelectionWithView:gesture.view];
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Customize" message:@"Choose Action" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change Text" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self showAccesoryView];
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change Size" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [self showAllFonts];
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change Color" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self addTextColor];
            
        }]];
        
        
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }
    pickerBackgrnd.hidden = true;
    shouldTrigger = true;
    
    
    
}
-(void)resetOldView{
    
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        if (txtField.tag > 0) {
            if (txtField.isTransformed) {
                txtField.transform = txtField.rotation;
            }
        }
    }
}

-(IBAction)resetAllSelections{
    
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        if (txtField.tag > 0) {
            if (txtField.isTransformed) {
                txtField.transform = txtField.rotation;
            }
        }
    }
    
    for (UIView *vw in [vwBackground subviews]){
        
        vw.layer.borderColor = [UIColor clearColor].CGColor;
        currentSelectedView = nil;
        if ([vw isKindOfClass:[UITextFieldWithColorInfo class]]) {
            UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)vw;
            txtField.isEditing = false;
            [txtField setNeedsDisplay];
        }
        
    }
    pickerBackgrnd.hidden = true;
    [self.view endEditing:YES];
}

-(void)setSelectionWithView:(id)view{
    
    for (UIView *vw in [vwBackground subviews]){
        vw.layer.borderColor = [UIColor clearColor].CGColor;
        if (vw == view)
            currentSelectedView = vw;
        if ([vw isKindOfClass:[UITextFieldWithColorInfo class]]) {
            UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)vw;
            txtField.isEditing = false;
            [txtField setNeedsDisplay];
        }
    }
    
    UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
    txtField.isEditing = true;
    [txtField setNeedsDisplay];
    
}


#pragma mark - Get Canvas Specifications to send to Webserver


-(IBAction)proceedToMapApplied:(id)sender{
    
    [self showLoadingScreenWithTitle:@"Saving.."];
    
    NSMutableDictionary *designSpec =  [self getDesignSpec];
    UIImage *screenShot = [self getScreenShot];
    NSData *imagedata=[NSData dataWithData:UIImagePNGRepresentation(screenShot)];
    
    [APIMapper addTheProductToCartWith:_productID userID:[User sharedManager].userId cartID:_cartID signTypeID:_strSelectedSignTypeID printType:_strPrintSideInfoID signSizeID:_strSelectedSignSizeID categoryID:_strCategoryID quantity:_quantity price:_strPrice jsonString:designSpec image:imagedata Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ( NULL_TO_NIL([responseObject objectForKey:@"header"])) {
            
            if ([[[responseObject objectForKey:@"header"] objectForKey:@"code"]integerValue] == kSuccessCode) {
                _cartID = [[[responseObject objectForKey:@"header"] objectForKey:@"cart_id"] integerValue];
                [self updateSharedManagerWith:responseObject];
                [self showMapView];
                
            }else{
                
                [[[UIAlertView alloc] initWithTitle:@"Info" message:@"Failed to Add." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            
        }
        
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
        
    }];

}

-(void)showMapView{
    
    PlotTemplateOnMapViewController *mapView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MapDetailsAndListingStoryBoard Identifier:StoryBoardIdentifierForTemplateOnMapPage];
    mapView.cartID              = self.cartID;
    mapView.numberOfBoardsRequired = self.quantity;
    [self.navigationController pushViewController:mapView animated:YES];
    
}


-(void)updateSharedManagerWith:(NSDictionary*)responseObject{
    
    // Update cart count for the user
    
    if ( NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"cartcount"])){
        [User sharedManager].cartCount = [[[responseObject objectForKey:@"header"] objectForKey:@"cartcount"] integerValue];
        [User sharedManager].notificationCount = [[[responseObject objectForKey:@"header"] objectForKey:@"notificationcount"] integerValue];
        [Utility saveUserObject:[User sharedManager] key:@"USER"];
    }
    
    
}



-(NSMutableDictionary*)getDesignSpec{
    
    NSMutableDictionary *canvasSpec = [NSMutableDictionary new];
    
    /*! Text Details  !*/
    NSArray *textInfo = [self getTextDetails];
    [canvasSpec setObject:textInfo forKey:kTextDetailsInfo];
    
   return canvasSpec;
    
}

-(NSArray*)getTextDetails{
    
    NSMutableArray *arrTexts = [NSMutableArray new];
    for (id obj in [dictObjSpecifications allValues]) {
        
        if ([obj isKindOfClass:[UITextFieldWithColorInfo class]]) {
            
            NSMutableDictionary *txtInfo = [NSMutableDictionary new];
            UITextFieldWithColorInfo *txtField = (UITextFieldWithColorInfo*)obj;
            BOOL isTextAvailable = false;
            if (txtField.text.length > 0) {
                [txtInfo setObject:txtField.text forKey:kTitle];
                isTextAvailable = true;
            }
            if (isTextAvailable) {
                
                UIColor *color = ( UIColor *)txtField.textColor;
                NSString *rgbColor = [NSString stringWithFormat:@"#%@",[self hexStringForColor:color]];;
                [txtInfo setObject:rgbColor forKey:kFontColor];
                
                NSInteger fontSize = (txtField.font.pointSize) * 2.2;
                [txtInfo setObject:[NSNumber numberWithInteger:fontSize] forKey:kFontSize];
                
                NSString *fontName = [[txtField font]fontName];
                if ([fontName hasPrefix:@"font"])
                    fontName = @"Hp-Impact";
                [txtInfo setObject:fontName forKey:kFontFamily];
                
                NSInteger XPosition = txtField.frame.origin.x;
                if (XPosition < 0) XPosition = 0;
                NSInteger XPercentage = [self getPercentageForXBy:XPosition];
                [txtInfo setObject:[NSNumber numberWithInteger:XPercentage] forKey:kXPosition];
                
                NSInteger YPosition = txtField.frame.origin.y;
                if (YPosition < 0) YPosition = 0;
                NSInteger YPercentage = [self getPercentageForYBy:YPosition];
                [txtInfo setObject:[NSNumber numberWithInteger:YPercentage] forKey:kYPosition];
                
                [arrTexts addObject:txtInfo];

            }
           
        }
      
    }
    
    return arrTexts;

}

#pragma mark - ASccessory Methods

-(void)showAccesoryView{
    
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        if (txtField.isPhoneNumber) {
            accesryTextField.keyboardType = UIKeyboardTypeNumberPad;
        }else{
            accesryTextField.keyboardType = UIKeyboardTypeASCIICapable;
        }
    }
    [accesryTextField becomeFirstResponder];
    [accesryTextField reloadInputViews];
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        accesryTextField.text = txtField.text;
        if (txtField.tag > 0) {
            if (txtField.isTransformed) {
                if (CGAffineTransformIsIdentity(txtField.transform)) {
                    // not rotated
                } else {
                    // rotated
                    txtField.transform = CGAffineTransformIdentity;
                }
            }
        }
        
    }
    
}

- (IBAction)setUpGrowingTextView {
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width , 55)];
    containerView.backgroundColor = [UIColor colorWithRed:245/255.f green:245/255.f blue:245/255.f alpha:1];
    containerView.layer.borderWidth = 1.f;
    containerView.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    accesryTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 8, self.view.frame.size.width - 60, 40)];
    accesryTextField.layer.borderWidth = 1.f;
    accesryTextField.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    accesryTextField.layer.cornerRadius = 5;
    accesryTextField.tag = -1;
    accesryTextField.font = [UIFont fontWithName:CommonFont size:14];
    accesryTextField.delegate = self;
    accesryTextField.backgroundColor = [UIColor whiteColor];
    accesryTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    accesryTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    accesryTextField.keyboardType=UIKeyboardTypeASCIICapable;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    accesryTextField.leftView = paddingView;
    accesryTextField.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:containerView];
    [containerView addSubview:accesryTextField];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(containerView.frame.size.width - 50, 5, 40, 40);
    [doneBtn setImage:[UIImage imageNamed:@"Send_Button.png"]forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(applyEditedText) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height  - (keyboardBounds.size.height + containerFrame.size.height);
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    
    // commit animations
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    // commit animations
    [UIView commitAnimations];
}

-(void)applyEditedText{
    
    if ([currentSelectedView isKindOfClass:[UITextFieldWithColorInfo class]]) {
        UITextFieldWithColorInfo * txtField = (UITextFieldWithColorInfo*)currentSelectedView;
        txtField.text = accesryTextField.text;
        [self adjustTextFieldFrameRespectToFontSizeWith:txtField shouldApplyRotation:YES];
    }
    
    [self resetAllSelections];
}



#pragma mark - Utility Methods

- (BOOL)numberValidation:(NSString *)text {
    NSString *regex = @"^([0-9]*[.][0-9]*[.][0-9]*)$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isValid = [test evaluateWithObject:text];
    return isValid;
}


-(void)convertUSFoneNumberFormatWithField:(UITextFieldWithColorInfo*)textField{
    
    NSInteger length = textField.text.length;
    switch (length) {
        case 3:
             textField.text = [NSString stringWithFormat:@"%@.",textField.text];
            break;
        case 7:
            textField.text = [NSString stringWithFormat:@"%@.",textField.text];
            break;
            
        default:
            break;
    }
   
}

-(UIImage*)getScreenShot{
    [self resetAllSelections];
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions(vwBackground.bounds.size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext(vwBackground.bounds.size);
  
    [vwBackground.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // UIImage* flippedImage = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationDown];
    return image;

}

-(float)getPercentageForXBy:(float)value{
    
    float denominator = canvasWidth / 100;
    float percentage = value / denominator;
    return percentage;
}

-(float)getPercentageForYBy:(float)value{
    
    float denominator = canvasHeight / 100;
    float percentage = value / denominator;
    return percentage;
}


-(float)getXPositionFromPercentageBy:(float)value{
    
    float percentage = canvasWidth * (value / 100);
    return percentage;
}

-(float)getYPositionFromPercentageBy:(float)value{
    
    float percentage = canvasHeight * (value / 100);
    return percentage;
}

-(void)getCanvasSize{
    canvasWidth  = vwBackground.frame.size.width;
    canvasHeight = vwBackground.frame.size.height;
    
}

-(void)adjustTextFieldFrameRespectToFontSizeWith:(UITextFieldWithColorInfo*)textField shouldApplyRotation:(BOOL)shouldApplyRotation{
    
    if (textField.tag < 0) {
        
    }else{
        
        NSString *reviews = textField.text;
        float widthPadding = 0;
        float heightPadding = 0;
        widthPadding = kExtraTextPadding;
        CGSize size = [reviews boundingRectWithSize:CGSizeMake(vwBackground.frame.size.width, vwBackground.frame.size.height)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{
                                                      NSFontAttributeName : [UIFont fontWithName:textField.font.fontName size:textField.font.pointSize]
                                                      }
                                            context:nil].size;
        
        textField.frame = CGRectMake(textField.frame.origin.x , textField.frame.origin.y, size.width + widthPadding, size.height + heightPadding );
        
        if (shouldApplyRotation) {
            if (textField.tag > 0) {
                if (textField.isTransformed) {
                    textField.layer.anchorPoint = CGPointMake(0, 0);
                    textField.transform = textField.rotation;
                    
                }
            }
        }
        
    }
    
   
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    cString = [cString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
- (NSString *)hexStringForColor:(UIColor *)color {
    NSString *hexString;
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    if (components) {
        CGFloat r = components[0];
        CGFloat g = components[1];
        CGFloat b = components[2];
        hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    }
    
    return hexString;
}





-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}



-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
