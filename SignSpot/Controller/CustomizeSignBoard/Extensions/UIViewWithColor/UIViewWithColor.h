//
//  UIViewWithColor.h
//  SignSpot
//
//  Created by Purpose Code on 06/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewWithColor : UIView

@property (nonatomic,strong) UIColor *bgColor;
@property (nonatomic,strong) UIColor *borderColor;

@end
