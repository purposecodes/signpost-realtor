//
//  UITextFieldWithColor.m
//  SignSpot
//
//  Created by Purpose Code on 28/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "UITextFieldWithColorInfo.h"

@implementation UITextFieldWithColorInfo

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (void) drawRect:(CGRect)rect {
    
    if (_isEditing) {
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(context, [[UIColor grayColor] CGColor]);
        CGFloat dashes[] = {5,5};
        CGContextSetLineDash(context, 0.0, dashes, 2);
        CGContextSetLineWidth(context, 5.0);
        CGContextMoveToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextSetShouldAntialias(context, NO);
        CGContextStrokePath(context);
    }else{
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(context, [[UIColor clearColor] CGColor]);
        CGContextMoveToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextSetShouldAntialias(context, NO);
        CGContextStrokePath(context);

    }
    
}


@end
