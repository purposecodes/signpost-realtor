//
//  UITextFieldWithColor.h
//  SignSpot
//
//  Created by Purpose Code on 28/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextFieldWithColorInfo : UITextField

@property (nonatomic,assign) BOOL isBGColorEnabled;
@property (nonatomic,assign) BOOL shouldActivate;
@property (nonatomic,assign) BOOL isEditing;
@property (nonatomic,assign) BOOL isTransformed;
@property (nonatomic,assign) BOOL leaveFrame;
@property (nonatomic,assign) BOOL isPhoneNumber;
@property (nonatomic,assign) CGAffineTransform rotation ;

@end
