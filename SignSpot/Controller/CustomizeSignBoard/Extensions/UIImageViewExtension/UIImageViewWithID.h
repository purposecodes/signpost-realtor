//
//  UIIMageViewWithID.h
//  SignSpot
//
//  Created by Purpose Code on 01/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageViewWithID : UIImageView

@property (nonatomic,assign) NSInteger imageID;

@end
