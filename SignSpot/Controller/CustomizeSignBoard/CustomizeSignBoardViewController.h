//
//  CustomizeSignBoardViewController.h
//  SignSpot
//
//  Created by Purpose Code on 01/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomizeSignBoardViewController : UIViewController

@property (nonatomic,strong) NSString *strPrice;
@property (nonatomic,strong) NSString  *productID;

@property (nonatomic,strong) NSString  *strSelectedSignTypeID;
@property (nonatomic,strong) NSString  *strSelectedSignSizeID;
@property (nonatomic,assign) NSInteger strPrintSideInfoID;
@property (nonatomic,strong) NSString  *strCategoryID;
@property (nonatomic,assign) NSInteger  quantity;
@property (nonatomic,assign) NSInteger  cartID;

@property (nonatomic,assign) BOOL isFromUserTemplate;

@end
