//
//  CustomizeSignBoardWebPageController.h
//  SignPost
//
//  Created by Purpose Code on 02/03/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomizeSignBoardWebPageController : UIViewController

@property (nonatomic,strong) NSString *strTemplateID;
@property (nonatomic,strong) NSString *strSignQuantity;
@property (nonatomic,strong) NSString *strPrintType;
@property (nonatomic,strong) NSString *strSignSizeID;
@property (nonatomic,strong) NSString *strSignType;




@end
