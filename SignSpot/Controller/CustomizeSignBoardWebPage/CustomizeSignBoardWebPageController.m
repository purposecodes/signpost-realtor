//
//  CustomizeSignBoardWebPageController.m
//  SignPost
//
//  Created by Purpose Code on 02/03/17.
//  Copyright © 2017 Purpose Code. All rights reserved.
//

#import "CustomizeSignBoardWebPageController.h"
#import "Constants.h"
#import "URLParser.h"
#import "PlotTemplateOnMapViewController.h"

@interface CustomizeSignBoardWebPageController (){
    
    IBOutlet UIWebView *webView;
}

@end

@implementation CustomizeSignBoardWebPageController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self loadSignBoardWithUserID:[User sharedManager].userId signTemplateID:_strTemplateID signQuantity:_strSignQuantity printType:_strPrintType signTypeId:_strSignType signSizeID:_strSignSizeID];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)loadSignBoardWithUserID:(NSString*)userID signTemplateID:(NSString*)signTemplateID signQuantity:(NSString*)signQuanity printType:(NSString*)printType signTypeId:(NSString*)signTypeId signSizeID:(NSString*)signSizeID{
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSString *urlString = [NSString stringWithFormat:@"http://purposecodes.com/signpost/index.php?page=mobileeditor&signtemplate_id=%@&sign_qty=%@&print_type=%@&user_id=%@&sizeid=%@&sign_type=%@",signTemplateID,signQuanity,printType,userID,signSizeID,signTypeId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];

}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self hideLoadingScreen];
    [self showLoadingScreen];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideLoadingScreen];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Customize Sign Board" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self hideLoadingScreen];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
        NSString *urlString = request.URL.absoluteString;
        BOOL result = [[urlString lowercaseString] hasPrefix:@"http://signpost/"];
        // Final URL "Click to view odrer details"
        if (result) {
            
            URLParser *parser = [[URLParser alloc] initWithURLString:urlString];
            NSString *cartID = [parser valueForVariable:@"cart_id"];
            if (cartID.length) {
                [self showMapViewWithCartID:[cartID integerValue]];
                //[self showSummaryPageWithOrderId:[queryStringDictionary objectForKey:@"orderId"]];
            }
            
            
            return NO;
        }
    
    return YES;
}

-(void)showMapViewWithCartID:(NSInteger)cartID{
    
    PlotTemplateOnMapViewController *mapView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MapDetailsAndListingStoryBoard Identifier:StoryBoardIdentifierForTemplateOnMapPage];
    mapView.cartID              = cartID;
    mapView.numberOfBoardsRequired = [_strSignQuantity integerValue];
    [self.navigationController pushViewController:mapView animated:YES];
    
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading..";
    hud.removeFromSuperViewOnHide = YES;
    
}

-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
