//
//  TemplateListViewController.m
//  SignSpot
//
//  Created by Purpose Code on 13/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

static NSString *CollectionViewCellIdentifier = @"TemplateCollectionViewCellIentifier";

#define kPadding                10
#define kDefaultNumberOfCells   1

#import "UserCustomTemplateListViewController.h"
#import "TemplateCollectionViewCell.h"
#import "TemplateListingHeaderView.h"
#import "TemplateDetailViewController.h"
#import "Constants.h"
#import "MyCartListingViewController.h"
#import "CustomizeSignBoardViewController.h"

@interface UserCustomTemplateListViewController () <ProductCellDelegate>{
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UIView *vwPaginationPopUp;
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblbadge;
    IBOutlet NSLayoutConstraint *paginationBottomConstraint;
    NSMutableArray *arrTemplates;
    NSInteger totalPages;
    NSInteger currentPage;
    
    BOOL isPageRefresing;
    BOOL isDataAvailable;
    UIRefreshControl *refreshControl;
    
}


@end

@implementation UserCustomTemplateListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self getAllProductsByPagination:NO withPageNumber:currentPage];
    // Do any additional setup after loading the view.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{
    
    UINib *cellNib = [UINib nibWithNibName:@"TemplateCollectionViewCell" bundle:nil];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    collectionView.hidden = true;

    arrTemplates = [NSMutableArray new];
    lblTitle.text = _strCategoryTitle;
    currentPage = 1;
    totalPages = 0;
    lblbadge.layer.cornerRadius = 10.0;
    lblbadge.layer.borderWidth = 1.f;
    lblbadge.layer.borderColor = [UIColor clearColor].CGColor;
    lblbadge.clipsToBounds = YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [collectionView addSubview:refreshControl];
    collectionView.alwaysBounceVertical = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self updateCart];
    //[self.view bringSubviewToFront:vwPaginationPopUp];
}

-(void)updateCart{
    
    lblbadge.hidden = false;
    
    lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
    lblbadge.hidden = false;
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }

}


#pragma mark - Methods for  Getting All Templates by Pagination as well as Intial load.


-(void)getAllProductsByPagination:(BOOL)isPagination withPageNumber:(NSInteger)pageNumber{
    
    if (!isPagination) {
          [self showLoadingScreen];
    }
    
    [APIMapper getUserTemplatesWith:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        collectionView.hidden = false;
        isPageRefresing = NO;
        [self getTemplatesFromResponds:responseObject];
        [collectionView reloadData];
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        collectionView.hidden = false;
        isDataAvailable = false;
        isPageRefresing = NO;
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [collectionView reloadData];
    }];
}

-(void)getTemplatesFromResponds:(NSDictionary*)responseObject{
    
    isDataAvailable = true;
    if ( NULL_TO_NIL([responseObject objectForKey:@"template"])) {
        NSArray *templates = [responseObject objectForKey:@"template"];
        for (NSDictionary *dict in templates) {
            [arrTemplates addObject:dict];
        }
    }
  
    if (!arrTemplates.count){
        isDataAvailable = false;
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    }else{
        
        
        UINib *cellNib = [UINib nibWithNibName:@"TemplateCollectionViewCell" bundle:nil];
        [collectionView registerNib:cellNib forCellWithReuseIdentifier:CollectionViewCellIdentifier];

    }

}

-(void)updateSharedManagerWith:(NSDictionary*)responseObject{
    
    // Update cart count for the user
    
    if ( NULL_TO_NIL([responseObject objectForKey:@"cartcount"]))[User sharedManager].cartCount = [[responseObject objectForKey:@"cartcount"] integerValue];
     lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
    [Utility saveUserObject:[User sharedManager] key:@"USER"];
    lblbadge.hidden = false;
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }
}

-(void)refreshData{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    [self showLoadingScreen];
    currentPage = 1;
    [APIMapper getUserTemplatesWith:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        isPageRefresing = NO;
        [arrTemplates removeAllObjects];
        [self getTemplatesFromResponds:responseObject];
        [collectionView reloadData];
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isPageRefresing = NO;
        isDataAvailable = false;
        [refreshControl endRefreshing];
        [collectionView reloadData];
        [self hideLoadingScreen];
    }];

}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kDefaultNumberOfCells;
    return arrTemplates.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
        
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        return cell;

    }
    TemplateCollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    cell.vwShare.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    cell.vwCart.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    cell.btnDelete.hidden = false;
    cell.delegate  = self;
    cell.trailingConstraint.constant = 50;
    [cell setUp];
    cell.vwBackground.layer.borderWidth = 1.f;
    cell.vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
    if (indexPath.row < arrTemplates.count) {
        
        NSDictionary *template = arrTemplates[indexPath.row];
        
        if (NULL_TO_NIL([template objectForKey:@"signtemplate_title"]))
            cell.lblProductName.text = [NSString stringWithFormat:@"  %@",[[template objectForKey:@"signtemplate_title"] uppercaseString]];
        
        if (NULL_TO_NIL([template objectForKey:@"rating"]))
            cell.starRatingImage.rating = [[template objectForKey:@"rating"] floatValue];
        
        if (NULL_TO_NIL([template objectForKey:@"total_review"])) 
           cell.lblRate.text = [NSString stringWithFormat:@"(%@)",[template objectForKey:@"total_review"]];
        
        if (NULL_TO_NIL([template objectForKey:@"signtemplate_image"])) {
            
            if ([template objectForKey:@"signtemplate_image"] && [[template objectForKey:@"signtemplate_image"] length]){
                [cell.activityIndicator startAnimating];
                [cell.templateImage sd_setImageWithURL:[NSURL URLWithString:[template objectForKey:@"signtemplate_image"]]
                                      placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                 [cell.activityIndicator stopAnimating];
                                                 
                                             }];
                
            }}

        }
      
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger columns = 2;
    if (!isDataAvailable)
        columns = 1;
    
    
    float percentage = 80;
    float width = _collectionView.bounds.size.width / columns;
    float delta = 43;
    float height = (width * percentage) / 100 + delta;
    return CGSizeMake(width, height);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)_collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        TemplateListingHeaderView *headerView = [_collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerView.lblCategoryTitle.text = [NSString stringWithFormat:@"Showing %lu Items",(unsigned long)arrTemplates.count];
        reusableview = headerView;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return CGSizeZero;
    return CGSizeMake(_collectionView.bounds.size.width, 40);
        
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; // This is the minimum inter item spacing, can be more
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    

}

#pragma mark - Productcell Delegate Methods

-(void)deleteButtonApplied:(NSInteger)row column:(NSInteger)column{
    
    if (row < arrTemplates.count) {
        NSDictionary *templateInfo = arrTemplates[row];
        if (NULL_TO_NIL([templateInfo objectForKey:@"delete_id"])) {
            [self showLoadingScreen];
            NSString *deleteID = [templateInfo objectForKey:@"delete_id"];
            [APIMapper deleteUserTemplateWithID:deleteID userID:[User sharedManager].userId success:^(AFHTTPRequestOperation *operation, id responseObject){
                
                if ( NULL_TO_NIL([[responseObject objectForKey:@"header"]objectForKey:@"text"])) {
                    [[[UIAlertView alloc] initWithTitle:@"Delete" message:[[responseObject objectForKey:@"header"]objectForKey:@"text"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                }
                [self hideLoadingScreen];
                [self refreshData];
                
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                
            [[[UIAlertView alloc] initWithTitle:@"Delete" message:@"Failed to delete the Template." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                
                [self hideLoadingScreen];
            }];
            
        }
    }
}

-(void)getSelectedProductFromPosition:(NSInteger)row column:(NSInteger)column{
    
    if (row < arrTemplates.count) {
        TemplateDetailViewController *templateDetailView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ProductDetailsStoryBoard Identifier:StoryBoardIdentifierForTemplateDetailView];
        NSDictionary *templateInfo = arrTemplates[row];
        templateDetailView.arrTemplates = arrTemplates;
        templateDetailView.startingIndex = row;
        templateDetailView.strCategoryName = [templateInfo objectForKey:@"category_title"];
        [[self navigationController]pushViewController:templateDetailView animated:YES];
        
    }

}

-(void)cartButtonAppliedFromPoistion:(NSInteger)row column:(NSInteger)column{
    
    if (row < arrTemplates.count) {
        TemplateDetailViewController *templateDetailView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ProductDetailsStoryBoard Identifier:StoryBoardIdentifierForTemplateDetailView];
        NSDictionary *templateInfo = arrTemplates[row];
        templateDetailView.arrTemplates = arrTemplates;
        templateDetailView.startingIndex = row;
        templateDetailView.strCategoryName = [templateInfo objectForKey:@"category_title"];
        [[self navigationController]pushViewController:templateDetailView animated:YES];
        
    }


}
-(void)shareButtonAppliedFromPoistion:(NSInteger)row column:(NSInteger)column{
    
    NSString *textToShare = @"Look at this awesome website for aspiring iOS Developers!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}

-(void)showPaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hidePaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = -40;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(IBAction)showAllItemsInCart{
    
    MyCartListingViewController *cartPage =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MapDetailsAndListingStoryBoard Identifier:StoryBoardIdentifierForCartListingPage];
    [[self navigationController]pushViewController:cartPage animated:YES];
    
}

-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
