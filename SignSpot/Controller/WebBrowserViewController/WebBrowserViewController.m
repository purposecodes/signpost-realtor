//
//  WebBrowserViewController.m
//  SignPost
//
//  Created by Purpose Code on 11/08/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kSuccessCode                200

#define kHelp                       @"HELP"
#define kPrivacyPolicy              @"PRIVACY POLICY"
#define kTermsOfService             @"TERMS OF SERVICE"

#import "WebBrowserViewController.h"
#import "Constants.h"
#import "OrderDetailsViewController.h"

@interface WebBrowserViewController (){
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UIWebView *webView;
}



@end

@implementation WebBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = _strTitle;
    if ([_strTitle isEqualToString:kHelp]) {
        [self loadContentWithType:@"Help"];
    }
    else if ([_strTitle isEqualToString:kPrivacyPolicy]) {
        [self loadContentWithType:@"Privacy"];
    }
    else if ([_strTitle isEqualToString:kTermsOfService]) {
        [self loadContentWithType:@"Terms"];
    }
    else [self loadPaymentPage];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)loadPaymentPage{
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSString *urlString = _strURL;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webView loadRequest:urlRequest];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self showLoadingScreen];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideLoadingScreen];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:_strTitle message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [self hideLoadingScreen];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (_isPayment) {
        
        NSMutableDictionary *queryStringDictionary = [NSMutableDictionary new];
        NSString *urlString = request.URL.absoluteString;
        BOOL result = [[urlString lowercaseString] hasPrefix:@"http://signpost/?status"];
        // Final URL "Click to view odrer details"
        if (result) {
            
            NSArray *urlComponents = [urlString componentsSeparatedByString:@"&"];
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
                [queryStringDictionary setObject:value forKey:key];
            }
            
            if (NULL_TO_NIL([queryStringDictionary objectForKey:@"orderId"])) {
                [self showSummaryPageWithOrderId:[queryStringDictionary objectForKey:@"orderId"]];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            return NO;
        }
        
    }
    
    return YES;
}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showSummaryPageWithOrderId:(NSString*)orderID{
    
    OrderDetailsViewController *orderDetails =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForOrderDetails];
    orderDetails.showBackBtn = false;
    [[self navigationController]pushViewController:orderDetails animated:YES];
    [orderDetails orderDetailsWithOrderID:orderID];
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [allViewControllers removeObjectAtIndex:allViewControllers.count - 2];
    self.navigationController.viewControllers = allViewControllers;
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading..";
    hud.removeFromSuperViewOnHide = YES;
    
}

-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)showAlertWithTitle:(NSString*)title message:(NSString*)message{
    
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    
}

#pragma mark - Cashing Methods

-(void)loadContentWithType:(NSString*)type{
    
    [self showLoadingScreen];
    [APIMapper getWebContentWithType:type success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self hideLoadingScreen];
        if ([responseObject objectForKey:@"text"]) {
            [self saveDetailsToFlder:[responseObject objectForKey:@"text"] type:type];
        }
        [self showContentFromFolder];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self showContentFromFolder];
        [self hideLoadingScreen];
    }];
    
   
    
}

-(void)saveDetailsToFlder:(NSString*)content type:(NSString*)type{
    
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",type];
    NSString *path = [[self applicationDocumentsDirectory].path
                      stringByAppendingPathComponent:fileName];
    [content writeToFile:path atomically:YES
                   encoding:NSUTF8StringEncoding error:nil];
    
}


- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

-(void)showContentFromFolder{
    
    NSString *type;

    if ([_strTitle isEqualToString:kHelp]) {
        type = @"Help";
    }
    else if ([_strTitle isEqualToString:kPrivacyPolicy]) {
        type = @"Privacy";
    }
    else if ([_strTitle isEqualToString:kTermsOfService]) {
        type = @"Terms";
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",type]];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if (fileExists) {
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
        [webView loadHTMLString:content baseURL:nil];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
