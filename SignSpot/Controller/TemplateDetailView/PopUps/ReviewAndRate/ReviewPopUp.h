//
//  ReviewPopUp.h
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@protocol ReviewPopUpDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Close' Button.
 */

-(void)closeReviewPopUpAfterADelay:(float)delay;

/*!
 *This method is invoked when user Posted a Review Successfully
 */

-(void)reviewPostedSuccessfullyWithResponds:(NSDictionary*)latestReview;



@end



@interface ReviewPopUp : UIView <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UITextViewDelegate,UITextFieldDelegate,EDStarRatingProtocol>{
    
    UITableView *tableView;
    NSString *strReviewTitle;
    NSString *strReviewText;
    float rating;
    UIView *inputAccView;
    NSInteger indexForTextFieldNavigation;
}

@property (nonatomic,weak)  id<ReviewPopUpDelegate>delegate;
@property (nonatomic,strong) NSString *strTemplateID;


-(void)setUp;

@end
