//
//  CustomCellForRateStar.m
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CustomCellForReview.h"

@implementation CustomCellForReview{
    
}

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}

-(void)setUp{
    
    _starRatingImage.starImage = [UIImage imageNamed:@"RatingStarGrey.png"];
    _starRatingImage.starHighlightedImage = [UIImage imageNamed:@"RatingStarFilled.png"];
    _starRatingImage.maxRating = 5.0;
    _starRatingImage.horizontalMargin = 0;
    _starRatingImage.editable = YES;
    _starRatingImage.displayMode=EDStarRatingDisplayHalf;
    
    self.txtReviewText.layer.borderWidth = 1.0f;
    self.txtReviewText.layer.borderColor = [[UIColor getSeperatorColor] CGColor];
    
    self.txtReviewTitle.layer.borderWidth = 1.0f;
    self.txtReviewTitle.layer.borderColor = [[UIColor getSeperatorColor] CGColor];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
