//
//  CustomCellForRateStar.h
//  SignSpot
//
//  Created by Purpose Code on 20/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"


@interface CustomCellForReview : UITableViewCell

@property (nonatomic,weak) IBOutlet  UITextField *txtReviewTitle;
@property (nonatomic,weak) IBOutlet  UITextView *txtReviewText;


@property (nonatomic,weak) IBOutlet  EDStarRating *starRatingImage;

@end
