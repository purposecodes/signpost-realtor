//
//  CustomCellForReviews.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@interface CustomCellForReviews : UITableViewCell

@property (nonatomic,weak) IBOutlet UIView *vwBackground;
@property (nonatomic,weak) IBOutlet UILabel *lblTitle;
@property (nonatomic,weak) IBOutlet UILabel *lblRating;
@property (nonatomic,weak) IBOutlet UILabel *lblDate;
@property (nonatomic,weak) IBOutlet UILabel *lblName;
@property (nonatomic,weak) IBOutlet UILabel *lblReview;

@property (nonatomic,weak) IBOutlet  EDStarRating *starRatingImage;

@end
