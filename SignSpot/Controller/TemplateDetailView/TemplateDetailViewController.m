//
//  TemplateDetailViewController.m
//  SignSpot
//
//  Created by Purpose Code on 17/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kHeaderHeight               45
#define kFooterHeight               45
#define kSectionCount               4
#define kCellHeightForFirstSection  140
#define kDefaultCellHeight          100
#define kCellHeightForSpecification 130
#define kLimitedReviewCount         3
#define kMinimumCellCount           1

#define kMargin                     34
#define kPaddingForDescription      25
#define kPaddingForReview           85

#define kSuccessCode                200

typedef enum{
    
    eTypeImageInfo = 0,
    eTypeDescription = 2,
    eTypeReviews = 3,
    eTypeSpecification = 1
    
}ETemplateInfo;

#import "CustomCellForDescription.h"
#import "CustomizeSignBoardViewController.h"
#import "TemplateDetailViewController.h"
#import "Constants.h"
#import "CustomCellForImageAndPrice.h"
#import "SpecificationFilterViewController.h"
#import "PlotTemplateOnMapViewController.h"
#import "ReviewPopUp.h"
#import "PhotoBrowser.h"
#import "CustomizeSignBoardWebPageController.h"

@interface TemplateDetailViewController ()<SignBoardSpecificationsDelegate,ReviewPopUpDelegate,PriceDetailsCellDelegate,PhotoBrowserDelegate>{
    
    IBOutlet UILabel *lblNavItemName;
    IBOutlet UILabel *lblbadge;
    IBOutlet UIButton *btnCustomize;
    NSDictionary *_templateBasicInfo;
    
    IBOutlet UITableView *tableView;
    ETemplateInfo templateInfo;
    NSString *strTemplateDescription;
    NSString *strURLForImage;
    NSString *strPrice;
    float rateValue;
    NSInteger totalReviews;
    NSString *strTemplateName;
    NSString *strTemplateID;
    
    NSMutableArray *arrReviews;
    NSDictionary *dictResponds;
    NSInteger templateQuantity;
    BOOL isPageRefresing;
    BOOL isDataAvailable;
    BOOL showFilter;
    
    NSString  *strSelectedTemplateTypeName;
    NSString  *strSelectedTemplateSizeName;
    NSString  *strSelectedTemplatePrintTypeName;
    NSString *strCategoryID;
    NSInteger cartID;
    
    
    /******! ID's for calculating the Price from WebService !*******/
    NSString  *strSelectedSignTypeID;
    NSString  *strSelectedSignSizeID;
    NSInteger strPrintSideInfoID;
    NSString *strCompanyID;
    
    ReviewPopUp *reviewPopUp;
    NSMutableArray *arrGallery;
    NSInteger imageIndex;
    UIRefreshControl *refreshControl;
    
    SpecificationFilterViewController *specFilter;
    PhotoBrowser *photoBrowser;
   
}

@end

@implementation TemplateDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    showFilter = true;
    [self setUp];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

-(void)setUp{
    
    btnCustomize.hidden = true;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.hidden = true;
    tableView.alwaysBounceVertical = YES;
    
    arrReviews = [NSMutableArray new];
    templateQuantity = 0;
    
    strSelectedSignTypeID = [NSString new];
    strSelectedSignSizeID = [NSString new];
    strCompanyID = [NSString new];
    arrGallery = [NSMutableArray new];
    lblNavItemName.text = _strCategoryName;
    if (_startingIndex < _arrTemplates.count) _templateBasicInfo = _arrTemplates[_startingIndex];
    [self performSelector:@selector(loadTemplateDetails) withObject:self afterDelay:0];
}


-(void)loadTemplateDetails{
    
    if (NULL_TO_NIL([_templateBasicInfo objectForKey:@"signtemplate_id"]))
        strTemplateID = [_templateBasicInfo objectForKey:@"signtemplate_id"];
    
    if (NULL_TO_NIL([_templateBasicInfo objectForKey:@"cart_id"]))
        cartID = [[_templateBasicInfo objectForKey:@"cart_id"] integerValue];
    
    if (cartID > 0) {
        
        // Its coming from User Template Page
        [self getUsetTemplateDetails];
        
    }else{
        
        [self getTemplateDetailsWithTemplateID:strTemplateID];
        // Ist coming from Normal category
    }
    
}


-(void)refreshData{
    
    [arrGallery removeAllObjects];
    [arrReviews removeAllObjects];
    if (cartID > 0)[self getUsetTemplateDetails];
    else [self getTemplateDetailsWithTemplateID:strTemplateID];
    
}

-(void)getUsetTemplateDetails{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    isPageRefresing = true;
    [self showLoadingScreenWithTitle:@"Loading.."];
    [APIMapper getUserTemplateDetailsWithTemplateID:strTemplateID userID:[User sharedManager].userId cartID:cartID Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        isPageRefresing = false;
        tableView.hidden = false;
        dictResponds = responseObject;
        [self loadContentsFromResponds:responseObject];
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isDataAvailable = false;
        isPageRefresing = false;
        tableView.hidden = false;
        [refreshControl endRefreshing];
        [tableView reloadData];
        [self hideLoadingScreen];
    }];
    
}

-(void)getTemplateDetailsWithTemplateID:(NSString*)templateID{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    isPageRefresing = true;
    [self showLoadingScreenWithTitle:@"Loading.."];
    [APIMapper getTemplateDetailsWithTemplateID:templateID userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        isPageRefresing = false;
        tableView.hidden = false;
        dictResponds = responseObject;
        [self loadContentsFromResponds:responseObject];
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isDataAvailable = false;
        isPageRefresing = false;
        tableView.hidden = false;
        [refreshControl endRefreshing];
        [tableView reloadData];
        [self hideLoadingScreen];
    }];
    
}

-(void)loadContentsFromResponds:(NSDictionary*)responds{
    
    /** ! Basic info from Header !**/
    if ( NULL_TO_NIL( [responds objectForKey:@"header"])) {
        if ([[responds objectForKey:@"header"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *headerDetails = [responds objectForKey:@"header"];
            if (headerDetails) {
                
                if ( NULL_TO_NIL([headerDetails objectForKey:@"signtemplate_info"])) {
                    strTemplateDescription = [headerDetails objectForKey:@"signtemplate_info"];
                    
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"signtemplate_image"])) {
                    strURLForImage = [headerDetails objectForKey:@"signtemplate_image"];
                    [arrGallery addObject:strURLForImage];
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"rating"])) {
                    rateValue = [[headerDetails objectForKey:@"rating"] floatValue];
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"total_review"])) {
                    totalReviews = [[headerDetails objectForKey:@"total_review"] integerValue];
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"signtemplate_price"])) {
                    
                    if ([[headerDetails objectForKey:@"signtemplate_price"] isKindOfClass:[NSString class]]) {
                        strPrice = [headerDetails objectForKey:@"signtemplate_price"];
                    }else{
                        strPrice = [[headerDetails objectForKey:@"signtemplate_price"] stringValue];
                    }
                    
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"signtemplate_title"])) {
                    strTemplateName = [headerDetails objectForKey:@"signtemplate_title"];
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"company_id"])) {
                    strCompanyID = [headerDetails objectForKey:@"company_id"];
                }
                if ( NULL_TO_NIL([headerDetails objectForKey:@"category_id"])) {
                    strCategoryID = [headerDetails objectForKey:@"category_id"];
                }
                isDataAvailable = true;
                if ([[headerDetails objectForKey:@"code"] integerValue] != kSuccessCode) {
                    isDataAvailable = false;
                }
                
            }
        }
    }
    
    /** ! Get All Reviews !**/
    if ( NULL_TO_NIL( [responds objectForKey:@"review"])) {
        if ([[responds objectForKey:@"review"] isKindOfClass:[NSArray class]]) {
            NSArray *reviews = [responds objectForKey:@"review"];
            if (reviews.count) {
                arrReviews = [NSMutableArray arrayWithArray:reviews];
                
            }
        }
    }
    [arrReviews removeAllObjects];
    
    /** ! Get Default Specifications Titles and IDs !**/
    
    if ( NULL_TO_NIL( [responds objectForKey:@"signtype"])) {
        if ([[responds objectForKey:@"signtype"] isKindOfClass:[NSArray class]]){
            NSArray *types =[responds objectForKey:@"signtype"];
            if (types.count)
                strSelectedTemplateTypeName = [types[0] objectForKey:@"signtemplate_title"];
            strSelectedSignTypeID = [types[0] objectForKey:@"signtype_id"];
        }
    }
    
    if ( NULL_TO_NIL( [responds objectForKey:@"signsize"])) {
        if ([[responds objectForKey:@"signsize"] isKindOfClass:[NSArray class]]){
            NSArray *types =[responds objectForKey:@"signsize"];
            if (types.count)
                strSelectedTemplateSizeName = [types[0] objectForKey:@"signsize_title"];
            strSelectedSignSizeID = [types[0] objectForKey:@"signsize_id"];
        }
        
    }
    
    if ( NULL_TO_NIL( [responds objectForKey:@"printtype"])) {
        if ([[responds objectForKey:@"printtype"] isKindOfClass:[NSArray class]]){
            NSArray *types =[responds objectForKey:@"printtype"];
            if (types.count)
                strSelectedTemplatePrintTypeName = [types[0] objectForKey:@"printtype_title"];
            //strPrintSideInfoID = [[types[0] objectForKey:@"print_type"] integerValue];
        }
        
    }
    
    /** ! Get Gallery Images for slide show !**/
    
    if ( NULL_TO_NIL([responds objectForKey:@"gallery"])) {
        if ( [[responds objectForKey:@"gallery"] isKindOfClass:[NSArray class]]){
            NSArray *types =[responds objectForKey:@"gallery"];
            for (NSDictionary *dict in types){
                if ([dict objectForKey:@"gallery_image"]) {
                    [arrGallery addObject:[dict objectForKey:@"gallery_image"]];
                }
            }
            
        }
        
    }
    
    [tableView reloadData];
    if (showFilter)[self showFilter:nil];
    showFilter = false;
    btnCustomize.hidden = false;
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!isDataAvailable) return 1;
    
    if (arrReviews.count <= 0) {
        return kSectionCount - 1;
    }
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = kMinimumCellCount;
    if (!isDataAvailable) return count;
    if ((section == eTypeDescription) || (section == eTypeImageInfo) || (section == eTypeSpecification)) return count = 1;
    else if (section == eTypeReviews) {
        
        if (arrReviews.count > kLimitedReviewCount) {
            return kLimitedReviewCount;
        }else return arrReviews.count;
        
    }
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Details found."];
        return cell;
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell = [self configureCellForIndexPath:indexPath];
    //Assigning the section number to collectionview , to get the section number once tapped the product.
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger cellHeight = 0;
    if (indexPath.section == eTypeImageInfo)cellHeight = [self getHeightForImage];
    else if (indexPath.section == eTypeSpecification) cellHeight = kCellHeightForSpecification;
    else if (indexPath.section == eTypeDescription) cellHeight = [self getDynamicHeightForDecsriptionWith:indexPath];
    else if (indexPath.section == eTypeReviews)cellHeight = [self getDynamicHeightForReviewWith:indexPath];
    else cellHeight = 50;
    return cellHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return nil;
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor whiteColor];
    vwHeader.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    vwHeader.layer.borderWidth = 1.f;
    UILabel *lblTitle = [UILabel new];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblTitle]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    
    if (section == eTypeImageInfo){
        
        vwHeader.layer.borderColor = [UIColor clearColor].CGColor;
        vwHeader.layer.borderWidth = 0.f;
        vwHeader.backgroundColor = [UIColor getHeaderOffBlackColor];
        lblTitle.text = [strTemplateName uppercaseString];
        lblTitle.textColor = [UIColor whiteColor];
        
    }
    else if (section == eTypeSpecification) lblTitle.text = @"SPECIFICATIONS";
    else if (section == eTypeDescription) lblTitle.text = @"DESCRIPTION";
    else if (section == eTypeReviews){
        
        lblTitle.text = [NSString stringWithFormat:@"Reviews (%lu)",(unsigned long)totalReviews];
        if (totalReviews > kLimitedReviewCount) {
            
            // VIEW ALL REVIEWS
            
            UIButton* btnAllReviews = [UIButton new];
            [vwHeader addSubview:btnAllReviews];
            [btnAllReviews setBackgroundColor:[UIColor clearColor]];
            btnAllReviews.translatesAutoresizingMaskIntoConstraints = NO;
            [btnAllReviews setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btnAllReviews.titleLabel.font = [UIFont fontWithName:CommonFontBold size:14];
            [btnAllReviews setTitle:@"View All" forState:UIControlStateNormal];
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnAllReviews
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:0.0]];
            
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnAllReviews
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0.0]];
            
            
            [btnAllReviews addConstraint:[NSLayoutConstraint constraintWithItem:btnAllReviews
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeWidth
                                                                     multiplier:1.0
                                                                       constant:80]];
            
            [btnAllReviews addConstraint:[NSLayoutConstraint constraintWithItem:btnAllReviews
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeHeight
                                                                     multiplier:1.0
                                                                       constant:40]];
        }
        
        
    }
    
    lblTitle.font = [UIFont fontWithName:CommonFontBold size:14];
    return vwHeader;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return 0.1;
    if (section == eTypeImageInfo) return kHeaderHeight;
    return kHeaderHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    float footerHeight = .001;
    return  footerHeight;
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    UITableViewCell *cell;
    
    if (indexPath.section == eTypeImageInfo) {
        
        static NSString *CellIdentifier = @"CellIdentifierForImageAndPrice";
        CustomCellForImageAndPrice *cell = (CustomCellForImageAndPrice *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellForImageAndPrice" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }
        
        imageIndex = 0;
        cell.lblReviewCount.text = [NSString stringWithFormat:@"(%ld)",(long)totalReviews];
        cell.lblPrice.text = [NSString stringWithFormat:@"$%.02f",[strPrice floatValue]];
        cell.starRatingImage.rating = rateValue;
        cell.delegate = self;
        cell.btnNext.hidden = true;
        cell.btnPrev.hidden = true;
        [cell.btnFilter addTarget:self action:@selector(showFilter:) forControlEvents:UIControlEventTouchUpInside];
        
        if (arrGallery.count > 1) {
            cell.btnNext.hidden = false;
            cell.btnNext.enabled = true;
            cell.btnPrev.hidden = false;
            cell.btnPrev.enabled = false;
        }
        if (strURLForImage.length) {
            [cell.imgTemplate sd_setImageWithURL:[NSURL URLWithString:strURLForImage]
                                placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           [cell.indicator stopAnimating];
                                           
                                           [UIView transitionWithView:cell.imgTemplate
                                                             duration:.5f
                                                              options:UIViewAnimationOptionTransitionCrossDissolve
                                                           animations:^{
                                                               cell.imgTemplate.image = image;
                                                           } completion:nil];
                                           
                                       }];
        }
        
        return cell;
        
    }else if (indexPath.section == eTypeDescription){
        
        static NSString *CellIdentifier = @"CustomCellForDescription";
        CustomCellForDescription *cell = (CustomCellForDescription *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellForDescription" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
            
        }
        cell.lblDescription.text = [strTemplateDescription stringByStrippingHTML];
        if (strTemplateDescription.length <= 0 || [strTemplateDescription isEqualToString:@"No Description Available."])  {
            strTemplateDescription = @"No Description Available.";
            cell.lblDescription.text = strTemplateDescription;
            cell.lblDescription.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:14];
        }
        
        cell.vwBorder.layer.borderWidth = 1.f;
        cell.vwBorder.layer.borderColor = [UIColor getSeperatorColor].CGColor;
        return cell;
        
    }else if (indexPath.section == eTypeSpecification){
        
        static NSString *CellIdentifier = @"CustomCellForDescription";
        CustomCellForDescription *cell = (CustomCellForDescription *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellForDescription" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
            
        }
        cell.lblDescription.text = [NSString stringWithFormat:@"Sign Type : %@ \n\nSign Size  : %@ \n\nPrint         : %@ \n\nQuantity   : %ld",strSelectedTemplateTypeName,strSelectedTemplateSizeName,strSelectedTemplatePrintTypeName,(long)templateQuantity];
        cell.lblDescription.font = [UIFont fontWithName:CommonFont size:14];
        
        cell.vwBorder.layer.borderWidth = 1.f;
        cell.vwBorder.layer.borderColor = [UIColor getSeperatorColor].CGColor;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
        
    }else if (indexPath.section == eTypeReviews){
        
        
    }else{
        
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
    
}


-(float)getDynamicHeightForDecsriptionWith:(NSIndexPath*)indexPath{
    
    CGSize size = [[strTemplateDescription stringByStrippingHTML] boundingRectWithSize:CGSizeMake(tableView.frame.size.width - 20, MAXFLOAT)
                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                            attributes:@{
                                                                                         NSFontAttributeName : [UIFont fontWithName:CommonFont size:14]
                                                                                         }
                                                                               context:nil].size;
    
    
    return size.height + kPaddingForDescription ;
}

-(float)getDynamicHeightForReviewWith:(NSIndexPath*)indexPath{
    
    if (indexPath.row < arrReviews.count) {
        
        NSDictionary *review = arrReviews[indexPath.row];
        if ( NULL_TO_NIL( [review objectForKey:@"review_details"])) {
            NSString *reviews = [review objectForKey:@"review_details"];
            CGSize size = [reviews boundingRectWithSize:CGSizeMake(self.view.frame.size.width - kMargin, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:@{
                                                          NSFontAttributeName : [UIFont fontWithName:CommonFont size:15]
                                                          }
                                                context:nil].size;
            return size.height + kPaddingForReview;
        }
    }
    
    return kDefaultCellHeight;
    
}





#pragma mark - Show  SignBoard Rating PopUp and Delegates

-(IBAction)showComposeReviewPopUp{
    
    if (!reviewPopUp) {
        
        reviewPopUp = [ReviewPopUp new];
        [self.view addSubview:reviewPopUp];
        reviewPopUp.delegate = self;
        reviewPopUp.strTemplateID = strTemplateID;
        reviewPopUp.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[reviewPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(reviewPopUp)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[reviewPopUp]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(reviewPopUp)]];
        
        reviewPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            reviewPopUp.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    
    [self.view endEditing:YES];
    [reviewPopUp setUp];
}

-(void)closeReviewPopUpAfterADelay:(float)delay{
    
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        reviewPopUp.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [reviewPopUp removeFromSuperview];
        reviewPopUp = nil;
    }];
}

/**! CallBack when user post a Review successfully !**/

-(void)reviewPostedSuccessfullyWithResponds:(NSDictionary*)latestReview{
    
    [APIMapper getAllReviewsForTemplate:strTemplateID userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ( NULL_TO_NIL([responseObject objectForKey:@"review"])) {
            if ([[responseObject objectForKey:@"review"] isKindOfClass:[NSArray class]]) {
                NSArray *reviews = [responseObject objectForKey:@"review"];
                if (reviews.count) {
                    arrReviews = [NSMutableArray arrayWithArray:reviews];
                }
            }
            
            if ( NULL_TO_NIL([latestReview objectForKey:@"header"])) {
                totalReviews = [[[latestReview objectForKey:@"header"] objectForKey:@"total_review"] integerValue];
            }
            
        }
        if (arrReviews.count) {
            [tableView reloadData];
        }
        
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
    }];
    
}

#pragma mark - Image and Price Info Cell Delegate

/**! CallBack when user tapped "Share" button !**/

-(void)shareButtonTapped{
    
    NSString *textToShare = @"Look at this awesome website for aspiring iOS Developers!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


#pragma mark - Specification Filter Methods

-(IBAction)showFilter:(id)sender{
    
    if (!specFilter) {
        SpecificationFilterViewController *specificationFilter =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ProductDetailsStoryBoard Identifier:StoryBoardIdentifierForSpecificationFilter];
        specFilter = specificationFilter;
    }
    specFilter.delegate = self;
    
    /** ! Get All Sign Types !**/
    if ( NULL_TO_NIL( [dictResponds objectForKey:@"signtype"])) {
        if ([[dictResponds objectForKey:@"signtype"] isKindOfClass:[NSArray class]]){
            specFilter.signTypes = [dictResponds objectForKey:@"signtype"];
            if (!strSelectedSignTypeID.length) {
                specFilter.strSelectedSignTypeID = [specFilter.signTypes[0] objectForKey:@"signtype_id"];
                specFilter.strSignTypeInfo = [specFilter.signTypes[0] objectForKey:@"signtemplate_title"];
                
                
            }else{
                
                specFilter.strSelectedSignTypeID = strSelectedSignTypeID;
            }
        }
        
    }
    
    
    /** ! Get All Sign Sizes !**/
    if ( NULL_TO_NIL( [dictResponds objectForKey:@"signsize"])) {
        if ([[dictResponds objectForKey:@"signsize"] isKindOfClass:[NSArray class]])
            specFilter.signSize = [dictResponds objectForKey:@"signsize"];
        
        if (!strSelectedSignSizeID.length) {
            specFilter.strSelectedSignBoardSizeID = [specFilter.signSize[0] objectForKey:@"signsize_id"];
            specFilter.strSignSizeInfo = [specFilter.signSize[0] objectForKey:@"signsize_title"];
        }else{
            
            specFilter.strSelectedSignBoardSizeID = strSelectedSignSizeID;
        }
    }
    
    
    /** ! Get All Print Types !**/
    if ( NULL_TO_NIL( [dictResponds objectForKey:@"printtype"])) {
        if ([[dictResponds objectForKey:@"printtype"] isKindOfClass:[NSArray class]])
            specFilter.printTypes = [dictResponds objectForKey:@"printtype"];
        
        if (!strPrintSideInfoID) {
            //specFilter.strSelectedPrintTypeID = [[specFilter.printTypes[0] objectForKey:@"print_type"] integerValue];
            //specFilter.strPrintSideInfo = [specFilter.printTypes[0] objectForKey:@"printtype_title"];
            
        }else{
            
            specFilter.strSelectedPrintTypeID = strPrintSideInfoID;
        }
        
    }
    
    specFilter.quantity = templateQuantity;
    
    [self addChildViewController:specFilter];
    UIView *vwPopUP = specFilter.view;
    [self.view addSubview:vwPopUP];
    vwPopUP.backgroundColor = [UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.7];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
    }];
}

-(void)getSelectedSignSpecificationsFromFilterWith:(NSString*)signTypeID SignSizeID:(NSString*)signSizeID printTypeID:(NSInteger)printTypeID quantity:(NSInteger)quantity signTypeTitle:(NSString*)signTypeTitle signSizeTitle:(NSString*)signSizeTitle printTypeTitle:(NSString*)printTypeTitle {
    
    
    if ([strTemplateID length] && [signTypeID length] && [signSizeID length] && [strCompanyID length]) {
        
        [self showLoadingScreenWithTitle:@"Processing.."];
        
        [APIMapper getCalculatedPriceForTheTemplateWithTemplateID:strTemplateID userID:[User sharedManager].userId signTypeID:signTypeID signSizeID:signSizeID printType:printTypeID companyID:strCompanyID quantity:quantity Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if (responseObject && [responseObject objectForKey:@"sign_price"]) {
                strPrice = [[responseObject objectForKey:@"sign_price"] stringValue];
                if (signTypeID.length)  strSelectedSignTypeID               = signTypeID;
                if (signSizeID.length)  strSelectedSignSizeID               = signSizeID;
                strPrintSideInfoID                  = printTypeID;
                templateQuantity                    = quantity;
                if (signTypeTitle.length) strSelectedTemplateTypeName       = signTypeTitle;
                if (signSizeTitle.length) strSelectedTemplateSizeName       = signSizeTitle;
                if (printTypeTitle.length) strSelectedTemplatePrintTypeName = printTypeTitle;
                
                NSRange range = NSMakeRange(0, 3);
                if (range.location == NSNotFound) {
                    
                } else {
                    //There is a valid range.
                    NSIndexSet *sections = [[NSIndexSet alloc] initWithIndexesInRange:range];
                    [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
                    
                }
            }
            
            [self hideLoadingScreen];
            [self closePopUp];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            [self hideLoadingScreen];
            [self closePopUp];
        }];
    }
    
    
}

-(void)closePopUp{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        specFilter.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [specFilter.view removeFromSuperview];
        [specFilter removeFromParentViewController];
        specFilter = nil;
        
    }];
    
}

-(IBAction)showExpandImage{
    
    
    if (imageIndex < arrGallery.count) {
        
        NSMutableArray *arrMapImage = [[NSMutableArray alloc] initWithObjects:[NSURL URLWithString:arrGallery[imageIndex]], nil];
        
        if (!photoBrowser) {
            photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
            photoBrowser.delegate = self;
        }
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        UIView *vwPopUP = photoBrowser;
        [app.window.rootViewController.view addSubview:vwPopUP];
        vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
        [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
        [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
        
        vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            vwPopUP.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
            [photoBrowser setUpWithImages:arrMapImage andIndex:0];
        }];
        
        
        
    }
    
}

-(void)closePhotoBrowserView{
    
    if (photoBrowser) {
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
            [photoBrowser removeFromSuperview];
            photoBrowser = nil;
        }];
        
        
    }
}
#pragma mark - Show Image Gallery


-(void)showGalleryImageWith:(UIImageView*)imageView withIndicator:(UIActivityIndicatorView*)indicator isNext:(BOOL)isNext btnPrev:(UIButton*)btnPrev btnNext:(UIButton*)btnNext{
    
    btnNext.enabled = true;
    btnPrev.enabled = true;
    
    if (isNext) {
        
        if (imageIndex + 1 < arrGallery.count)imageIndex += 1;
        
    }else{
        
        if (imageIndex - 1 >= 0)imageIndex -= 1;
    }
    
    if (imageIndex == arrGallery.count - 1)btnNext.enabled = false;
    if (imageIndex == 0) btnPrev.enabled = false;
    
    if (imageIndex < arrGallery.count){
        
        NSString *url = [arrGallery objectAtIndex:imageIndex];
        if (url.length) {
            [indicator startAnimating];
            [imageView sd_setImageWithURL:[NSURL URLWithString:url]
                         placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    [indicator stopAnimating];
                                    
                                    [UIView transitionWithView:imageView
                                                      duration:.5f
                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                    animations:^{
                                                        imageView.image = image;
                                                    } completion:nil];
                                    
                                }];
        }
    }
    
}


#pragma mark - Generic Methods




-(CGFloat)getHeightForImage{
    
    float padding = 20;
    float percentage = 80;
    float delta = 125;
    float width = self.view.frame.size.width - padding;
    float height = ((width - padding) * percentage) / 100 + delta;
    return height;
}


-(IBAction)customizeSignBoard{
    
    CustomizeSignBoardWebPageController *CustomizePage = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForCustomizeSignWebPage];
     CustomizePage.strPrintType = [NSString stringWithFormat:@"%d",strPrintSideInfoID];
     CustomizePage.strTemplateID = strTemplateID;
     CustomizePage.strSignQuantity = [NSString stringWithFormat:@"%d",templateQuantity];
    CustomizePage.strSignSizeID = strSelectedSignSizeID;
    CustomizePage.strSignType = strSelectedSignTypeID;
    
    
     [[self navigationController]pushViewController:CustomizePage animated:YES];
//    [CustomizePage loadSignBoardWithUserID:[User sharedManager].userId signTemplateID:strTemplateID signQuantity:[NSString stringWithFormat:@"%d",templateQuantity] printType:[NSString stringWithFormat:@"%d",strPrintSideInfoID]];
    /*
    
    CustomizeSignBoardViewController *customSignBoard =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForCustomSignBoard];
    customSignBoard.productID              = strTemplateID;
    customSignBoard.strPrice               = strPrice ;
    customSignBoard.strSelectedSignTypeID  = strSelectedSignTypeID;
    customSignBoard.strPrintSideInfoID     = strPrintSideInfoID;
    customSignBoard.strSelectedSignSizeID  = strSelectedSignSizeID;
    customSignBoard.strCategoryID          = strCategoryID;
    customSignBoard.quantity               = templateQuantity;
    if (cartID > 0){
        customSignBoard.isFromUserTemplate = true;
        customSignBoard.cartID = cartID;
    }
    [[self navigationController]pushViewController:customSignBoard animated:YES];*/
    
}
-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
    
}
-(void)dealloc{
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
