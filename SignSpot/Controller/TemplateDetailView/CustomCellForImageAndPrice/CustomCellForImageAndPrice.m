//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "CustomCellForImageAndPrice.h"

@implementation CustomCellForImageAndPrice

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
    _starRatingImage.starImage = [UIImage imageNamed:@"RatingStarGrey.png"];
    _starRatingImage.starHighlightedImage = [UIImage imageNamed:@"RatingStarFilled.png"];
    _starRatingImage.maxRating = 5.0;
    _starRatingImage.horizontalMargin = 0;
    _starRatingImage.editable = NO;
    _starRatingImage.displayMode=EDStarRatingDisplayHalf;

    _vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwBackground.layer.borderWidth = 1.f;
    
}


-(IBAction)shareButtonTapped:(id)sender{
    
    [[self delegate]shareButtonTapped];
   
}

-(IBAction)showNextImage:(id)sender{
    
    [[self delegate]showGalleryImageWith:self.imgTemplate withIndicator:self.indicator isNext:YES btnPrev:self.btnPrev btnNext:self.btnNext];
}


-(IBAction)showPrviousImage:(id)sender{
     [[self delegate]showGalleryImageWith:self.imgTemplate withIndicator:self.indicator isNext:NO btnPrev:self.btnPrev btnNext:self.btnNext];
    
}

-(IBAction)expandImage{
    
    [[self delegate]showExpandImage];
}


@end
