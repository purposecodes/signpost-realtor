//
//  CustomCellForType.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"


@protocol PriceDetailsCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Share' Button.
 */

-(void)shareButtonTapped;

/*!
 *This method is invoked when user taps the 'Next' or "Prevous" Button.
 */

-(void)showGalleryImageWith:(UIImageView*)imageView withIndicator:(UIActivityIndicatorView*)indicator isNext:(BOOL)isNext btnPrev:(UIButton*)btnPrev btnNext:(UIButton*)btnNext;


-(IBAction)showExpandImage;


@end


@interface CustomCellForImageAndPrice : UITableViewCell

@property (nonatomic,weak) IBOutlet UIButton *btnFilter;
@property (nonatomic,weak) IBOutlet UIView *vwBackground;
@property (nonatomic,weak) IBOutlet UIImageView *imgTemplate;
@property (nonatomic,weak) IBOutlet UILabel *lblPrice;
@property (nonatomic,weak) IBOutlet UILabel *lblReviewCount;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic,weak) IBOutlet  EDStarRating *starRatingImage;
@property (nonatomic,weak)  id<PriceDetailsCellDelegate>delegate;

@property (nonatomic,weak) IBOutlet UIButton *btnNext;
@property (nonatomic,weak) IBOutlet UIButton *btnPrev;

@end
