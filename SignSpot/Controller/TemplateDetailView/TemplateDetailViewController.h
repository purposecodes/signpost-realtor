//
//  TemplateDetailViewController.h
//  SignSpot
//
//  Created by Purpose Code on 17/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TemplateDetailViewController : UIViewController{
    
     UIPageViewController *pageController;
}

@property (nonatomic,strong) NSArray *arrTemplates;
@property (nonatomic,strong) NSString *strCategoryName;
@property (nonatomic,assign) NSInteger startingIndex;



@end
