//
//  SpecificationFilterViewController.m
//  SignSpot
//
//  Created by Purpose Code on 23/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kHeaderHeight               40
#define kFooterHeight               50
#define kCellCountForMenus          4
#define kCellHeightForMenu          50
#define kCellHeightForValues        60


typedef enum{
    
    eSignType = 3,
    eSignBoardSize = 2,
    ePrintType = 1,
    eQuantity = 0
    
}Menu;

#import "SpecificationFilterViewController.h"
#import "Constants.h"

@interface SpecificationFilterViewController (){
    
    IBOutlet UITableView *tableViewMenus;
    IBOutlet UITableView *tableViewValues;
    NSArray *arrValues;
    NSArray *arrMenus;
    Menu selectedMenu;
    UILabel *lblQuantity;
}

@end

@implementation SpecificationFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self showDefaultSelection];
    [self setUpHeader];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)setUp{
    
    tableViewMenus.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableViewValues.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  //  arrMenus = [[NSArray alloc] initWithObjects:@"Quantity",@"Board Size",@"Sign Type",@"Print Type", nil];
    arrMenus = [[NSArray alloc] initWithObjects:@"Quantity",@"Print Type", nil];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self tableView:tableViewMenus didSelectRowAtIndexPath:indexPath];
    

}

-(void)setUpHeader{
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor getThemeColor];
    vwHeader.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:vwHeader];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[vwHeader]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwHeader)]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:vwHeader
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:Nil
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0
                                                      constant:45.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:vwHeader
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:tableViewMenus
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    UILabel *lblTitle = [UILabel new];
    [vwHeader addSubview:lblTitle];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"CHOOSE SPECIFICATIONS";
    lblTitle.font = [UIFont fontWithName:CommonFont size:14];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:vwHeader
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:vwHeader
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0
                                                           constant:10.0]];
    
}


-(void)showDefaultSelection{
    
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == tableViewMenus) return arrMenus.count;
    else if (selectedMenu == eQuantity) return 1;
    else return arrValues.count;
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //_tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font = [UIFont fontWithName:CommonFont size:13];
    if ([cell viewWithTag:101])[cell viewWithTag:101].hidden = true;
    
   
    if ((_tableView == tableViewMenus) && (indexPath.row < arrMenus.count)){
        
        cell.textLabel.text = arrMenus[[indexPath row]];
        _tableView.layer.borderWidth = 1.f;
        _tableView.layer.borderColor = [UIColor getSeperatorColor].CGColor;
        cell.backgroundColor = [UIColor colorWithRed:53.f/255.f green:44.f/255.f blue:64.f/255.f alpha:1];
        cell.textLabel.textColor = [UIColor blackColor];
        if (indexPath.row == selectedMenu){
            cell.backgroundColor = [UIColor getHeaderOffBlackColor];
            cell.textLabel.textColor = [UIColor whiteColor];
        }else{
            cell.backgroundColor = [UIColor whiteColor];
        }
        
        
    }else{
        
        
        cell.imageView.image = [UIImage imageNamed:@"UnCheckMark.png"];
        NSDictionary *specifications = [arrValues objectAtIndex:indexPath.row];
        NSString *key;
     
        if (selectedMenu == eSignType) {
            key = @"signtemplate_title";
            
            if (_strSelectedSignTypeID) {
                if ([[specifications objectForKey:@"signtype_id"] isEqualToString:_strSelectedSignTypeID]){
                    cell.imageView.image = [UIImage imageNamed:@"CheckMark.png"];
                    _strSignTypeInfo = [specifications objectForKey:key];
                }
                
            }else if ([indexPath row] == 0){
                
                cell.imageView.image = [UIImage imageNamed:@"CheckMark.png"];
                _strSignTypeInfo = [specifications objectForKey:key];
            }
            
            
        }
        if (selectedMenu == eSignBoardSize) {
            
            key = @"signsize_title";
            
            if (_strSelectedSignBoardSizeID) {
                if ([[specifications objectForKey:@"signsize_id"] isEqualToString:_strSelectedSignBoardSizeID]){
                 cell.imageView.image = [UIImage imageNamed:@"CheckMark.png"];
                 _strSignSizeInfo = [specifications objectForKey:key];
                }
            }else if ([indexPath row] == 0){
                
                cell.imageView.image = [UIImage imageNamed:@"CheckMark.png"];
                _strSignSizeInfo = [specifications objectForKey:key];
            }
            
            
        }
        if (selectedMenu == ePrintType) {
            
            key = @"printtype_title";
            
            if ([[specifications objectForKey:@"print_type"] integerValue] == _strSelectedPrintTypeID){
                cell.imageView.image = [UIImage imageNamed:@"CheckMark.png"];
                _strPrintSideInfo = [specifications objectForKey:key];
            }
            
           
        }
        if (selectedMenu == eQuantity) {
            
            if (![cell viewWithTag:101])[self addQuantitySelectorViewWithCell:cell];
            else[[cell viewWithTag:101]setHidden:false];
     
        }
     
        if (specifications && [specifications objectForKey:key])
            cell.textLabel.text = [specifications objectForKey:key];
    }
    
    
   
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == tableViewMenus) {
        
        // Show selected menu options in Menu table
        
        if ([indexPath row] == eSignType) {
            selectedMenu = eSignType;
            arrValues = _signTypes;
        }
        
        if ([indexPath row] == eSignBoardSize) {
            selectedMenu = eSignBoardSize;
            arrValues = _signSize;
        }
        
        if ([indexPath row] == ePrintType) {
             selectedMenu = ePrintType;
             arrValues = _printTypes;
        }
        
        if ([indexPath row] == eQuantity) {
             selectedMenu = eQuantity;
        }
       
    }else if (indexPath.row < arrValues.count){
        
        NSDictionary *specifications = [arrValues objectAtIndex:indexPath.row];
        
        if (selectedMenu == eSignType) {
            _strSelectedSignTypeID = [specifications objectForKey:@"signtype_id"];
            
        }
        if (selectedMenu == eSignBoardSize) {
            
            _strSelectedSignBoardSizeID = [specifications objectForKey:@"signsize_id"];
        }
        
        if (selectedMenu == ePrintType) {
            
            _strSelectedPrintTypeID = [[specifications objectForKey:@"print_type"] integerValue];
            
        }
        if (selectedMenu == eQuantity) {
            
            
        }
        

    }
    
    [tableViewMenus reloadData];
    [tableViewValues reloadData];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == tableViewMenus) return kCellHeightForMenu;
    else return kCellHeightForValues;
}


-(void)addQuantitySelectorViewWithCell:(UITableViewCell*)cell{
    
    UIView *vwQuantuty = [UIView new];
    vwQuantuty.translatesAutoresizingMaskIntoConstraints = NO;
    [cell addSubview:vwQuantuty];
    vwQuantuty.backgroundColor = [UIColor whiteColor];
    vwQuantuty.tag = 101;
    [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[vwQuantuty]-1-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwQuantuty)]];
    [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwQuantuty]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwQuantuty)]];
    
    //PLUS ICON
    
    UIButton* btnIncrement = [UIButton new];
    [vwQuantuty addSubview:btnIncrement];
    btnIncrement.backgroundColor = [UIColor clearColor];
    btnIncrement.translatesAutoresizingMaskIntoConstraints = NO;
    [btnIncrement addTarget:self action:@selector(incrementQuantity) forControlEvents:UIControlEventTouchUpInside];
    [btnIncrement setImage:[UIImage imageNamed:@"PlusIcon.png"] forState:UIControlStateNormal];
    
    [vwQuantuty addConstraint:[NSLayoutConstraint constraintWithItem:btnIncrement
                                                         attribute:NSLayoutAttributeRight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwQuantuty
                                                         attribute:NSLayoutAttributeRight
                                                        multiplier:1.0
                                                          constant:-10.0]];
    
    [vwQuantuty addConstraint:[NSLayoutConstraint constraintWithItem:btnIncrement
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwQuantuty
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
    
    
    [btnIncrement addConstraint:[NSLayoutConstraint constraintWithItem:btnIncrement
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:50]];
    
    [btnIncrement addConstraint:[NSLayoutConstraint constraintWithItem:btnIncrement
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:50]];
    
    //COUNT LABEL
    
    UILabel *lblCount = [UILabel new];
    lblCount.translatesAutoresizingMaskIntoConstraints = NO;
    [vwQuantuty addSubview:lblCount];
    lblCount.text = [NSString stringWithFormat:@"%ld",(long)_quantity];
    [vwQuantuty addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblCount]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblCount)]];
    [vwQuantuty addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[lblCount]-60-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblCount)]];
    lblCount.font = [UIFont fontWithName:CommonFont size:18];
    lblCount.textColor = [UIColor blackColor];
    lblCount.textAlignment = NSTextAlignmentCenter;
    lblQuantity = lblCount;

    // BUTTON DECREMENT
    
    UIButton* btnDecrement = [UIButton new];
    [vwQuantuty addSubview:btnDecrement];
    btnDecrement.backgroundColor = [UIColor clearColor];
    btnDecrement.translatesAutoresizingMaskIntoConstraints = NO;
    [btnDecrement addTarget:self action:@selector(decrementQuantity) forControlEvents:UIControlEventTouchUpInside];
    [btnDecrement setImage:[UIImage imageNamed:@"MinusIcon.png"] forState:UIControlStateNormal];
    
    [vwQuantuty addConstraint:[NSLayoutConstraint constraintWithItem:btnDecrement
                                                           attribute:NSLayoutAttributeLeft
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:vwQuantuty
                                                           attribute:NSLayoutAttributeLeft
                                                          multiplier:1.0
                                                            constant:10.0]];
    
    [vwQuantuty addConstraint:[NSLayoutConstraint constraintWithItem:btnDecrement
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:vwQuantuty
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0
                                                            constant:0.0]];
    
    
    [btnDecrement addConstraint:[NSLayoutConstraint constraintWithItem:btnDecrement
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeWidth
                                                            multiplier:1.0
                                                              constant:50]];
    
    [btnDecrement addConstraint:[NSLayoutConstraint constraintWithItem:btnDecrement
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeHeight
                                                            multiplier:1.0
                                                              constant:50]];
    
   
    
}
-(IBAction)goBack:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(closePopUp)])
        [self.delegate performSelector:@selector(closePopUp)];
    
    
}

-(IBAction)incrementQuantity{
    
    _quantity += 1;
    lblQuantity.text = [NSString stringWithFormat:@"%ld",(long)_quantity];
}

-(IBAction)decrementQuantity{
    
    if (_quantity - 1 > 0) {
        _quantity -= 1;
    }
    
    
    lblQuantity.text = [NSString stringWithFormat:@"%ld",(long)_quantity];
}

-(IBAction)submitApplied:(id)sender{
    
    if (_quantity <= 0 || _strSelectedPrintTypeID <= 0) {
        NSString *strMsg = @"Please choose a print type";
        if ( _quantity <= 0) strMsg = @"Please choose a quantity";
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Specifications"
                                              message:strMsg
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action)
                             {
                                 
                             }];
        
        [alertController addAction:ok];
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        [delegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
        }];

        return;
    }
    
    [[self delegate]getSelectedSignSpecificationsFromFilterWith:_strSelectedSignTypeID SignSizeID:_strSelectedSignBoardSizeID printTypeID:_strSelectedPrintTypeID quantity:_quantity signTypeTitle:_strSignTypeInfo signSizeTitle:_strSignSizeInfo printTypeTitle:_strPrintSideInfo];
   
}



-(void)addSeperatorOnCell:(UITableViewCell*)cell{
    
    if (![cell viewWithTag: 100]) {
        
        UIView *vwSeperator = [UIView new];
        vwSeperator.translatesAutoresizingMaskIntoConstraints = NO;
        [cell addSubview:vwSeperator];
        vwSeperator.tag = 100;
        vwSeperator.backgroundColor = [UIColor getSeperatorColor];
        [cell addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwSeperator]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwSeperator)]];
        
      [vwSeperator addConstraint:[NSLayoutConstraint constraintWithItem:vwSeperator
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.0
                                                          constant:1.0]];
        
        [cell addConstraint:[NSLayoutConstraint constraintWithItem:vwSeperator
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:cell
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:0.0]];
    }

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
