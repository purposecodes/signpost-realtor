//
//  SpecificationFilterViewController.h
//  SignSpot
//
//  Created by Purpose Code on 23/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SignBoardSpecificationsDelegate <NSObject>

@optional
/*!
 *This method is invoked when user Apply "APPLY"
 */

-(void)getSelectedSignSpecificationsFromFilterWith:(NSString*)signTypeID SignSizeID:(NSString*)signSizeID printTypeID:(NSInteger)printTypeID quantity:(NSInteger)quantity signTypeTitle:(NSString*)signTypeTitle signSizeTitle:(NSString*)signSizeTitle printTypeTitle:(NSString*)printTypeTitle;


/*!
 *This method is invoked when user Apply "BACK"
 */

-(void)closePopUp;

@end


@interface SpecificationFilterViewController : UIViewController

@property (nonatomic,strong) NSArray *printTypes;
@property (nonatomic,strong) NSArray *signTypes;
@property (nonatomic,strong) NSArray *signSize;

@property (nonatomic,assign) NSInteger strSelectedPrintTypeID;
@property (nonatomic,strong) NSString *strSelectedSignTypeID;
@property (nonatomic,strong) NSString *strSelectedSignBoardSizeID;
@property (nonatomic,assign) NSInteger quantity;

@property (nonatomic,strong) NSString *strSignSizeInfo;
@property (nonatomic,strong) NSString *strSignTypeInfo;
@property (nonatomic,strong) NSString *strPrintSideInfo;


@property (nonatomic,weak)  id<SignBoardSpecificationsDelegate>delegate;


@end
