//
//  PhotoBrowser.h
//  PurposeColor
//
//  Created by Purpose Code on 08/09/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//



@protocol AddMorePhotosDelegate <NSObject>


/*!
 *This method is invoked when user close the audio player view.
 */

-(void)closePhotoBrowserView;
-(void)showUpdatedImagesWithNewImages:(NSArray*)images;

@end


#import <UIKit/UIKit.h>
#import "CustomeImagePicker.h"

@interface AddMorePhotoViewer : UIView <UICollectionViewDataSource,UICollectionViewDelegate,CustomeImagePickerDelegate>

@property (nonatomic,weak)  id<AddMorePhotosDelegate>delegate;
@property (nonatomic,strong)  NSString *mapID;
@property (nonatomic,strong)  NSString *cartID;

-(void)setUpWithImages:(NSArray*)images andIndex:(NSInteger)index;

@end
