//
//  PhotoBrowser.m
//  PurposeColor
//
//  Created by Purpose Code on 08/09/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "AddMorePhotoViewer.h"
#import "AddMorePhotoCell.h"
#import "PhotoBrowser.h"
#import "Constants.h"
@interface AddMorePhotoViewer() <PhotoBrowserDelegate>{
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UIButton *btnSave;
    NSMutableArray *arrDataSource;
    PhotoBrowser *photoBrowser;
    NSMutableArray *deletedIDs;
}

@end

@implementation AddMorePhotoViewer

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setUpWithImages:(NSArray*)images andIndex:(NSInteger)index{
    
    arrDataSource = [NSMutableArray arrayWithArray:images];
    [collectionView registerNib:[UINib nibWithNibName:@"AddMorePhotoCell" bundle:nil] forCellWithReuseIdentifier:@"AddMorePhotoCell"];
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.alwaysBounceVertical = YES;
    [collectionView reloadData];
    btnSave.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnSave.layer.borderWidth = 1.0f;
    btnSave.layer.cornerRadius = 5.0f;
    btnSave.hidden = true;
    
}



#pragma mark - UICollectionViewDataSource Methods


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
     return  1;
}

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    return  arrDataSource.count ;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AddMorePhotoCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"AddMorePhotoCell" forIndexPath:indexPath];
    cell.btnThumbDelete.hidden = false;
    cell.btnThumbDelete.tag = indexPath.row;
    [cell.btnThumbDelete addTarget:self action:@selector(deleteSelectedImage:) forControlEvents:UIControlEventTouchUpInside];
    cell.indicator.hidden = false;
    if (indexPath.row < arrDataSource.count) {
            [cell.indicator stopAnimating];
            id object = arrDataSource[indexPath.row];
            if ([object isKindOfClass:[NSDictionary class]]) {
                NSDictionary *details = arrDataSource[indexPath.row];
                if ([details objectForKey:@"image_url"]) {
                    NSString *strUrl = [details objectForKey:@"image_url"];
                    [cell.indicator startAnimating];
                    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:strUrl]
                                      placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                 [cell.indicator stopAnimating];
                                                 
                                                 
                                             }];
                }
                
            }else if ([object isKindOfClass:[NSURL class]]){
                NSURL *url = (NSURL*)object;
                [cell.indicator startAnimating];
                [cell.imageView sd_setImageWithURL:url
                                  placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             [cell.indicator stopAnimating];
                                             
                                             
                                         }];
            }
            else{
                cell.imageView.image = arrDataSource[indexPath.row];
            }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self presentGalleryWithImages:arrDataSource andIndex:indexPath.row];
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float width = collectionView.frame.size.width / 3 - 7;
    float height = width;
    return CGSizeMake(width, height);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 100, 0);
}

-(IBAction)closePopUp:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(closePhotoBrowserView)]) {
        [self.delegate closePhotoBrowserView];
    }
    
}

-(IBAction)addMorePhotos{
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    CustomeImagePicker *cip = [[CustomeImagePicker alloc] init];
    cip.delegate = self;
    [cip setHideSkipButton:NO];
    [cip setHideNextButton:NO];
    [cip setMaxPhotos:MAX_ALLOWED_PICK];
    cip.isPhotos = true;
    [delegate.window.rootViewController presentViewController:cip animated:YES completion:^{
    }];

    
}

-(void)imageSelected:(NSArray*)arrayOfGallery isPhoto:(BOOL)isPhoto;
{
    btnSave.hidden = false;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showLoadingScreen];
        }); // Main Queue to Display the Activity View
        __block NSInteger imageCount = 0;
        
        for(NSString *imageURLString in arrayOfGallery)
        {
            // Asset URLs
            ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
            [assetsLibrary assetForURL:[NSURL URLWithString:imageURLString] resultBlock:^(ALAsset *asset) {
                ALAssetRepresentation *representation = [asset defaultRepresentation];
                CGImageRef imageRef = [representation fullScreenImage];
                UIImage *image = [UIImage imageWithCGImage:imageRef];
                if (imageRef) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (!arrDataSource) {
                            arrDataSource = [NSMutableArray new];
                        }
                        if (arrDataSource.count) {
                            [arrDataSource insertObject:image atIndex:0];
                        }else{
                             [arrDataSource addObject:image];
                        }
                    });
                    imageCount ++;
                    
                    if (imageCount == arrayOfGallery.count) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                        [collectionView performSelector:@selector(reloadData) withObject:self afterDelay:0];
                            [self hideLoadingScreen];
                        });
                    }
                }
                
                // Valid Image URL
            } failureBlock:^(NSError *error) {
            }];
        } // All Images I got
        
        
    });
    
    
}

-(IBAction)updateImage:(UIButton*)sender{
    NSMutableArray *imgsToBeUploaded = [NSMutableArray new];
    if (arrDataSource.count) {
        for (id type in arrDataSource) {
            if ([type isKindOfClass:[NSDictionary class]]) {
            }else{
                [imgsToBeUploaded addObject:type];
            }
        }
        
    }
    if (imgsToBeUploaded.count || deletedIDs.count) {
        [self showLoadingScreen];
        NSMutableArray *deltedIDs;
        if (deletedIDs.count) {
             deltedIDs = [NSMutableArray new];
            for (NSDictionary *dict in deletedIDs) {
                [deltedIDs addObject:[dict objectForKey:@"id"]];
            }
        }
        [APIMapper updateSurveyImageWhileReviewWith:_mapID cartID:_cartID userID:[User sharedManager].userId deletedIDs:deltedIDs newImages:imgsToBeUploaded success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([[responseObject objectForKey:@"code"] integerValue] == 200 ) {
                
                if (NULL_TO_NIL([responseObject objectForKey:@"imagearray"])) {
                    if ([self.delegate respondsToSelector:@selector(showUpdatedImagesWithNewImages:)]) {
                        [self.delegate showUpdatedImagesWithNewImages:[responseObject objectForKey:@"imagearray"]];
                    }
                }else{
                    if ([self.delegate respondsToSelector:@selector(showUpdatedImagesWithNewImages:)]) {
                        [self.delegate showUpdatedImagesWithNewImages:nil];
                    }
                }
                
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"Update"
                                                      message:@"Images updated successfully."
                                                      preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         
                                     }];
                
                [alertController addAction:ok];
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                
                [delegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
                }];
                
            }else{
                
                NSString *message = @"Failed to update image.";
                if (NULL_TO_NIL([responseObject objectForKey:@"text"])) {
                    message = [responseObject objectForKey:@"text"];
                }
               
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"Update"
                                                      message:message
                                                      preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         
                                     }];
                
                [alertController addAction:ok];
                AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                
                [delegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
                }];
                
            }
            
            [self hideLoadingScreen];
           
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Update"
                                                  message:@"Failed to update image."
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     
                                 }];
            
            [alertController addAction:ok];
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            
            [delegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
            }];
            [self hideLoadingScreen];
        }];
    }
}

-(IBAction)deleteSelectedImage:(UIButton*)sender{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Delete"
                                          message:@"Do you want to delete this image?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"DELETE"
                         style:UIAlertActionStyleDestructive
                         handler:^(UIAlertAction *action)
                         {
                            btnSave.hidden = false;
                             if (sender.tag < arrDataSource.count) {
                                 if (!deletedIDs) {
                                     deletedIDs = [NSMutableArray new];
                                 }
                                 if ([[arrDataSource objectAtIndex:sender.tag] isKindOfClass:[NSDictionary class]]) {
                                     [deletedIDs addObject:[arrDataSource objectAtIndex:sender.tag]];
                                 }
                                 [arrDataSource removeObjectAtIndex:sender.tag];
                             }
                             [collectionView reloadData];
                            
                             
                         }];
    
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"CANCEL"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action)
                             {
                             }];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    [delegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
    }];
    
}

#pragma mark - Photo Browser & Deleagtes

- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser removeFromSuperview];
        photoBrowser = nil;
    }];
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self animated:YES];
    
}

-(void)dealloc{
    
}

@end
