//
//  TemplateOnMapPopUp.h
//  SignSpot
//
//  Created by Purpose Code on 27/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


@protocol TemplateOnMapDelegate <NSObject>



/*!
 *This method is invoked when user selects an Option from the List, send selected Menu Details sends back to delegate.
 */


-(void)closeMapPopUp;

@end


#import <UIKit/UIKit.h>

@interface TemplateOnMapPopUp : UIView

@property (nonatomic,weak)  id<TemplateOnMapDelegate>delegate;

-(void)setUpMapWithDetails:(NSString*)cartID;

@end
