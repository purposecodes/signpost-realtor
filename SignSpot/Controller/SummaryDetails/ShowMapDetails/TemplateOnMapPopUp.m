//
//  TemplateOnMapPopUp.m
//  SignSpot
//
//  Created by Purpose Code on 27/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "SARMapDrawView.h"
#import <CoreLocation/CoreLocation.h>
#import "TemplateOnMapPopUp.h"
#import "Constants.h"

@import GoogleMaps;

@interface TemplateOnMapPopUp () {
    
   IBOutlet GMSMapView *mapView;
    
}

@end


@implementation TemplateOnMapPopUp

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setUpMapWithDetails:(NSString*)cartID{
    
    [self setUp];
    [self showLoadingScreenWithTitle:@"Loading.."];
    
    [APIMapper loadMapWithCartID:cartID userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self showMapDetailsWith:responseObject];
        [self hideLoadingScreen];
       
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
      [self hideLoadingScreen];
        
    }];
  
}

-(void)setUp{
    
     self.backgroundColor = [UIColor colorWithWhite:0.4 alpha:0.67];
}



-(IBAction)closePopUp:(id)sender{
    
    [[self delegate]closeMapPopUp];
   
}

-(void)showMapDetailsWith:(NSDictionary*)details{
    
    if (NULL_TO_NIL( [details objectForKey:@"mapDetails"]))
        [self drawPolygonFromSavedPath:[[details objectForKey:@"mapDetails"] objectForKey:@"map_path"]];
    
    NSString *address;
    float latitude;
    float longitude;
    
    if ( NULL_TO_NIL([[details objectForKey:@"mapDetails"] objectForKey:@"location_details"]))
        address = [[details objectForKey:@"mapDetails"] objectForKey:@"location_details"];
    
    if ( NULL_TO_NIL([[details objectForKey:@"mapDetails"] objectForKey:@"location_latitude"]))
        latitude = [[[details objectForKey:@"mapDetails"] objectForKey:@"location_latitude"] floatValue];
    
    if ( NULL_TO_NIL([[details objectForKey:@"mapDetails"] objectForKey:@"location_longitude"]))
        longitude = [[[details objectForKey:@"mapDetails"] objectForKey:@"location_longitude"]floatValue];
    
    [self plotAnnotationOnMapWithInfo:address withPoint:CLLocationCoordinate2DMake(latitude, longitude)];
    
}

-(void)drawPolygonFromSavedPath:(NSString*)polygonPath{
    
    [polygonPath stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (polygonPath.length) {
        
        GMSPath *oldpath = [GMSPath pathFromEncodedPath:polygonPath];
        
        GMSPolygon *polygon = [[GMSPolygon alloc] init];
        GMSMutablePath *path = [GMSMutablePath path];
        
        for (int i = 0; i < oldpath.count; i++) {
            CLLocationCoordinate2D loc = [oldpath coordinateAtIndex:i];
            [path addCoordinate:loc];
            
        }
        polygon.path = path;
        polygon.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.2f];
        polygon.strokeColor = [UIColor blueColor];
        polygon.strokeWidth = 4;
        polygon.tappable = YES;
        polygon.map = mapView;

    }
}

-(void)plotAnnotationOnMapWithInfo:(NSString*)information withPoint:(CLLocationCoordinate2D)location{
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.snippet = information;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"MapPointer.png"];
    marker.position = location;
    marker.map = mapView;
    mapView.accessibilityElementsHidden = NO;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.latitude
                                                            longitude:location.longitude
                                                                 zoom:14
                                                              bearing:10
                                                         viewingAngle:10];
    
    mapView.camera = camera;
}


-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:delegate.window animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [MBProgressHUD hideHUDForView:delegate.window animated:YES];
    
}


-(void)dealloc{
    
    
}


@end
