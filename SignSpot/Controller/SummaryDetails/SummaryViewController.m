//
//  SummaryViewController.m
//  SignSpot
//
//  Created by Purpose Code on 09/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eMap = 0,
    eProducts = 1,
    eDeliveryAddress = 2,
    eBillingAddress = 3,
    
    
}eHeaderType;



#define kSectionCount                   4
#define kCellHeightWhenOrderInfo        760
#define kCellHeightForMap               250
#define kCellHeightForProducts          265
#define kCellHeightForAddress           110
#define kCellHeightForDeliveryAddress   100
#define kSuccessCode                    200
#define kMinimumCellCount               1
#define kFooterHeight                   50
#define kHeightForHeader                50


#import "SummaryViewController.h"
#import "ProductCustomCell.h"
#import "Constants.h"
#import "AdderssCustomCell.h"
#import "TemplateOnMapPopUp.h"
#import "OrderDetailsViewController.h"
#import "MapCustomTableViewCell.h"
#import "WebBrowserViewController.h"
#import "DeliveryAdderssCustomCell.h"
#import "AddMorePhotoViewer.h"
#import "PhotoBrowser.h"

@interface SummaryViewController () <ProductCustomCellDelegate,TemplateOnMapDelegate,AddMorePhotosDelegate,PhotoBrowserDelegate>{
    
     IBOutlet UITableView *tableView;
     IBOutlet GMSMapView *mapView;
     IBOutlet UIButton *btnPriceTag;
    
     NSDictionary *_dictProductInfo;
     NSDictionary *dictProducts;
     NSDictionary *dictAddress;
     NSDictionary *dictMapInfo;
     NSArray *arrImages;
    
     BOOL isDataAvailable;
     float grandTotal;
     NSString* checkOutID;
    
    TemplateOnMapPopUp*templateOnMap;
    AddMorePhotoViewer *photoBrowser;
    PhotoBrowser *mapPhotoBrowser;
}

@end

@implementation SummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)setUp{
    
    isDataAvailable = true;
    tableView.allowsSelection = NO;
   
}

-(void)showSummaryDetailsWithInfo:(NSDictionary*)details{
    
    dictProducts = [NSDictionary new];
    dictAddress = [NSDictionary new];
    dictMapInfo = [NSDictionary new];
   _dictProductInfo = [NSDictionary new];
   _dictProductInfo = details;
    [self showProductsAndDetails];
}

-(void)showProductsAndDetails{
    
    if (_dictProductInfo) {
        
        if (NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"map"]))
            dictMapInfo = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"map"];
        
        if (NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"billing"]) )
            dictAddress = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"billing"];
        
        if (NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"cartitems"]))
            dictProducts = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"cartitems"];
        
        if ( NULL_TO_NIL([dictProducts objectForKey:@"grand_total"])){
            grandTotal = [[dictProducts objectForKey:@"grand_total"] floatValue];
        }
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"header"] objectForKey:@"checkoutid" ])){
           checkOutID  = [[_dictProductInfo objectForKey:@"header"] objectForKey:@"checkoutid" ];
        }
        
        if ( NULL_TO_NIL([[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"image"]) )
            arrImages = [[_dictProductInfo objectForKey:@"summary"]  objectForKey:@"image"];
    }
    [tableView reloadData];
    [btnPriceTag setTitle:[NSString stringWithFormat:@"TOTAL $%.02f",grandTotal] forState:UIControlStateNormal];
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return kMinimumCellCount;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Items Available."];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == eMap) return kCellHeightForMap;
    if (indexPath.section == eProducts) return kCellHeightForProducts;
    if (indexPath.section == eDeliveryAddress) return kCellHeightForDeliveryAddress;
    if (indexPath.section == eBillingAddress) return kCellHeightForAddress;
    return kCellHeightForProducts;
    
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    UITableViewCell *cell;
   
    if (indexPath.section == eProducts) {
        static NSString *CellIdentifier = @"ProductCustomCell";
        ProductCustomCell *cell = (ProductCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            NSDictionary *product = dictProducts;
            if ( NULL_TO_NIL([product objectForKey:@"signtemplate_title"]))
                cell.lblTemplateName.text = [product objectForKey:@"signtemplate_title"];
            if ( NULL_TO_NIL([product objectForKey:@"print_type"]))
                cell.lblPrintType.text = [NSString stringWithFormat:@"Print Type : %@",[product objectForKey:@"print_type"]];
            if ( NULL_TO_NIL([product objectForKey:@"size"]))
                cell.lblBoardSize.text = [NSString stringWithFormat:@"Board Size : %@",[product objectForKey:@"size"]];
            if ( NULL_TO_NIL([product objectForKey:@"signtype_title"]))
                cell.lblSignType.text = [NSString stringWithFormat:@"Sign Type : %@",[product objectForKey:@"signtype_title"]];
            if ( NULL_TO_NIL([product objectForKey:@"template_price"]))
                cell.lblPrice.text = [NSString stringWithFormat:@" Price : $%.02f",[[product objectForKey:@"template_price"] floatValue]];
            if ( NULL_TO_NIL([product objectForKey:@"estimated_date"]))
                cell.lblDate.text = [product objectForKey:@"estimated_date"];
            if ( NULL_TO_NIL([product objectForKey:@"sign_qty"]))
                cell.lblQuantity.text =  [NSString stringWithFormat:@"Quantity : %@",[product objectForKey:@"sign_qty"]];
        
            if ( NULL_TO_NIL([product objectForKey:@"sub_total"]))
                cell.lblSubTotal.text = [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"sub_total"] floatValue]];
            if ( NULL_TO_NIL([product objectForKey:@"tax"]))
                cell.lblTax.text =  [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"tax"] floatValue]];
            if ( NULL_TO_NIL([product objectForKey:@"installation_charge"]))
                cell.lblInstallationChrg.text = [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"installation_charge"] floatValue]];;
            if ( NULL_TO_NIL([product objectForKey:@"grand_total"])){
                cell.lblGrandTotal.text = [NSString stringWithFormat:@"$%.02f",[[product objectForKey:@"grand_total"] floatValue]];
                grandTotal = [[product objectForKey:@"grand_total"] floatValue];
            }
        
            [cell.indicator stopAnimating];
            if (NULL_TO_NIL([product objectForKey:@"signtemplate_image"])) {
                [cell.indicator startAnimating];
                NSString *url = [product objectForKey:@"signtemplate_image"];
                if (url.length)
                [cell.templateImage sd_setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    [cell.indicator stopAnimating];}];
            }
        
        cell.tagForBtn = indexPath.row;
        cell.delegate = self;
        return cell;
    }
    if (indexPath.section == eDeliveryAddress) {
        static NSString *CellIdentifier = @"DeliveryAdderssCustomCell";
        DeliveryAdderssCustomCell *cell = (DeliveryAdderssCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if ( NULL_TO_NIL([dictMapInfo objectForKey:@"location_address"]))
            cell.lblAddress.text = [dictMapInfo objectForKey:@"location_address"];
        if ( NULL_TO_NIL([dictMapInfo objectForKey:@"location_title"]))
            cell.lblName.text = [dictMapInfo objectForKey:@"location_title"];
        
        return  cell;
    }
    if (indexPath.section == eMap) {
        static NSString *CellIdentifier = @"MapCustomCell";
        MapCustomTableViewCell *cell = (MapCustomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (dictMapInfo)
            [cell loadMapDetailsWithDetails:dictMapInfo];
        return  cell;
    }
    
    if (indexPath.section == eBillingAddress) {
        static NSString *CellIdentifier = @"AddressCell";
        AdderssCustomCell *cell = (AdderssCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if ( NULL_TO_NIL([dictAddress objectForKey:@"full_address"]))
            cell.lblAddress.text = [dictAddress objectForKey:@"full_address"];
        if ( NULL_TO_NIL([dictAddress objectForKey:@"bill_mobile"]))
            cell.lblPhoneNumber.text = [dictAddress objectForKey:@"bill_mobile"];
        if ( NULL_TO_NIL([dictAddress objectForKey:@"bill_name"]))
            cell.lblName.text = [dictAddress objectForKey:@"bill_name"];
        
        return  cell;
    }

    return cell;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .001;

}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) {
        return nil;
    }
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor clearColor];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    lblTitle.backgroundColor = [UIColor whiteColor];
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.font = [UIFont fontWithName:CommonFontBold size:14];
    lblTitle.textColor = [UIColor blackColor];
    
    if (section == eProducts){
        
            UIButton *btnGallery = [UIButton buttonWithType: UIButtonTypeCustom];
            [btnGallery addTarget: self action: @selector(showGallery) forControlEvents: UIControlEventTouchUpInside];
            btnGallery.translatesAutoresizingMaskIntoConstraints = NO;
            [vwHeader addSubview:btnGallery];
            [btnGallery setImage:[UIImage imageNamed:@"Gallery_Image.png"] forState:UIControlStateNormal];
            [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[btnGallery]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnGallery)]];
            
            [btnGallery addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                                   attribute:NSLayoutAttributeWidth
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute:NSLayoutAttributeWidth
                                                                  multiplier:1.0
                                                                    constant:60]];
            
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:btnGallery
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:-5]];
            
            UILabel *lblCount = [UILabel new];
            lblCount.translatesAutoresizingMaskIntoConstraints = NO;
            [vwHeader addSubview:lblCount];
            lblCount.font = [UIFont fontWithName:CommonFontBold size:14];
            lblCount.textColor = [UIColor whiteColor];
            lblCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrImages.count];
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblCount
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant: 0]];
            
            [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblCount
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:vwHeader
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0
                                                                  constant:-15]];
    }
    
    if (section == eProducts)lblTitle.text = @"  PRODUCT";
    if (section == eDeliveryAddress)lblTitle.text = @"  DELIVERY ADDRESS";
    if (section == eMap)lblTitle.text = @"  MAP";
    if (section == eBillingAddress)lblTitle.text = @"  BILLING ADDRESS";
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}

#pragma mark - UITableview custom cell delegate Methods

-(IBAction)showExpandedMap:(id)sender{
    
    if ( NULL_TO_NIL([dictMapInfo objectForKey:@"map_image"])) {
        NSString *url = [dictMapInfo objectForKey:@"map_image"];
        NSMutableArray *arrMapImage = [[NSMutableArray alloc] initWithObjects:[NSURL URLWithString:url], nil];
       // [self presentGalleryWithImages:arrMapImage andIndex:0];
        
        if (!mapPhotoBrowser) {
            mapPhotoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"PhotoBrowser" owner:self options:nil] objectAtIndex:0];
            mapPhotoBrowser.delegate = self;
        }
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        UIView *vwPopUP = mapPhotoBrowser;
        [app.window.rootViewController.view addSubview:vwPopUP];
        vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
        [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
        [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
        
        vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            vwPopUP.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
            [mapPhotoBrowser setUpWithImages:arrMapImage andIndex:0];
        }];

        
        
    }
   
}


-(void)showMapOnSelectedCell:(NSInteger)tag{


    if (!templateOnMap) {
        
        templateOnMap = [[[NSBundle mainBundle] loadNibNamed:@"TemplateOnMapPopUp" owner:self options:nil] objectAtIndex:0];
        templateOnMap.delegate = self;
        [self.view addSubview:templateOnMap];
        templateOnMap.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[templateOnMap]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(templateOnMap)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[templateOnMap]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(templateOnMap)]];
        
        templateOnMap.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            templateOnMap.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
        NSDictionary *mapInfo = dictProducts;
        NSString *cartID = [mapInfo objectForKey:@"cart_id"];
        [templateOnMap setUpMapWithDetails:cartID];
    
    
    
}

-(void)closeMapPopUp{
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        templateOnMap.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [templateOnMap removeFromSuperview];
        templateOnMap = nil;
        
    }];

}




-(IBAction)buyBtnClicked{
    
    NSString *billID;
    if (NULL_TO_NIL([dictAddress objectForKey:@"bill_id"]))
        billID = [dictAddress objectForKey:@"bill_id"];
    NSString *cartID;
    if (NULL_TO_NIL([dictProducts objectForKey:@"cart_id"]))
        cartID = [dictProducts objectForKey:@"cart_id"];
    NSString *date;
    if (NULL_TO_NIL([dictProducts objectForKey:@"estimated_date"]))
        date = [dictProducts objectForKey:@"estimated_date"];
    
    if ((date.length && cartID.length && billID.length) > 0) {
        
        WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
        browser.strTitle = @"PAYMENT";
        browser.isPayment = true;
//       browser.strURL =[NSString stringWithFormat:@"%@admin/paypal_operations.php?paypal=checkout&user_id=%@&cart_id=%@&checkoutid=%@&device=mobile",ExternalWebPageURL,[User sharedManager].userId,cartID,checkOutID];
    //    browser.strURL =[NSString stringWithFormat:@"%@pay_usingcard.php?user_id=%@&cart_id=%@&checkoutid=%@&device=mobile&bill_id=%@&shipping_id=%@",ExternalWebPageURL,[User sharedManager].userId,cartID,checkOutID,billID,[User sharedManager].shippingID];
    browser.strURL =[NSString stringWithFormat:@"%@payment_gateway_all.php?user_id=%@&cart_id=%@&checkoutid=%@&device=mobile&bill_id=%@&shipping_id=%@",ExternalWebPageURL,[User sharedManager].userId,cartID,checkOutID,billID,[User sharedManager].shippingID];

        [[self navigationController]pushViewController:browser animated:YES];
        
        
    }
 
    
}


-(void)showPageAfterPurchaseWith:(NSDictionary*)details{
    
    OrderDetailsViewController *orderDetails =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForOrderDetails];
     orderDetails.showBackBtn = false;
    [[self navigationController]pushViewController:orderDetails animated:YES];
    [orderDetails showOrderDetailsWithInfo:details];
}


-(void)showAlertWithTitle:(NSString*)title message:(NSString*)message{
    
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    
}

#pragma mark - Photo Browser & Deleagtes

-(IBAction)showGallery{
    
    [self presentGalleryWithImages:arrImages andIndex:0];
}



- (void)presentGalleryWithImages:(NSArray*)images andIndex:(NSInteger)index
{
    
    if (!photoBrowser) {
        photoBrowser = [[[NSBundle mainBundle] loadNibNamed:@"AddMorePhotoViewer" owner:self options:nil] objectAtIndex:0];
        photoBrowser.delegate = self;
    }
    if ([[[_dictProductInfo objectForKey:@"summary"] objectForKey:@"map"] objectForKey:@"map_id"]){
        photoBrowser.mapID = [[[_dictProductInfo objectForKey:@"summary"] objectForKey:@"map"] objectForKey:@"map_id"];
    }
    if ([[[_dictProductInfo objectForKey:@"summary"] objectForKey:@"cartitems"] objectForKey:@"cart_id"]) {
        photoBrowser.cartID = [[[_dictProductInfo objectForKey:@"summary"] objectForKey:@"cartitems"] objectForKey:@"cart_id"];
    }
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *vwPopUP = photoBrowser;
    [app.window.rootViewController.view addSubview:vwPopUP];
    vwPopUP.translatesAutoresizingMaskIntoConstraints = NO;
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    [app.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[vwPopUP]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(vwPopUP)]];
    
    vwPopUP.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        vwPopUP.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [photoBrowser setUpWithImages:images andIndex:index];
    }];
    
}

-(void)closePhotoBrowserView{
    
    if (mapPhotoBrowser) {
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            mapPhotoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
            [mapPhotoBrowser removeFromSuperview];
            mapPhotoBrowser = nil;
        }];
        
        
    }else{
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            photoBrowser.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
            [photoBrowser removeFromSuperview];
            photoBrowser = nil;
        }];
        
    }
    
}

-(void)showUpdatedImagesWithNewImages:(NSArray*)images{
    
    [self closePhotoBrowserView];
    arrImages = nil;
    if (images) {
        arrImages = [NSMutableArray arrayWithArray:images];
    }
    
    [tableView reloadData];
    
}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}


-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
