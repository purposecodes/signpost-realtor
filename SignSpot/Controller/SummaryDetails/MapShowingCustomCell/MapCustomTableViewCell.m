//
//  MapCustomTableViewCell.m
//  SignSpot
//
//  Created by Purpose Code on 30/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "MapCustomTableViewCell.h"
#import "Constants.h"

@implementation MapCustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




-(void)loadMapDetailsWithDetails:(NSDictionary*)mapInfo{
    
    if ( NULL_TO_NIL([mapInfo objectForKey:@"map_image"])) {
        [indicator startAnimating];
        NSString *url = [mapInfo objectForKey:@"map_image"];
        if (url.length)
            [imgMapView sd_setImageWithURL:[NSURL URLWithString:url]
                                  placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             [indicator stopAnimating];}];
    }
    
}




@end
