//
//  MapCustomTableViewCell.h
//  SignSpot
//
//  Created by Purpose Code on 30/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface MapCustomTableViewCell : UITableViewCell{
    
   IBOutlet UIImageView *imgMapView;
    IBOutlet UIActivityIndicatorView *indicator;
    
}

@property(nonatomic,weak) IBOutlet GMSMapView *mapView;

-(void)loadMapDetailsWithDetails:(NSDictionary*)mapInfo;

@end
