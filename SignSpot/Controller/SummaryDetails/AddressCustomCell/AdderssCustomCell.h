//
//  AdderssCustomCell.h
//  SignSpot
//
//  Created by Purpose Code on 27/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdderssCustomCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblPhoneNumber;
@property (nonatomic,strong) IBOutlet UILabel *lblAddress;

@end
