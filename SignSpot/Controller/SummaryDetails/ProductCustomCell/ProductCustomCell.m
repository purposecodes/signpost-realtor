//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "ProductCustomCell.h"

@implementation ProductCustomCell

- (void)awakeFromNib {
    // Initialization code
    [self setUp];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
    
    _vwTemplateBg.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwTemplateBg.layer.borderWidth = 1.f;

}
-(IBAction)mapIconSelected:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(showMapOnSelectedCell:)])
         [[self delegate] showMapOnSelectedCell:self.tagForBtn];
   
}

-(IBAction)datePickerSelected:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(showDatePickerWith:)])
        [[self delegate] showDatePickerWith:self.tagForBtn];
    
}






@end
