//
//  CustomCellForType.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SARMapDrawView.h"

@protocol ProductCustomCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Map' Button.
 */

-(void)showMapOnSelectedCell:(NSInteger)tag;

/*!
 *This method is invoked when user taps the 'Date' Button.
 */

-(void)showDatePickerWith:(NSInteger)tag;




@end


@interface ProductCustomCell : UITableViewCell{
   
   

}


@property (nonatomic,weak) IBOutlet UIView *vwTemplateBg;
@property (nonatomic,weak) IBOutlet UIImageView *templateImage;
@property (nonatomic,weak) IBOutlet UILabel *lblTemplateName;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic,weak) IBOutlet UILabel *lblSignType;
@property (nonatomic,weak) IBOutlet UILabel *lblPrintType;
@property (nonatomic,weak) IBOutlet UILabel *lblBoardSize;
@property (nonatomic,weak) IBOutlet UILabel *lblPrice;
@property (nonatomic,weak) IBOutlet UILabel *lblDate;
@property (nonatomic,weak) IBOutlet UILabel *lblQuantity;
@property (nonatomic,weak) IBOutlet UILabel *lblEstimatedDaetHeading;

@property (nonatomic,assign) NSInteger tagForBtn;

@property (nonatomic,weak) IBOutlet UILabel *lblSubTotal;
@property (nonatomic,weak) IBOutlet UILabel *lblTax;
@property (nonatomic,weak) IBOutlet UILabel *lblInstallationChrg;
@property (nonatomic,weak) IBOutlet UILabel *lblGrandTotal;


@property (nonatomic,weak)  id<ProductCustomCellDelegate>delegate;


@end
