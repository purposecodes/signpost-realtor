//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#define kTagForTitle        1
#define kCellHeight         50
#define kHeightForHeader    170

NSString *const HomeMenu        = @"HOME";
NSString *const UserTemplate    = @"USER TEMPLATE";

#import "MenuViewController.h"
#import "HomeViewController.h"
#import "Constants.h"

@implementation SWUITableViewCell
@end

@interface MenuViewController ()

@end

@implementation MenuViewController{
    
    NSMutableArray *arrCategories;
}


- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // configure the destination view controller:
    if ( [sender isKindOfClass:[UITableViewCell class]] ){
    }
}
-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self setUp];
    [self loadAllCategories];
}

-(void)setUp{
    
    arrCategories = [NSMutableArray new];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
}

-(void)loadAllCategories{
    
    [arrCategories addObject:@"HOME"];
    [arrCategories addObject:@"ORDERS"];
    [arrCategories addObject:@"PROFILE"];
    [arrCategories addObject:@"NOTIFICATIONS"];
    //[arrCategories addObject:@"MY TEMPLATES"];
    [arrCategories addObject:@"HELP"];
    [arrCategories addObject:@"PRIVACY POLICY"];
    [arrCategories addObject:@"TERMS OF SERVICE"];
    [_tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCategories.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSString *title;
    if (indexPath.row < arrCategories.count) {
        title = arrCategories [indexPath.row];
    }
    if ([[cell contentView]viewWithTag:kTagForTitle]) {
        UILabel *lblTitle = (UILabel*)[[cell contentView]viewWithTag:kTagForTitle];
        lblTitle.text = title;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kCellHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIFont *font = [UIFont fontWithName:CommonFont size:15];
    
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor getThemeColor];
    //User Profile Pic
    
    UIImageView *imgDisplay = [UIImageView new];
    [vwHeader addSubview:imgDisplay];
    imgDisplay.translatesAutoresizingMaskIntoConstraints = NO;
    imgDisplay.clipsToBounds = YES;
    imgDisplay.layer.cornerRadius = 40.f;
    imgDisplay.layer.borderWidth = 3.f;
    imgDisplay.backgroundColor = [UIColor whiteColor];
    imgDisplay.layer.borderColor = [UIColor colorWithRed:0.13 green:0.40 blue:0.84 alpha:1.0].CGColor;
    imgDisplay.contentMode = UIViewContentModeScaleAspectFit;
    [imgDisplay addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeWidth
                                                          multiplier:1.0
                                                            constant:80.0]];
    [imgDisplay addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeHeight
                                                          multiplier:1.0
                                                            constant:80.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:vwHeader
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:-20.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:imgDisplay
                                                           attribute:NSLayoutAttributeCenterX
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:vwHeader
                                                           attribute:NSLayoutAttributeCenterX
                                                          multiplier:1.0
                                                            constant:-30.0]];
    
    if ([User sharedManager].profileurl.length) {
        
        [imgDisplay sd_setImageWithURL:[NSURL URLWithString:[User sharedManager].profileurl]
                      placeholderImage:[UIImage imageNamed:@"UserProfilePic.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 
                             }];
    }

    /*! User Name !*/
  
    UILabel *lblName = [UILabel new];
    lblName.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblName];
    lblName.numberOfLines = 1;
    lblName.textColor = [UIColor whiteColor];;
    lblName.font = [UIFont fontWithName:CommonFontBold size:14];;
    lblName.text = [User sharedManager].name;
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblName
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:imgDisplay
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:10.0]];
    
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblName
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:imgDisplay
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];

    
    
    /*! User Other Details !*/
    
    UILabel *lblOtherInfo = [UILabel new];
    lblOtherInfo.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblOtherInfo];
    lblOtherInfo.numberOfLines = 1;
    lblOtherInfo.font = font;
    lblOtherInfo.textColor = [UIColor whiteColor];;
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblOtherInfo
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:lblName
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0
                                                          constant:10.0]];
    [vwHeader addConstraint:[NSLayoutConstraint constraintWithItem:lblOtherInfo
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:imgDisplay
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
    lblOtherInfo.text = [User sharedManager].email;
   
    return vwHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return kHeightForHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < arrCategories.count) {
        
        /**! Get RootVC from the navigation controller.Since SWReavealcontroller is initialised by two NAV Controller on Front and Rear.!**/
        
        UINavigationController *navController = (UINavigationController*)self.revealViewController.frontViewController;
        NSArray *viewControllers = navController.viewControllers;
        if (viewControllers.count) {
            HomeViewController *homeVC = viewControllers[0];
            NSString *title = arrCategories[indexPath.row];
           [homeVC showSelectedCategoryDetailsFromMenuList:title];
        }
    }
    [self.revealViewController revealToggleAnimated:YES];
  
}

-(IBAction)logout:(id)sender{
    
    UINavigationController *navController = (UINavigationController*)self.revealViewController.frontViewController;
    NSArray *viewControllers = navController.viewControllers;
    if (viewControllers.count) {
        HomeViewController *homeVC = viewControllers[0];
        [homeVC showSelectedCategoryDetailsFromMenuList:@"LOGOUT"];
    }
    [self.revealViewController revealToggleAnimated:YES];
}

-(IBAction)closeSlider:(id)sender{
    [self.revealViewController revealToggleAnimated:YES];
}


#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO call whatever function you need to visually restore
}

@end
