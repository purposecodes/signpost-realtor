//
//  NotificationsListingViewController.m
//  SignSpot
//
//  Created by Purpose Code on 09/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define kSectionCount               1
#define kDefaultCellHeight          95
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kWidthPadding               115
#define kHeightPadding              35

#import "NotificationsListingViewController.h"
#import "NotificationListingCell.h"
#import "Constants.h"

@interface NotificationsListingViewController (){
    
    IBOutlet UITableView *tableView;
    BOOL isDataAvailable;
    NSInteger totalPages;
    NSInteger currentPage;
    BOOL isPageRefresing;
    NSMutableArray *arrNotifications;
}

@end

@implementation NotificationsListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self loadAllMyNotificationsWithPageNo:currentPage isByPagination:NO];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    currentPage = 1;
    totalPages = 0;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.alwaysBounceVertical = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    arrNotifications = [NSMutableArray new];
    isDataAvailable = false;
    tableView.allowsSelection = NO;
    [tableView setHidden:true];
    
    

}

-(void)loadAllMyNotificationsWithPageNo:(NSInteger)pageNo isByPagination:(BOOL)isPagination{
    
    if (!isPagination) {
        [self showLoadingScreen];
    }
    
    [APIMapper getAllMyNotificationsWith:[User sharedManager].userId pageNumber:pageNo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getTemplatesFromResponds:responseObject];
        [self resetNotificationCount];
        [self hideLoadingScreen];

        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isPageRefresing = false;
        [self hideLoadingScreen];
        [tableView reloadData];
        [tableView setHidden:false];
    }];
        
      
}

-(void)resetNotificationCount{
    
    [User sharedManager].cartCount = 0;
    [Utility saveUserObject:[User sharedManager] key:@"USER"];
    
    
}
-(void)getTemplatesFromResponds:(NSDictionary*)responseObject{
    
    isDataAvailable = false;
    isPageRefresing = false;
    
    if (NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"notificationdata"])){
        NSArray *notifications = [[responseObject objectForKey:@"header"] objectForKey:@"notificationdata"];
        [arrNotifications addObjectsFromArray:notifications];
    }
    
    if (NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"pageCount"]))
        totalPages =  [[[responseObject objectForKey:@"header"] objectForKey:@"pageCount"]integerValue];
    
    if (NULL_TO_NIL([[responseObject objectForKey:@"header"] objectForKey:@"currentPage"]))
        currentPage =  [[[responseObject objectForKey:@"header"] objectForKey:@"currentPage"]integerValue];
    
    if (arrNotifications.count)isDataAvailable = true;
    [tableView setHidden:false];
    [tableView reloadData];
    
    
}



#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    
    return arrNotifications.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Notifications Available."];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) return kDefaultCellHeight;
    CGFloat height = [self getDynamicHeightForDecsriptionWith:indexPath.row];
    return height;
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    
    if (!isDataAvailable) {
        
        /*****! No listing found , default cell !**************/
        
        UITableViewCell *cell = [Utility getNoDataCustomCellWith:tableView withTitle:@"No Listing found"];
        return cell;
        
    }
    
    static NSString *CellIdentifier = @"NotificationListingCell";
    NotificationListingCell *cell = (NotificationListingCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.vwBackground.backgroundColor = [UIColor whiteColor];
    cell.vwBackground.layer.borderWidth = 1.f;
    cell.vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    cell.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row < arrNotifications.count) {
        
        NSDictionary *productInfo = arrNotifications[[indexPath row]];
        if (productInfo && [productInfo objectForKey:@"notify_msg"])
            cell.lblDescription.text = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"notify_msg"]];
        
        if (productInfo && [productInfo objectForKey:@"notify_date"])
            cell.lblDate.text = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"notify_date"]];
        
        NSString *urlImage;
        if (productInfo && [productInfo objectForKey:@"notify_image"])
            urlImage = [NSString stringWithFormat:@"%@",[productInfo objectForKey:@"notify_image"]];
        
        [cell.indicator stopAnimating];
        if (urlImage.length) {
            [cell.indicator startAnimating];
            [cell.imgTemplate sd_setImageWithURL:[NSURL URLWithString:urlImage]
                                placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                           [cell.indicator stopAnimating];
                                        
                                       }];
        }
        
    }
    
    
    return cell;
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    
    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self loadAllMyNotificationsWithPageNo:nextPage isByPagination:YES];
            }
            
        }
    }
    
}




-(float)getDynamicHeightForDecsriptionWith:(NSInteger)index{
    if (index < arrNotifications.count) {
        NSString *message;
        CGSize size;
        NSDictionary *productInfo = arrNotifications[index];
        if (productInfo && [productInfo objectForKey:@"notify_msg"])
           message =  [productInfo objectForKey:@"notify_msg"];
        if (message.length) {
             size = [message boundingRectWithSize:CGSizeMake(tableView.frame.size.width - kWidthPadding, MAXFLOAT)
                                                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                                                    attributes:@{
                                                                                                 NSFontAttributeName : [UIFont fontWithName:CommonFont size:14]
                                                                                                 }
                                                                                       context:nil].size;
            if (size.height + kHeightPadding < kDefaultCellHeight)return kDefaultCellHeight;
            
            return size.height + kHeightPadding;

        }

    }
    
    
    
    return kDefaultCellHeight; ;
}

-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
