//
//  CustomCellForType.m
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "MyOrdersCustomCell.h"

@implementation MyOrdersCustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUp{
    
    _vwOrderBg.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwOrderBg.layer.borderWidth = 1.f;
   

}


-(IBAction)cancelOrder:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(cancelOrderWithIndex:)])
        [self.delegate cancelOrderWithIndex:self.tagForCell];

}

-(IBAction)removeSignBoard:(id)sender{
    
    if ([self.delegate respondsToSelector:@selector(removeSignBoardWithTag:)])
        [self.delegate removeSignBoardWithTag:self.tagForCell];
    
}




@end
