//
//  CustomCellForType.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyOrdersCustomCellDelegate <NSObject>

@optional

/*!
 *This method is invoked when user taps the 'Share' Button.
 */

-(void)shareButtonTappedWithTag:(NSInteger)tag;

/*!
 *This method is invoked when user taps the 'Delete' Button.
 */


-(void)deleteSelectedCellWithTag:(NSInteger)tag;


/*!
 *This method is invoked when user taps the 'CANCEL ORDER' Button.
 */

-(void)removeSignBoardWithTag:(NSInteger)index;

/*!
 *This method is invoked when user taps the 'REMOVE SIGN BOARD' Button.
 */

-(void)cancelOrderWithIndex:(NSInteger)index;



@end


@interface MyOrdersCustomCell : UITableViewCell{
    
   

}

@property (nonatomic,assign) BOOL isSummary;



@property (nonatomic,assign) NSInteger tagForCell;


/*! Order Details !*/

@property (nonatomic,weak) IBOutlet UIView  *vwOrderBg;
@property (nonatomic,weak) IBOutlet UILabel *lblOrderNo;
@property (nonatomic,weak) IBOutlet UILabel *lblOrderQuantity;
@property (nonatomic,weak) IBOutlet UILabel *lblOrderDate;
@property (nonatomic,weak) IBOutlet UILabel *lblOrderDueOrInstalledDate;
@property (nonatomic,weak) IBOutlet UILabel *lblOrderDueOrInstalledDateHeading;
@property (nonatomic,weak) IBOutlet UILabel *lblGrandTotalOrStatus;
@property (nonatomic,weak) IBOutlet UILabel *lblTotalOrStatusHeading;
@property (nonatomic,weak) IBOutlet UILabel *lblRemoveSignPrice;
@property (nonatomic,weak) IBOutlet UIView *vwRemoveSign;
@property (nonatomic,weak) IBOutlet UIView *vwCancelOrder;



@property (nonatomic,weak)  id<MyOrdersCustomCellDelegate>delegate;
-(void)setUp;


@end
