//
//  MyOrderListViewController.m
//  SignSpot
//
//  Created by Purpose Code on 10/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//


typedef enum{
    
    eTypePending    = 0,
    eTypeCancelled  = 2,
    eTypeCompleted  = 1
    
    
}eOrderType;



#define kSectionCount               1
#define kSuccessCode                200
#define kMinimumCellCount           1
#define kHeaderHeight               40
#define kCellHeight                 140
#define kEmptyHeaderAndFooter       0
#define kCellHeightForCompleted     190



#import "MyOrderListViewController.h"
#import "MyOrdersCustomCell.h"
#import "Constants.h"
#import "OrderDetailsViewController.h"
#import "WebBrowserViewController.h"

@interface MyOrderListViewController () <MyOrdersCustomCellDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UISegmentedControl *segmentControll;
    BOOL isDataAvailable;
    
    NSMutableArray *arrPendingOrders;
    NSMutableArray *arrInstalledOrders;
    NSMutableArray *arrCancelledOrders;
    NSMutableArray *arrDataSource;
    

}

@end

@implementation MyOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self loadAllMyOrders];
    NSIndexPath *indexPath = tableView.indexPathForSelectedRow;
    if (indexPath) {
        [tableView deselectRowAtIndexPath:indexPath animated:animated];
    }
}

-(void)setUp{
    
    arrPendingOrders    = [NSMutableArray new];
    arrInstalledOrders  = [NSMutableArray new];
    arrCancelledOrders  = [NSMutableArray new];
    arrDataSource       = [NSMutableArray new];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    segmentControll.tintColor = [UIColor getHeaderOffBlackColor];
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:CommonFont size:13], NSFontAttributeName,
                                 [UIColor blackColor], NSForegroundColorAttributeName, nil];
    NSDictionary *attributes2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:CommonFont size:13], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [segmentControll setTitleTextAttributes:attributes1 forState:UIControlStateNormal];
    [segmentControll setTitleTextAttributes:attributes2 forState:UIControlStateSelected];
    isDataAvailable = false;
    tableView.hidden = true;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

-(void)loadAllMyOrders{
    
    [arrPendingOrders removeAllObjects];
    [arrInstalledOrders removeAllObjects];
    [arrCancelledOrders removeAllObjects];
    [arrDataSource removeAllObjects];
    
    [self showLoadingScreen];
    [APIMapper getAllOrdersWithUserID:[User sharedManager].userId PageNumber:1 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getTemplatesFromResponds:responseObject];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [tableView setHidden:false];
          [self hideLoadingScreen];
    }];
    
}

-(void)getTemplatesFromResponds:(NSDictionary*)responseObject{
    
    if ( NULL_TO_NIL([[responseObject objectForKey:@"resultarray"] objectForKey:@"completed"])) {
        NSArray *installed = [[responseObject objectForKey:@"resultarray"] objectForKey:@"completed"];
        for (NSDictionary *dict in installed)
            [arrInstalledOrders addObject:dict];
    }
    if ( NULL_TO_NIL([[responseObject objectForKey:@"resultarray"] objectForKey:@"pending"])) {
        NSArray *pending = [[responseObject objectForKey:@"resultarray"] objectForKey:@"pending"];
        for (NSDictionary *dict in pending)
            [arrPendingOrders addObject:dict];
        
    }
    if ( NULL_TO_NIL([[responseObject objectForKey:@"resultarray"] objectForKey:@"cancel"])) {
        NSArray *pending = [[responseObject objectForKey:@"resultarray"] objectForKey:@"cancel"];
        for (NSDictionary *dict in pending)
            [arrCancelledOrders addObject:dict];
        
    }
    
    // Default Pending Orders
    
    [tableView setHidden:false];
    [self segementSelected:nil];
    
    
}


-(IBAction)segementSelected:(id)sender{
    
    isDataAvailable = false;
    
    if ([segmentControll selectedSegmentIndex] == eTypeCompleted)
        arrDataSource = arrInstalledOrders;
    
    else if ([segmentControll selectedSegmentIndex] == eTypePending)
        arrDataSource = arrPendingOrders;
    
    else if ([segmentControll selectedSegmentIndex] == eTypeCancelled)
         arrDataSource = arrCancelledOrders;
    
    if (arrDataSource.count > 0)isDataAvailable = true;

    [tableView reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return kSectionCount;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kMinimumCellCount;
    return arrDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!isDataAvailable) {
        cell = [Utility getNoDataCustomCellWith:aTableView withTitle:@"No Orders Available."];
        return cell;
    }
    cell = [self configureCellForIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable)  return kCellHeight;
    if ([segmentControll selectedSegmentIndex] == eTypeCompleted)return kCellHeightForCompleted;
    if ([segmentControll selectedSegmentIndex] == eTypePending)return kCellHeightForCompleted;
    return kCellHeight;
}


-(UITableViewCell*)configureCellForIndexPath:(NSIndexPath*)indexPath{
    
    static NSString *CellIdentifier = @"MyOrdersCustomCell";
    MyOrdersCustomCell *cell = (MyOrdersCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.delegate = self;
    cell.tagForCell = indexPath.row;
    cell.lblTotalOrStatusHeading.text = @"Grand Total : ";
    [cell.vwRemoveSign setHidden:true];
    [cell.vwCancelOrder setHidden:true];
    if ([segmentControll selectedSegmentIndex] == eTypeCompleted){
        [cell.vwRemoveSign setHidden:false];
    }
    if ([segmentControll selectedSegmentIndex] == eTypePending){
        [cell.vwCancelOrder setHidden:false];
    }
    
    if (indexPath.row < arrDataSource.count) {
        
        NSDictionary *details = arrDataSource[indexPath.row];
        if (NULL_TO_NIL([details objectForKey:@"order_date"]))
            cell.lblOrderDate.text = [details objectForKey:@"order_date"];
        
        if (NULL_TO_NIL([details objectForKey:@"order_no"]))
            cell.lblOrderNo.text = [details objectForKey:@"order_no"];
        
        if (NULL_TO_NIL([details objectForKey:@"totalqty"]))
            cell.lblOrderQuantity.text = [details objectForKey:@"totalqty"];
        
        if (NULL_TO_NIL([details objectForKey:@"installed_date"])){
            cell.lblOrderDueOrInstalledDate.text = [details objectForKey:@"installed_date"];
            cell.lblOrderDueOrInstalledDateHeading.text = @"Installed Date :";
            
        }
        else if (NULL_TO_NIL([details objectForKey:@"order_canceldate"])){
            cell.lblOrderDueOrInstalledDate.text = [details objectForKey:@"order_canceldate"];
            cell.lblOrderDueOrInstalledDateHeading.text = @"Cancelled Date :";
            
        }else{
            if (NULL_TO_NIL([details objectForKey:@"order_duedate"])){
                cell.lblOrderDueOrInstalledDate.text = [details objectForKey:@"order_duedate"];
                cell.lblOrderDueOrInstalledDateHeading.text = @"Order Due Date :";
            }
        }
     
        if (NULL_TO_NIL([details objectForKey:@"totalprice"]))
            cell.lblGrandTotalOrStatus.text = [NSString stringWithFormat:@"$%.02f",[[details objectForKey:@"totalprice"] floatValue]];
        
        if (NULL_TO_NIL([details objectForKey:@"removalfee"])){
            cell.lblRemoveSignPrice.text = [NSString stringWithFormat:@" For $%.02f",[[details objectForKey:@"removalfee"] floatValue]];
            NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:  cell.lblRemoveSignPrice.attributedText];
            
            [text addAttribute:NSForegroundColorAttributeName
                         value:[UIColor redColor]
                         range:NSMakeRange(5, text.length - 5)];
            [cell.lblRemoveSignPrice setAttributedText: text];
            
        }
        
            
    }
    
    [cell setUp];
    
    return cell;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return nil;
    UIView *vwHeader = [UIView new];
    vwHeader.backgroundColor = [UIColor whiteColor];
    vwHeader.layer.borderWidth = 1.f;
    vwHeader.layer.borderColor = [UIColor clearColor].CGColor;
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [vwHeader addSubview:lblTitle];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lblTitle]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    [vwHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblTitle]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    lblTitle.font = [UIFont fontWithName:CommonFont size:14];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    
    NSString *strTitle;
    if ([segmentControll selectedSegmentIndex] == eTypeCompleted)
        strTitle = [NSString stringWithFormat:@"%lu Orders Completed",(unsigned long)arrDataSource.count];
    
    else if ([segmentControll selectedSegmentIndex] == eTypePending)
        strTitle = [NSString stringWithFormat:@"%lu Orders Pending",(unsigned long)arrDataSource.count];
    
    else if ([segmentControll selectedSegmentIndex] == eTypeCancelled)
        strTitle = [NSString stringWithFormat:@"%lu Orders Cancelled",(unsigned long)arrDataSource.count];
    
    
    lblTitle.text = strTitle;
    
    return vwHeader;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return kEmptyHeaderAndFooter;
    return kHeaderHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row < arrDataSource.count) {
           NSDictionary *details = arrDataSource[indexPath.row];
        if ( NULL_TO_NIL([details objectForKey:@"order_id"])) {
            NSString *orderID = [details objectForKey:@"order_id"];
            
            [self showLoadingScreen];
             [APIMapper getOrderDetails:[User sharedManager].userId orderID:orderID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                if (responseObject) [self showOrderDetailPageWith:responseObject];
                 [self hideLoadingScreen];
                 
             } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                  [self hideLoadingScreen];
             }];
        }
     }
}

-(void)cancelOrderWithIndex:(NSInteger)index{
    
    if (index < arrDataSource.count) {
        NSDictionary *details = arrDataSource[index];
        if ( NULL_TO_NIL([details objectForKey:@"order_id"])) {
            NSString *orderID = [details objectForKey:@"order_id"];
            
            [self showLoadingScreen];
            
             [APIMapper cancelAnOrderWith:[User sharedManager].userId orderID:orderID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 [self hideLoadingScreen];
                 [self loadAllMyOrders];
                 if (NULL_TO_NIL([responseObject objectForKey:@"text"]))
                     [[[UIAlertView alloc] initWithTitle:@"Cancel Order" message:[responseObject objectForKey:@"text"]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 
                
             } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                 [self hideLoadingScreen];
             }];
            
        }
    }

   
}

-(void)removeSignBoardWithTag:(NSInteger)index{
    
    
    if (index < arrDataSource.count) {
        NSDictionary *details = arrDataSource[index];
        if ( NULL_TO_NIL([details objectForKey:@"order_id"])) {
            NSString *orderID = [details objectForKey:@"order_id"];
            
            WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
            browser.strTitle = @"PAYMENT";
            browser.isPayment = true;
            browser.strURL =[NSString stringWithFormat:@"%@payment_gateway_all.php?page=removal&user_id=%@&order_id=%@&device=mobile",ExternalWebPageURL,[User sharedManager].userId,orderID];
            [[self navigationController]pushViewController:browser animated:YES];
            
            /*
            
            [APIMapper removeSignBoradWith:[User sharedManager].userId orderID:orderID success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [self hideLoadingScreen];
                [self loadAllMyOrders];
                if (NULL_TO_NIL([responseObject objectForKey:@"text"]))
                    [[[UIAlertView alloc] initWithTitle:@"Remove Sign Board" message:[responseObject objectForKey:@"text"]  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            } failure:^(AFHTTPRequestOperation *task, NSError *error) {
                [self hideLoadingScreen];
            }];*/
            
            
        }
    }

}

-(void)showOrderDetailPageWith:(NSDictionary*)details{
    
    OrderDetailsViewController *orderDetails =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForOrderDetails];
    orderDetails.showBackBtn = true;
    [[self navigationController]pushViewController:orderDetails animated:YES];
    [orderDetails showOrderDetailsWithInfo:details];

}
-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"Loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}



-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
