//
//  ProductCollectionViewCell.m
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import "TemplateCollectionViewCell.h"


@implementation TemplateCollectionViewCell

-(void)setUpIndexPathWithRow:(NSInteger)row section:(NSInteger)section;{
    
    self.row = row;
    self.section = section;
}

-(void)setUp{
    
    _vwShare.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    _vwCart.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    
    _starRatingImage.starImage = [UIImage imageNamed:@"RatingStarGrey.png"];
    _starRatingImage.starHighlightedImage = [UIImage imageNamed:@"RatingStarFilled.png"];
    _starRatingImage.maxRating = 5.0;
    _starRatingImage.horizontalMargin = 0;
    _starRatingImage.editable = NO;
    _starRatingImage.displayMode=EDStarRatingDisplayHalf;
}


-(IBAction)cartButtonApplied{
    
    [[self delegate] cartButtonAppliedFromPoistion:self.row column:self.section];
    [UIView animateWithDuration:.1 animations:^{
        _vwCart.layer.backgroundColor = [UIColor getThemeColor].CGColor;
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.1 animations:^{
            _vwCart.layer.backgroundColor = [UIColor clearColor].CGColor;
        } completion:^(BOOL finished) {
            
        }];
        
    }];

    
}

-(IBAction)deleteTemplate{
    
    if ([_delegate respondsToSelector:@selector(deleteButtonApplied:column:)])
        [_delegate deleteButtonApplied:self.row column:self.section];
    
}
-(IBAction)shareButtonApplied{
    
    
    [UIView animateWithDuration:.1 animations:^{
        _vwShare.layer.backgroundColor = [UIColor getThemeColor].CGColor;
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.1 animations:^{
            _vwShare.layer.backgroundColor = [UIColor clearColor].CGColor;
        } completion:^(BOOL finished) {
            
        }];

    }];
    
    [[self delegate] shareButtonAppliedFromPoistion:self.row column:self.section];

}

-(IBAction)templateClicked:(id)sender{
    
    [[self delegate]getSelectedProductFromPosition:self.row column:self.section];
}
@end
