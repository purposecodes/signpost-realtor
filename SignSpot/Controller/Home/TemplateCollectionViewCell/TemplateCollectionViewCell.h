//
//  ProductCollectionViewCell.h
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"

@protocol ProductCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Close' Button.
 */

-(void)getSelectedProductFromPosition:(NSInteger)row column:(NSInteger)column;
-(void)cartButtonAppliedFromPoistion:(NSInteger)row column:(NSInteger)column;
-(void)shareButtonAppliedFromPoistion:(NSInteger)row column:(NSInteger)column;
-(void)deleteButtonApplied:(NSInteger)row column:(NSInteger)column;

/*!
 *This method is invoked when user selects a country.The selected Country Details sends back to Registration page
 */

@end



@interface TemplateCollectionViewCell : UICollectionViewCell{
    
}

@property (nonatomic, weak) IBOutlet UIView *vwBackground;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic, weak) IBOutlet UILabel *lblProductName;
@property (nonatomic,weak)  id<ProductCellDelegate>delegate;
@property (nonatomic,weak) IBOutlet UIImageView *templateImage;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic,weak) IBOutlet UILabel *lblRate;

@property (nonatomic,weak) IBOutlet  UIView *vwShare;
@property (nonatomic,weak) IBOutlet  UIView *vwCart;
@property (nonatomic,weak) IBOutlet  EDStarRating *starRatingImage;

@property (nonatomic,weak) IBOutlet  NSLayoutConstraint *trailingConstraint;

-(void)setUpIndexPathWithRow:(NSInteger)row section:(NSInteger)section;
-(void)setUp;



@end
