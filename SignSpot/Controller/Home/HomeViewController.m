
//
//  HomeViewController.m
//  SignSpot
//
//  Created by Purpose Code on 12/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

static NSString *CollectionViewCellIdentifier = @"TemplateCollectionViewCellIentifier";

#define kPadding                10
#define kDefaultNumberOfCells   1
#define kCellHeight             250
#define kProductCellHeight      250
#define kProductCellWidth       250
#define kHeightForHeader        45
#define kHeightForFooter        5
#define kDefaultNumberOfCells   1

#import "HomeViewController.h"
#import "Constants.h"
#import "TemplateDetailViewController.h"
#import "MyOrderListViewController.h"
#import "MyAccountViewController.h"
#import "NotificationsListingViewController.h"
#import "UserCustomTemplateListViewController.h"
#import "TemplateCollectionViewCell.h"
#import "TemplateListingHeaderView.h"
#import "MenuViewController.h"
#import "WebBrowserViewController.h"


@interface HomeViewController ()<SWRevealViewControllerDelegate,ProductCellDelegate>{
    
    IBOutlet UICollectionView *collectionView;
    IBOutlet UIView *vwNavigation;
    IBOutlet UIButton *btnSlideMenu;
    IBOutlet UILabel *lblbadge;
    IBOutlet UILabel *lblNotificationCount;
    IBOutlet UIView *vwOverLay;
    
    BOOL isPageRefresing;
    BOOL isDataAvailable;
    UIRefreshControl *refreshControl;
    
    IBOutlet UIView *vwPaginationPopUp;
    IBOutlet UILabel *lblTitle;
    IBOutlet NSLayoutConstraint *paginationBottomConstraint;
    NSMutableArray *arrTemplates;
    NSInteger totalPages;
    NSInteger currentPage;
    NSInteger totalCount;
    

  
}

@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUp];
    [self customSetup];
    [self getAllProductsByPagination:NO withPageNumber:currentPage];
    
     
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self performSelector:@selector(updateCartAndNotification) withObject:self afterDelay:0.1];

    
}

-(void)updateCartAndNotification{
    
    lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
    lblbadge.hidden = false;
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }
    
    lblNotificationCount.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].notificationCount];
    lblNotificationCount.hidden = false;
    if ([User sharedManager].notificationCount <= 0) {
        lblNotificationCount.hidden = true;
    }
}


-(void)setUp{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBarHidden = true;
    
    lblbadge.layer.cornerRadius = 10.0;
    lblbadge.layer.borderWidth = 1.f;
    lblbadge.layer.borderColor = [UIColor clearColor].CGColor;
    lblbadge.clipsToBounds = YES;
    lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
    lblbadge.hidden = false;
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }
    
    lblNotificationCount.layer.cornerRadius = 10.0;
    lblNotificationCount.layer.borderWidth = 1.f;
    lblNotificationCount.layer.borderColor = [UIColor clearColor].CGColor;
    lblNotificationCount.clipsToBounds = YES;
    lblNotificationCount.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].notificationCount];
    lblNotificationCount.hidden = false;
    if ([User sharedManager].notificationCount <= 0) {
        lblNotificationCount.hidden = true;
    }
    currentPage = 1;
    arrTemplates = [NSMutableArray new];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [collectionView addSubview:refreshControl];
    collectionView.alwaysBounceVertical = YES;
    collectionView.hidden = true;

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Methods for  Getting All Templates by Pagination as well as Intial load.


-(void)getAllProductsByPagination:(BOOL)isPagination withPageNumber:(NSInteger)pageNumber{
    
    if (!isPagination) {
        [self showLoadingScreen];
    }
    
    [APIMapper getAllProductsWithUserID:[User sharedManager].userId pageNumber:[NSString stringWithFormat:@"%ld",(long)pageNumber]  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        collectionView.hidden = false;
        isPageRefresing = NO;
        [self getTemplatesFromResponds:responseObject];
        [collectionView reloadData];
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        collectionView.hidden = false;
        isDataAvailable = false;
        isPageRefresing = NO;
        [self hideLoadingScreen];
        [self hidePaginationPopUp];
        [collectionView reloadData];
    }];
}

-(void)getTemplatesFromResponds:(NSDictionary*)responseObject{
    
    isDataAvailable = true;
    [self updateSharedManagerWith:responseObject];
    if ( NULL_TO_NIL([responseObject objectForKey:@"templates"])) {
        NSArray *templateDetails = [responseObject objectForKey:@"templates"];
        if ([templateDetails count]){
                for (NSDictionary *dict in templateDetails) {
                    [arrTemplates addObject:dict];
                }
            }
    }
    if ( NULL_TO_NIL([responseObject objectForKey:@"header"])) {
        NSDictionary *header = [responseObject objectForKey:@"header"];
        if (NULL_TO_NIL([header objectForKey:@"pageCount"])) totalPages = [[header objectForKey:@"pageCount"] integerValue];
        if (NULL_TO_NIL([header objectForKey:@"currentPage"])) currentPage = [[header objectForKey:@"currentPage"] integerValue];
        if (NULL_TO_NIL([header objectForKey:@"currentPage"])) totalCount = [[header objectForKey:@"totalCount"] integerValue];
        
    }
    if (!arrTemplates.count){
        isDataAvailable = false;
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    }else{
        UINib *cellNib = [UINib nibWithNibName:@"TemplateCollectionViewCell" bundle:nil];
        [collectionView registerNib:cellNib forCellWithReuseIdentifier:CollectionViewCellIdentifier];
        
    }
    
}

-(void)updateSharedManagerWith:(NSDictionary*)responseObject{
    
    // Update cart count for the user
    
    if ( NULL_TO_NIL([responseObject objectForKey:@"cartcount"]))[User sharedManager].cartCount = [[responseObject objectForKey:@"cartcount"] integerValue];
    lblbadge.text = [NSString stringWithFormat:@"%ld",(long)[User sharedManager].cartCount];
    [Utility saveUserObject:[User sharedManager] key:@"USER"];
    lblbadge.hidden = false;
    if ([User sharedManager].cartCount <= 0) {
        lblbadge.hidden = true;
    }
}

-(void)refreshData{
    
    if (isPageRefresing){
        [refreshControl endRefreshing];
        return;
    }
    [self showLoadingScreen];
    currentPage = 1;
    [APIMapper getAllProductsWithUserID:[User sharedManager].userId pageNumber:[NSString stringWithFormat:@"%ld",(long)currentPage]  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        isPageRefresing = NO;
        [arrTemplates removeAllObjects];
        [self getTemplatesFromResponds:responseObject];
        [collectionView reloadData];
        [refreshControl endRefreshing];
        [self hideLoadingScreen];
        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        isPageRefresing = NO;
        isDataAvailable = false;
        [refreshControl endRefreshing];
        [collectionView reloadData];
        [self hideLoadingScreen];
    }];
    
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)_collectionView numberOfItemsInSection:(NSInteger)section
{
    if (!isDataAvailable) return kDefaultNumberOfCells;
    return arrTemplates.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)_collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!isDataAvailable) {
        
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        return cell;
        
    }
    TemplateCollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    cell.vwShare.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    cell.vwCart.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    cell.delegate = self;
    [cell setUp];
    cell.vwBackground.layer.borderWidth = 1.f;
    cell.trailingConstraint.constant = 10;
    cell.vwBackground.layer.borderColor = [UIColor getSeperatorColor].CGColor;
    [cell setUpIndexPathWithRow:indexPath.row section:indexPath.section];
    if (indexPath.row < arrTemplates.count) {
        NSDictionary *template = arrTemplates[indexPath.row];
        if (NULL_TO_NIL([template objectForKey:@"signtemplate_title"]))
            cell.lblProductName.text = [NSString stringWithFormat:@"  %@",[[template objectForKey:@"signtemplate_title"] uppercaseString]];
        cell.starRatingImage.rating = [[template objectForKey:@"rating"] floatValue];
        cell.lblRate.text = [NSString stringWithFormat:@"(%@)",[template objectForKey:@"total_review"]];
        if ([template objectForKey:@"signtemplate_image"] && [[template objectForKey:@"signtemplate_image"] length]){
            [cell.activityIndicator startAnimating];
            [cell.templateImage sd_setImageWithURL:[NSURL URLWithString:[template objectForKey:@"signtemplate_image"]]
                                  placeholderImage:[UIImage imageNamed:@"NoImage.png"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                             [cell.activityIndicator stopAnimating];
                                             
                                         }];
            
        }}
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSInteger columns = 2;
    if (!isDataAvailable)
        columns = 1;
    
    float percentage = 80;
    float width = _collectionView.frame.size.width / columns;
    float delta = 43;
    float height = (width * percentage) / 100 + delta;
    return CGSizeMake(width, height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0; // This is the minimum inter item spacing, can be more
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)_collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        TemplateListingHeaderView *headerView = [_collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerView.lblCategoryTitle.text = [NSString stringWithFormat:@"You have %lu Templates",(unsigned long)totalCount];
        reusableview = headerView;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)_collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (!isDataAvailable) return CGSizeZero;
    return CGSizeMake(_collectionView.bounds.size.width, 45);
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0,0, 0);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    /**! Pagination call !**/
    
    if(collectionView.contentOffset.y >= (collectionView.contentSize.height - collectionView.bounds.size.height)) {
        
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            NSInteger nextPage = currentPage ;
            nextPage += 1;
            if (nextPage  <= totalPages) {
                isPageRefresing = YES;
                [self showPaginationPopUp];
                [self getAllProductsByPagination:YES withPageNumber:nextPage];
            }
            
        }
    }
    
}

#pragma mark - Productcell Delegate Methods

-(void)getSelectedProductFromPosition:(NSInteger)row column:(NSInteger)column{
    
    if (row < arrTemplates.count) {
        TemplateDetailViewController *templateDetailView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ProductDetailsStoryBoard Identifier:StoryBoardIdentifierForTemplateDetailView];
        NSDictionary *templateInfo = arrTemplates[row];
        templateDetailView.arrTemplates = arrTemplates;
        templateDetailView.startingIndex = row;
        templateDetailView.strCategoryName = [templateInfo objectForKey:@"category_title"];
        [[self navigationController]pushViewController:templateDetailView animated:YES];
        
    }
}




/**! When user tapped  Actions From Sliding Menu  list  !**/

-(void)showSelectedCategoryDetailsFromMenuList:(NSString*)title{
    
    if ([title isEqualToString:@"PROFILE"]) [self showMyAccount];
    if ([title isEqualToString:@"ORDERS"]) [self showMyOrders];
    if ([title isEqualToString:@"MY TEMPLATES"]) [self showAllUserTemplates];
    if ([title isEqualToString:@"NOTIFICATIONS"]) [self showAllNotifications:nil];
    if ([title isEqualToString:@"LOGOUT"]) [self logouUser];
    if ([title isEqualToString:@"HELP"]) [self showHelpPage];
    if ([title isEqualToString:@"PRIVACY POLICY"]) [self showPrivacyPolicy];
    if ([title isEqualToString:@"TERMS OF SERVICE"]) [self showTermsOfSerivice];
    
}

-(IBAction)showAllNotifications:(id)sender{
    
    NotificationsListingViewController *notification =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForNotificationsListing];
    [[self navigationController]pushViewController:notification animated:YES];

}

-(void)showAllUserTemplates{
    
    UserCustomTemplateListViewController *templateListView =  [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:ProductDetailsStoryBoard Identifier:StoryBoardIdentifierForUserTemplates];
    templateListView.strCategoryTitle = @"MY TEMPLATES";
    [[self navigationController]pushViewController:templateListView animated:YES];
}

-(void)showMyAccount{
    
    MyAccountViewController *myAccount = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForMyAccount];
    [[self navigationController]pushViewController:myAccount animated:YES];
}

-(void)showMyOrders{
    
    MyOrderListViewController *order = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:CustomizeSignStoryBoard Identifier:StoryBoardIdentifierForMyOrders];
    [[self navigationController]pushViewController:order animated:YES];
}

-(void)showShareMenu{
    
}

-(void)showHelpPage{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    browser.strTitle = @"HELP";
    browser.strURL =[NSString stringWithFormat:@"%@help.php",ExternalWebPageURL];
    [[self navigationController]pushViewController:browser animated:YES];
}

-(void)showPrivacyPolicy{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    browser.strTitle = @"PRIVACY POLICY";
    browser.strURL = [NSString stringWithFormat:@"%@privacy-policy.php",ExternalWebPageURL];
    [[self navigationController]pushViewController:browser animated:YES];
    
}
-(void)showTermsOfSerivice{
    
    WebBrowserViewController *browser = [UIStoryboard get_ViewControllerFromStoryboardWithStoryBoardName:MyAccountStoryBoard Identifier:StoryBoardIdentifierForWebBrowser];
    browser.strTitle = @"TERMS OF SERVICE";
    browser.strURL = [NSString stringWithFormat:@"%@terms.php",ExternalWebPageURL];
    [[self navigationController]pushViewController:browser animated:YES];
}

-(void)logouUser{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Confirm Logout"
                                  message:@"Logout from the app ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             [self clearUserSessions];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil]; 
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)clearUserSessions{
    
   [self.navigationController popToRootViewControllerAnimated:NO];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (defaults && [defaults objectForKey:@"USER"])
        [defaults removeObjectForKey:@"USER"];
    [delegate checkUserStatus];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
    if ( revealViewController )
    {
        [btnSlideMenu addTarget:self.revealViewController action:@selector(revealToggle:)forControlEvents:UIControlEventTouchUpInside];
        [vwOverLay addGestureRecognizer: self.revealViewController.panGestureRecognizer];
       
    }
    
    
}

#pragma mark - Slider View Setup and Delegates Methods

- (void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position{
    
    UIViewController *vc = (MenuViewController*)revealController.rearViewController;
    if ([vc isKindOfClass:[MenuViewController class]]) {
        MenuViewController *menu = (MenuViewController*)vc;
        [[menu tableView]reloadData];
    }
    if (position == FrontViewPositionRight) {
        [self setVisibilityForOverLayIsHide:NO];
    }else{
        [self setVisibilityForOverLayIsHide:YES];
    }
    
}
-(IBAction)hideSlider:(id)sender{
    [self.revealViewController revealToggle:nil];
}

-(void)setVisibilityForOverLayIsHide:(BOOL)isHide{
    
    if (isHide) {
        [UIView transitionWithView:vwOverLay
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            vwOverLay.alpha = 0.0;
                        }
                        completion:^(BOOL finished) {
                            
                            vwOverLay.hidden = true;
                        }];

        
    }else{
        
        vwOverLay.hidden = false;
        [UIView transitionWithView:vwOverLay
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            vwOverLay.alpha = 0.7;
                        }
                        completion:^(BOOL finished) {
                            
                        }];

    }
}


#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Call whatever function you need to visually restore
    [self customSetup];
}


-(void)showPaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hidePaginationPopUp{
    
    [self.view layoutIfNeeded];
    paginationBottomConstraint.constant = -40;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}


-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"loading...";
    hud.removeFromSuperViewOnHide = YES;
    
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}

@end
