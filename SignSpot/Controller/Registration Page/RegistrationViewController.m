//
//  RegistrationViewController.m
//  SignSpot
//
//  Created by Purpose Code on 10/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#define StatusSucess 201

#import "RegistrationViewController.h"
#import "RegistrationPageCustomTableViewCell.h"
#import "Constants.h"
#import "LocationListView.h"


typedef enum{
    
    eFieldName = 0,
    eFieldEmail = 1,
    eFieldCountry = 2,
    eFieldState = 3,
    eFieldCity = 4,
    eFieldPhoneNumber = 5,
    eFieldPasword = 6,
    eFieldConfirmPasword = 7,
    
    
}ERegistrationField;



#define kTotalFields        8;
#define kCellHeight         60;


@interface RegistrationViewController () <LocationSelectionDelegate>{
    
    IBOutlet UITableView *tableView;
    IBOutlet UIView *vwBackground;
    ERegistrationField registrationField;
    NSString *name;
    NSString *email;
    NSString *country;
    NSString *countryID;
    NSString *stateID;
    NSString *state;
    NSString *cityID;
    NSString *city;
    NSString *phoneNumber;
    NSString *password;
    NSString *confirmPassword;
    NSInteger indexForTextFieldNavigation;
    NSInteger totalRequiredFieldCount;
    
    LocationListView *locationsListView;
    CountryListView *countryListView;
    UIView *inputAccView;
}

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    
    // Do any additional setup after loading the view.
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    name = [NSString new];
    email = [NSString new];
    country = [NSString new] ;
    phoneNumber = [NSString new];
    password = [NSString new];
    confirmPassword = [NSString new];
    countryID = [NSString new];
    stateID = [NSString new];
    state = [NSString new];
    city = [NSString new];
    cityID = [NSString new];
    totalRequiredFieldCount = kTotalFields;
    country = @"United States";
    countryID = @"231";
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return kTotalFields;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    RegistrationPageCustomTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil)
        cell = [[RegistrationPageCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    cell.btnCountryDropDown.hidden = true;
    cell.dropDownIcon.hidden = true;
    cell.entryField.clearButtonMode = UITextFieldViewModeWhileEditing;
    cell.entryField.keyboardType = UIKeyboardTypeDefault;
    cell.entryField.secureTextEntry = false;
    
    NSInteger row = indexPath.row;
    switch (row) {
        case eFieldName:
            cell.entryField.placeholder = @"Name";
            cell.iconView.image = [UIImage imageNamed:@"UserIcon.png"];
            cell.entryField.tag = eFieldName;
            cell.entryField.text = name;
            break;
            
        case eFieldEmail:
             cell.entryField.placeholder = @"Email";
             cell.iconView.image = [UIImage imageNamed:@"EmailIcon.png"];
             cell.entryField.tag = eFieldEmail;
             cell.entryField.text = email;
             break;
            
        case eFieldCountry:
             cell.entryField.placeholder = @"Country";
             cell.iconView.image = [UIImage imageNamed:@"CountryIcon.png"];
             cell.entryField.tag = eFieldCountry;
             cell.entryField.clearButtonMode = UITextFieldViewModeNever;
             cell.entryField.text = country;
             break;
            
        case eFieldState:
            cell.entryField.placeholder = @"State";
            cell.iconView.image = [UIImage imageNamed:@"StateIcon.png"];
            cell.entryField.tag = eFieldCountry;
            cell.entryField.clearButtonMode = UITextFieldViewModeNever;
            cell.entryField.text = state;
            break;
            
        case eFieldCity:
            cell.entryField.placeholder = @"City";
            cell.iconView.image = [UIImage imageNamed:@"CityIcon.png"];
            cell.entryField.tag = eFieldCountry;
            cell.entryField.clearButtonMode = UITextFieldViewModeNever;
            cell.entryField.text = city;
            break;

        case eFieldPhoneNumber:
             cell.entryField.placeholder = @"Phone Number";
             cell.iconView.image = [UIImage imageNamed:@"PhoneIcon.png"];
             cell.entryField.tag = eFieldPhoneNumber;
             cell.entryField.text = phoneNumber;
             cell.entryField.keyboardType = UIKeyboardTypeNumberPad;
             break;
            
        case eFieldPasword:
             cell.entryField.placeholder = @"Password";
             cell.iconView.image = [UIImage imageNamed:@"PasswordIcon.png"];
             cell.entryField.tag = eFieldPasword;
             cell.entryField.text = password;
             cell.entryField.secureTextEntry = true;
            break;
            
        case eFieldConfirmPasword:
            cell.entryField.placeholder = @"Confirm Password";
            cell.iconView.image = [UIImage imageNamed:@"PasswordIcon.png"];
            cell.entryField.tag = eFieldConfirmPasword;
            cell.entryField.text = confirmPassword;
            cell.entryField.secureTextEntry = true;
            break;

        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

#pragma mark - TextField Delegates


#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        RegistrationPageCustomTableViewCell *cell = (RegistrationPageCustomTableViewCell*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.entryField];
    }
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    // activeTextField = textField;
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        indexForTextFieldNavigation = indexPath.row;
    }
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    
     /*! For Country listing popup !*/
    if (indexForTextFieldNavigation == eFieldCountry) {
        return NO;
    }
     /*! For State listing popup !*/
    if (indexForTextFieldNavigation == eFieldState) {
        [self showAllStatesUnderCountry];
        return NO;
    }
     /*! For City listing popup !*/
    if (indexForTextFieldNavigation == eFieldCity) {
        
        [self showAllCitiesUnderState];
        return NO;
    }
 
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL allowed = YES;
    // Prevent invalid character input, if keyboard is numberpad
    if (textField.keyboardType == UIKeyboardTypeNumberPad)
    {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            allowed =  NO;
        }
    }
    
    return allowed;
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Registration Methods

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 0.8];
    
    UIButton *btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    [btnPrev setFrame: CGRectMake(0.0, 1.0, 80.0, 38.0)];
    [btnPrev setTitle: @"PREVIOUS" forState: UIControlStateNormal];
    [btnPrev setBackgroundColor: [UIColor getHeaderOffBlackColor]];
    btnPrev.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    btnPrev.layer.cornerRadius = 5.f;
    btnPrev.layer.borderWidth = 1.f;
    btnPrev.layer.borderColor = [UIColor clearColor].CGColor;
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 1.0f, 80.0f, 38.0f)];
    [btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor getHeaderOffBlackColor]];
    btnNext.layer.cornerRadius = 5.f;
    btnNext.layer.borderWidth = 1.f;
    btnNext.layer.borderColor = [UIColor clearColor].CGColor;
    btnNext.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 1.0f, 85.0f, 38.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor clearColor].CGColor;
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}

-(void)gotoPrevTextfield{
    
    if (indexForTextFieldNavigation - 1 < 0) indexForTextFieldNavigation = 0;
    else{
        indexForTextFieldNavigation -= 1;
        if (indexForTextFieldNavigation == eFieldCountry) {
            indexForTextFieldNavigation -= 1;
        }
    }
    
    [self gotoTextField];
    
}

-(void)gotoNextTextfield{
    
    if (indexForTextFieldNavigation + 1 < totalRequiredFieldCount){
        indexForTextFieldNavigation += 1;
        if (indexForTextFieldNavigation == eFieldCountry) {
            indexForTextFieldNavigation += 1;
        }
    }
    
    [self gotoTextField];
}

-(void)gotoTextField{
    
    RegistrationPageCustomTableViewCell *nextCell = (RegistrationPageCustomTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
    if (!nextCell) {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        nextCell = (RegistrationPageCustomTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
        
    }
    [nextCell.entryField becomeFirstResponder];
    
}

-(void)doneTyping{
    [self.view endEditing:YES];
    
}


-(void)getTextFromField:(UITextField*)textField{
   
    NSString *string = textField.text;
    NSInteger tag = textField.tag;
    switch (tag) {
        case eFieldName:
            name = string;
            break;
        case eFieldEmail:
            email = string;
            break;
        case eFieldPhoneNumber:
            phoneNumber = string;
            break;
        case eFieldCountry:
            //[self showAllCountries];
            break;
        case eFieldPasword:
            password = string;
            break;
        case eFieldConfirmPasword:
            confirmPassword = string;
            break;
        default:
            break;
    }
   
}


-(IBAction)tapToRegister:(id)sender{
    
    [self.view endEditing:YES];
    [self checkAllFieldsAreValid:^{
        
        [self showLoadingScreen];
        [APIMapper registerUserWithName:name userEmail:email phoneNumber:phoneNumber countryID:countryID stateID:stateID cityID:cityID userPassword:password
                                success:^(AFHTTPRequestOperation *operation, id responseObject){
            
                                    NSDictionary *responds = (NSDictionary*)responseObject;
                                    NSInteger statusCode = [[responds objectForKey:@"code"] integerValue];
                                    if (statusCode == StatusSucess)[self gotoLoginPage];
                                    if ( NULL_TO_NIL( [responds objectForKey:@"text"]))
                                         [self showAlertWithMessage:[responds objectForKey:@"text"] title:@"SignUp"];
                                    [self hideLoadingScreen];
                           
                                }
         
                                failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                    
                                 [self showAlertWithMessage:[error localizedDescription] title:@"SignUp"];
                                 [self hideLoadingScreen];
                                    
                                }];

        
    } failure:^(NSString *error) {
        
        // Field validation error block
        
        [self showAlertWithMessage:error title:@"SignUp"];
        
    }];
    

}

-(void)showLoadingScreen{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = @"loading...";
    hud.removeFromSuperViewOnHide = YES;

}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}
-(void)checkAllFieldsAreValid:(void (^)())success failure:(void (^)(NSString *error))failure{
    
    NSString *errorMessage;
    BOOL isValid = false;
    if ((name.length) && (email.length) && (country.length) && (phoneNumber.length) && (confirmPassword.length) > 0){
        isValid = [email isValidEmail] ? YES : NO;
        errorMessage = @"Please enter a valid Email address";
        if (![password isEqualToString:confirmPassword]) {
            isValid = false;
            errorMessage = @"Password doesn't match.";
        }
        
    }else{
        
        errorMessage = @"Please fill all the fields";
    }
    if (isValid)success();
    else failure(errorMessage);

}

-(void)showAlertWithMessage:(NSString*)alertMessage title:(NSString*)alertTitle{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)gotoLoginPage{
    [[self navigationController]popViewControllerAnimated:YES];
    [self.view endEditing:YES];
}



#pragma mark - Country , State , State listing popups



-(void)showAllStatesUnderCountry{
    
    [self.view endEditing:YES];
    if (!locationsListView) {
        
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [locationsListView setUp];
    [locationsListView loadLocationsOn:eStateList];
    
}

-(void)showAllCitiesUnderState{
    
    [self.view endEditing:YES];
    BOOL isIDAvailable = true;
    
    if (stateID.length <= 0) {
        isIDAvailable = false;
    }
    
    if (!isIDAvailable){
        [[[UIAlertView alloc] initWithTitle:@"City" message:@"Please choose a State." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    if (!locationsListView) {
        
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [locationsListView setUp];
    locationsListView.selectedID = stateID;
    [locationsListView loadLocationsOn:eCityList];
    
}

-(void)closeLocationListingPopUpAfterADelay:(float)delay{
    
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [locationsListView removeFromSuperview];
        locationsListView = nil;
    }];
}

-(void)sendSelectedLocationsWith:(NSDictionary*)locationInfo type:(eLocationList)locType{
    
    if (locType == eStateList) {
        
        if ( NULL_TO_NIL([locationInfo objectForKey:@"name"])) {
            stateID = [locationInfo objectForKey:@"id"];
            state = [locationInfo objectForKey:@"name"];
            cityID = @"";
            city = @"";
        }
    }
    
    if (locType == eCityList) {
        
        if ( NULL_TO_NIL([locationInfo objectForKey:@"name"])) {
            cityID = [locationInfo objectForKey:@"id"];
            city = [locationInfo objectForKey:@"name"];
        }
    }
    
    [tableView reloadData];
}




-(IBAction)goBack:(id)sender{
    
    [[self navigationController]popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
