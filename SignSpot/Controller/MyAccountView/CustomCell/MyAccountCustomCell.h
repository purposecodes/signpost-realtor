//
//  CustomCellForType.h
//  SignSpot
//
//  Created by Purpose Code on 18/05/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyAccountCustomCellDelegate <NSObject>

/*!
 *This method is invoked when user taps the 'Share' Button.
 */

-(void)shareButtonTappedWithTag:(NSInteger)tag;

/*!
 *This method is invoked when user taps the 'Delete' Button.
 */


-(void)deleteSelectedCellWithTag:(NSInteger)tag;



@end


@interface MyAccountCustomCell : UITableViewCell{
    
   

}

@property (nonatomic,weak) IBOutlet  UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet  UITextField *txtField;


@property (nonatomic,weak)  id<MyAccountCustomCellDelegate>delegate;
-(void)setUp;


@end
