//
//  MyAccountViewController.m
//  SignSpot
//
//  Created by Purpose Code on 10/06/16.
//  Copyright © 2016 Purpose Code. All rights reserved.
//

typedef enum{
    
    eName = 0,
    eEmail = 1,
    eMobile = 2,
    eCountry = 3,
    eState  = 4,
    eCity  = 5,
    
    eOldPaswrd = 6,
    eNewPaswrd = 7,
    eConfirmPwd = 8
    
    
}EFieldInfo;

#define kCellHeight             60;
#define kHeightForHeader        40;
#define kHeightForFooter        80;
#define kMaxReviewLength        150
#define kMaxReviewTitleLength   50

#define kCellCountForFirstTab   6
#define kCellCountForSecondTab  3

#define kCountryID              @"CountryID"
#define kStateID                @"StateID"

#define kPROFILE                @"PROFILE"
#define kCONFIRMPWD             @"CONFIRMPWD"

#define kSuccessCode            200


#import "MyAccountViewController.h"
#import "Constants.h"
#import "CountryListView.h"
#import "MyAccountCustomCell.h"
#import "LocationListView.h"

@interface MyAccountViewController ()<CountrySelectionDelegate,LocationSelectionDelegate>{
    
    
    IBOutlet UITableView *tableView;
    IBOutlet UISegmentedControl *segmentControll;
    UIView *inputAccView;
    NSMutableDictionary *dictProfileInfo;
    NSMutableDictionary *dictPwdInfo;
    NSInteger indexForTextFieldNavigation;
    NSInteger totalCellCount;
    NSInteger totalRequiredProfileCount;
    NSInteger totalRequiredPwdCount;
    
    CountryListView *countryListView;
    UITextField *activeTextField;
    LocationListView *locationsListView;
}

@end

@implementation MyAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    [self setUpUserDetails];
    [self setUpFooter];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setUp{
    
    totalRequiredProfileCount = 6;
    totalRequiredPwdCount = 3;
    dictProfileInfo = [NSMutableDictionary new];
    dictPwdInfo = [NSMutableDictionary new];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.allowsSelection = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    segmentControll.tintColor = [UIColor getHeaderOffBlackColor];
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:CommonFont size:13], NSFontAttributeName,
                                 [UIColor blackColor], NSForegroundColorAttributeName, nil];
    NSDictionary *attributes2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:CommonFont size:13], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [segmentControll setTitleTextAttributes:attributes1 forState:UIControlStateNormal];
    [segmentControll setTitleTextAttributes:attributes2 forState:UIControlStateSelected];
   }

-(void)setUpFooter{
    

    UIButton *btnSend = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:btnSend];
    btnSend.translatesAutoresizingMaskIntoConstraints = NO;
    [btnSend addTarget:self action:@selector(submitClicked:)
      forControlEvents:UIControlEventTouchUpInside];
    btnSend.layer.borderColor = [UIColor clearColor].CGColor;
    btnSend.titleLabel.font = [UIFont fontWithName:CommonFont size:16];
    [btnSend setTitle:@"UPDATE" forState:UIControlStateNormal];
    [btnSend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSend setBackgroundColor:[UIColor getThemeColor]];
    
    NSArray *horizontalConstraints =[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[btnSend]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnSend)];
    [self.view addConstraints:horizontalConstraints];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:btnSend
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:-10.0]];
    
    
    [btnSend addConstraint:[NSLayoutConstraint constraintWithItem:btnSend
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeHeight
                                                       multiplier:1.0
                                                         constant:45]];

    
}

-(void)setUpUserDetails{
    
    if ([User sharedManager].name)
        [dictProfileInfo setObject:[User sharedManager].name forKey:[NSNumber numberWithInteger:eName]];
    
    if ([User sharedManager].email)
        [dictProfileInfo setObject:[User sharedManager].email forKey:[NSNumber numberWithInteger:eEmail]];
    
    if ([User sharedManager].phoneNumber)
        [dictProfileInfo setObject:[User sharedManager].phoneNumber forKey:[NSNumber numberWithInteger:eMobile]];
    
    if ([User sharedManager].countryID) {
        NSDictionary *countryInfo = [[NSDictionary alloc]initWithObjectsAndKeys:@"231",@"id",@"United States",@"name", nil];
        [dictProfileInfo setObject:countryInfo forKey:[NSNumber numberWithInteger:eCountry]];
    }
    
    if ([User sharedManager].stateID) {
        NSDictionary *countryInfo = [[NSDictionary alloc]initWithObjectsAndKeys:[User sharedManager].stateID,@"id",[User sharedManager].stateName,@"name", nil];
        [dictProfileInfo setObject:countryInfo forKey:[NSNumber numberWithInteger:eState]];
    }
    
    if ([User sharedManager].cityID) {
        NSDictionary *countryInfo = [[NSDictionary alloc]initWithObjectsAndKeys:[User sharedManager].cityID,@"id",[User sharedManager].cityName,@"name", nil];
        [dictProfileInfo setObject:countryInfo forKey:[NSNumber numberWithInteger:eCity]];
    }
 
}

#pragma mark - TableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = kCellCountForSecondTab;
    if (segmentControll.selectedSegmentIndex == 0) {
        count = kCellCountForFirstTab;
    }
    totalCellCount = count;
    return count;
}



- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MyAccountCustomCell";
    MyAccountCustomCell *cell = (MyAccountCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.txtField.text = @"";
    cell.txtField.keyboardType = UIKeyboardTypeDefault;
    NSInteger caseCalue;
    caseCalue = [indexPath row];
    if ([segmentControll selectedSegmentIndex] == 0){
        
        // PROFILE INFO
        
        if ( NULL_TO_NIL([dictProfileInfo objectForKey:[NSNumber numberWithInteger:caseCalue]])){
            
            if ([[dictProfileInfo objectForKey:[NSNumber numberWithInteger:caseCalue]]isKindOfClass:[NSDictionary class]]) {
                cell.txtField.text = (NSString*)[[dictProfileInfo objectForKey:[NSNumber numberWithInteger:caseCalue]]objectForKey:@"name"];
            }else{
                cell.txtField.text = (NSString*)[dictProfileInfo objectForKey:[NSNumber numberWithInteger:caseCalue]];
            }
        }

    }
        
    else{
        
        // PWD INFO
        
        caseCalue = [indexPath row] + 6;
        if ( NULL_TO_NIL([dictPwdInfo objectForKey:[NSNumber numberWithInteger:caseCalue]])){
            cell.txtField.text = (NSString*)[dictPwdInfo objectForKey:[NSNumber numberWithInteger:caseCalue]];
            
        }

    }
    
    cell.txtField.tag = caseCalue;
    
    cell = [self configureCellWithCaseValue:caseCalue cell:cell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kCellHeight;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


#pragma mark - UITextfield delegate methods


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        MyAccountCustomCell *cell = (MyAccountCustomCell*)textField.superview.superview;
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        [self getTextFromField:cell.txtField.tag data:textField.text];
    }
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self createInputAccessoryView];
    [textField setInputAccessoryView:inputAccView];
    activeTextField = textField;
    NSIndexPath *indexPath;
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero toView: tableView];
        indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
        indexForTextFieldNavigation = indexPath.row;
    }
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height );
    [tableView setContentOffset:contentOffset animated:YES];
    
    /*! For country listing popup !*/
    
    int tagForCountry = eCountry;
    if (([segmentControll selectedSegmentIndex] == 0) && (indexPath.row == tagForCountry)) {
        
        return NO;
    }
    
    if (([segmentControll selectedSegmentIndex] == 0) && (indexPath.row == eState)) {
        
        [self showAllStatesUnderCountry];
        return NO;
    }
    
    if (([segmentControll selectedSegmentIndex] == 0) && (indexPath.row == eCity)) {
        
        [self showAllCitiesUnderState];
        return NO;
    }
    
    return YES;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:tableView];
    CGPoint contentOffset = tableView.contentOffset;
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    [tableView setContentOffset:contentOffset animated:YES];
    
}

#pragma mark - Common Actions


-(MyAccountCustomCell*)configureCellWithCaseValue:(NSInteger)caseValue cell:(MyAccountCustomCell*)cell{
    
    cell.txtField.keyboardType = UIKeyboardTypeDefault;
    cell.txtField.secureTextEntry = NO;
    switch (caseValue) {
            
        case eName:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Name";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"UserIcon.png"];
            break;
           
        case eCountry:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Country";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"CountryIcon.png"];
            break;
            
        case eState:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"State";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"StateIcon.png"];
            break;
            
        case eCity:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"City";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"CityIcon.png"];
            break;
            
      case eMobile:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Mobile";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"MobileIcon.png"];
            cell.txtField.keyboardType = UIKeyboardTypeNumberPad;
            break;
            
      case eEmail:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Email";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"EmailIcon.png"];
            cell.txtField.keyboardType = UIKeyboardTypeEmailAddress;
            break;
            
     case eOldPaswrd:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Current Password";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"PasswordIcon.png"];
            cell.txtField.secureTextEntry = YES;
            break;
            
    case eNewPaswrd:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"New Password";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"PasswordIcon.png"];
            cell.txtField.secureTextEntry = YES;
            break;
            
     case eConfirmPwd:
            if (cell.txtField.text.length <= 0) {
                cell.txtField.placeholder = @"Confirm New Password";
            }
            cell.imgIcon.image = [UIImage imageNamed:@"PasswordIcon.png"];
            cell.txtField.secureTextEntry = YES;
            break;
            
        default:
            break;
    }
    
    [cell.txtField reloadInputViews];
    return cell;
    
}

-(void)createInputAccessoryView{
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0)];
    [inputAccView setBackgroundColor:[UIColor lightGrayColor]];
    [inputAccView setAlpha: 0.8];
    
    UIButton *btnPrev = [UIButton buttonWithType: UIButtonTypeCustom];
    [btnPrev setFrame: CGRectMake(0.0, 1.0, 80.0, 38.0)];
    [btnPrev setTitle: @"PREVIOUS" forState: UIControlStateNormal];
    [btnPrev setBackgroundColor: [UIColor getHeaderOffBlackColor]];
    btnPrev.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    btnPrev.layer.cornerRadius = 5.f;
    btnPrev.layer.borderWidth = 1.f;
    btnPrev.layer.borderColor = [UIColor clearColor].CGColor;
    [btnPrev addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(85.0f, 1.0f, 80.0f, 38.0f)];
    [btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
    [btnNext setBackgroundColor:[UIColor getHeaderOffBlackColor]];
    btnNext.layer.cornerRadius = 5.f;
    btnNext.layer.borderWidth = 1.f;
    btnNext.layer.borderColor = [UIColor clearColor].CGColor;
    btnNext.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnNext addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(inputAccView.frame.size.width - 85, 1.0f, 85.0f, 38.0f)];
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor getThemeColor]];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnDone.layer.cornerRadius = 5.f;
    btnDone.layer.borderWidth = 1.f;
    btnDone.layer.borderColor = [UIColor clearColor].CGColor;
    btnDone.titleLabel.font = [UIFont fontWithName:CommonFont size:14];
    [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
    
    [inputAccView addSubview:btnPrev];
    [inputAccView addSubview:btnNext];
    [inputAccView addSubview:btnDone];
}

-(void)gotoPrevTextfield{
    
    if (indexForTextFieldNavigation - 1 < 0) indexForTextFieldNavigation = 0;
    else indexForTextFieldNavigation -= 1;
    [self gotoTextField];
    
}

-(void)gotoNextTextfield{
    
    if (indexForTextFieldNavigation + 1 < totalCellCount) indexForTextFieldNavigation += 1;
    [self gotoTextField];
}

-(void)gotoTextField{
    
    MyAccountCustomCell *nextCell = (MyAccountCustomCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
    if (!nextCell) {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        nextCell = (MyAccountCustomCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexForTextFieldNavigation inSection:0]];
        
    }
    [nextCell.txtField becomeFirstResponder];
    
}

-(void)doneTyping{
    [self.view endEditing:YES];
    
}

-(void)getTextFromField:(NSInteger)type data:(NSString*)string{
    
    if (type < eOldPaswrd) {
        
        // PROFILE DETAILS
        
        if ( NULL_TO_NIL([dictProfileInfo objectForKey:[NSNumber numberWithInteger:type]]))
            [dictProfileInfo removeObjectForKey:[NSNumber numberWithInteger:type]];
        if ([string length])
            [dictProfileInfo setObject:string forKey:[NSNumber numberWithInteger:type]];
    }else{
        
        // PWD DETAILS
        
        if ( NULL_TO_NIL([dictPwdInfo objectForKey:[NSNumber numberWithInteger:type]]))
            [dictPwdInfo removeObjectForKey:[NSNumber numberWithInteger:type]];
        if ([string length])
            [dictPwdInfo setObject:string forKey:[NSNumber numberWithInteger:type]];
    }
  
}


#pragma mark - Button Actions

-(IBAction)segmentSelected:(UISegmentedControl*)segmentControl{
    
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [tableView numberOfSections])];
    [tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
    
}

-(IBAction)submitClicked:(id)sender{
    
    if ([segmentControll selectedSegmentIndex] == 0) {
        
        // VALIDATE PROFILE FIELDS
        
        NSArray *allKeys = [dictProfileInfo  allKeys];
        if (allKeys.count == totalRequiredProfileCount) {
            
            [self checkAllFieldsAreValidWithType:kPROFILE OnSuccess:^{
                
                [self updateUserProfile];
                
            } failure:^(NSString *title, NSString *message) {
                
                [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    
            }];
        }else{
            
            // Show missed field as selected by scrolling the table
            for (NSInteger i = 0; i <= totalRequiredProfileCount; i ++) {
                
                if (![dictProfileInfo objectForKey:[NSNumber numberWithInteger:i]]) {
                    
                    [self scrollTableToRow:i];
                    break;
                }
            }

        }
    }else{
        
         // VALIDATE PASSWORD FIELDS
        
        NSArray *allKeys = [dictPwdInfo  allKeys];
        if (allKeys.count == totalRequiredPwdCount) {
            
            [self checkAllFieldsAreValidWithType:kCONFIRMPWD OnSuccess:^{
                
                [self updateUserPassword];
                
            } failure:^(NSString *title, NSString *message) {
                
                [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                
            }];
        }else{
            
            for (NSInteger i = eOldPaswrd; i <= eConfirmPwd; i ++) {
                
                if (![dictPwdInfo objectForKey:[NSNumber numberWithInteger:i]]) {
            
                    NSInteger row = i - 4;
                    [self scrollTableToRow:row];
                    break;
                }
            }

        }

        
    }
    

}

-(void)scrollTableToRow:(NSInteger)row{
    
    double delayInSeconds = .2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        MyAccountCustomCell *nextCell = (MyAccountCustomCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
        if (!nextCell) {
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            nextCell = (MyAccountCustomCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row  inSection:0]];
        }
        
        [nextCell.txtField becomeFirstResponder];
    });

    
}

-(void)showAllCountries{
    
    [self.view endEditing:YES];
    if (!countryListView) {
        
        countryListView = [CountryListView new];
        [self.view addSubview:countryListView];
        countryListView.delegate = self;
        countryListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[countryListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(countryListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[countryListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(countryListView)]];
        
        countryListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            countryListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [countryListView setUp];
}

-(void)closeCountryPopUpAfterADelay:(float)delay{
    
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        countryListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [countryListView removeFromSuperview];
        countryListView = nil;
    }];
}

-(void)sendSelectedCountryDetailsToRegisterPage:(NSDictionary*)countryDetails{
    
    // CallBack when user tapped any Country from country list
    if (countryDetails && [countryDetails objectForKey:@"name"]) {
        //countryID = [countryDetails objectForKey:@"id"];
        [dictProfileInfo setObject:countryDetails forKey:[NSNumber numberWithInteger:activeTextField.tag]];
        //[dictInfo setObject:[countryDetails objectForKey:@"id"] forKey:kCountryID];
    }
    
    [tableView reloadData];
}

-(void)checkAllFieldsAreValidWithType:(NSString*)type OnSuccess:(void (^)())success failure:(void (^)(NSString *title,NSString *message))failure{
    
    if ([type isEqualToString:kPROFILE]) {
        
        if ([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eEmail]]){
            
            NSString *email = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eEmail]];
            if (![email isValidEmail]){
                failure(@"Email",@"Please enter a valid Email.");
                return;
            }
            
        }

    }else{
        
        if ([dictPwdInfo objectForKey:[NSNumber numberWithInteger:eNewPaswrd]] && [dictPwdInfo objectForKey:[NSNumber numberWithInteger:eConfirmPwd]]){
            
            NSString *pwd = [dictPwdInfo objectForKey:[NSNumber numberWithInteger:eNewPaswrd]];
            NSString *cpwd = [dictPwdInfo objectForKey:[NSNumber numberWithInteger:eConfirmPwd]];
            if (![pwd isEqualToString:cpwd]){
                failure(@"Password",@"Password doesn't match.");
                return;
            }
        }
    }
    
    success();
}

#pragma mark - Upadte Profile and Password API  Actions

-(void)updateUserProfile{
    
    NSString *name = @"";
    NSString *phone = @"";
    NSString *countryID = @"";
    NSString *stateID = @"";
    NSString *cityID = @"";
    
    if ([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eName]]) {
        name = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eName]];
    }
    if ([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eMobile]]) {
        phone = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eMobile]];
    }
    
    if ([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eCountry]]) {
        NSDictionary *info = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eCountry]];
        if ([info objectForKey:@"id"]) {
            countryID = [info objectForKey:@"id"];
        }
    }
    
    if ([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eState]]) {
        
        NSDictionary *info = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eState]];
        if ([info objectForKey:@"id"]) {
            stateID = [info objectForKey:@"id"];
        }
        
    }
    if ([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eCity]]) {
        
        NSDictionary *info = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eCity]];
        if ([info objectForKey:@"id"]) {
            cityID = [info objectForKey:@"id"];
        }
        
    }
    
    [self showLoadingScreenWithTitle:@"Updating.."];
    
    [APIMapper updateUserProfileWithName:name userID:[User sharedManager].userId userEmail:nil phoneNumber:phone countryID:countryID stateID:stateID cityID:cityID  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ( NULL_TO_NIL([responseObject objectForKey:@"header"])) {
            
            if ([[[responseObject objectForKey:@"header"] objectForKey:@"code"]integerValue] == kSuccessCode) {
                
                [[[UIAlertView alloc] initWithTitle:@"Profile" message:@"Successfully updated your Profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                [self updateUserInfoWithDetails:responseObject];
                
            }else{
                
                [[[UIAlertView alloc] initWithTitle:@"Profile" message:@"Failed to update your Profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            
        }
        [self hideLoadingScreen];

        
    } failure:^(AFHTTPRequestOperation *task, NSError *error) {
        
        [self hideLoadingScreen];
    }];
}

-(void)updateUserPassword{
    
    NSString *oldPwd;
    if ([dictPwdInfo objectForKey:[NSNumber numberWithInteger:eOldPaswrd]]) {
        oldPwd = [dictPwdInfo objectForKey:[NSNumber numberWithInteger:eOldPaswrd]];
    }
    NSString *newPwd;
    if ([dictPwdInfo objectForKey:[NSNumber numberWithInteger:eNewPaswrd]]) {
        newPwd = [dictPwdInfo objectForKey:[NSNumber numberWithInteger:eNewPaswrd]];
    }
    if ((newPwd.length) && (oldPwd.length)) {
        
        [self showLoadingScreenWithTitle:@"Updating.."];
        [APIMapper updateUserPaswordWithOldPwd:oldPwd newPwd:newPwd userID:[User sharedManager].userId Onsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([responseObject objectForKey:@"text"]) {
                
                 [[[UIAlertView alloc] initWithTitle:@"Change Password" message:[responseObject objectForKey:@"text"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            [self hideLoadingScreen];
            
        } failure:^(AFHTTPRequestOperation *task, NSError *error) {
            
             [self hideLoadingScreen];
            
        }];

    }
    
}

-(void)showLoadingScreenWithTitle:(NSString*)title{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.detailsLabelText = title;
    hud.removeFromSuperViewOnHide = YES;
}
-(void)hideLoadingScreen{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)updateUserInfoWithDetails:(NSDictionary*)userInfo{
   
    if ( NULL_TO_NIL([userInfo objectForKey:@"header"])) {
        
        NSDictionary *userDetails = [userInfo objectForKey:@"header"];
        
        if (NULL_TO_NIL([userDetails objectForKey:@"firstname"])) {
            [User sharedManager].name  = [userDetails objectForKey:@"firstname"];
        }
       
        if (NULL_TO_NIL([userDetails objectForKey:@"country_id"])) {
            [User sharedManager].countryID  = [userDetails objectForKey:@"country_id"];
        }
        if (NULL_TO_NIL([userDetails objectForKey:@"country_name"])) {
            [User sharedManager].countryName  = [userDetails objectForKey:@"country_name"];
        }
        if (NULL_TO_NIL([userDetails objectForKey:@"state_id"])) {
            [User sharedManager].stateID  = [userDetails objectForKey:@"state_id"];
        }
        if (NULL_TO_NIL([userDetails objectForKey:@"state_name"])) {
            [User sharedManager].stateName  = [userDetails objectForKey:@"state_name"];
        }
        if (NULL_TO_NIL([userDetails objectForKey:@"city_id"])) {
            [User sharedManager].cityID  = [userDetails objectForKey:@"city_id"];
        }
        if (NULL_TO_NIL([userDetails objectForKey:@"city_name"])) {
            [User sharedManager].cityName  = [userDetails objectForKey:@"city_name"];
        }
        
        if (NULL_TO_NIL([userDetails objectForKey:@"phone"])) {
            [User sharedManager].phoneNumber  = [userDetails objectForKey:@"phone"];
        }
        
        /*!............ Saving user to NSUserDefaults.............!*/
        
        [Utility saveUserObject:[User sharedManager] key:@"USER"];
    }

    
}


#pragma mark - Country , State , State listing popups



-(void)showAllStatesUnderCountry{
    
    [self.view endEditing:YES];
    if (!locationsListView) {
        
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [locationsListView setUp];
    [locationsListView loadLocationsOn:eStateList];
    
}

-(void)showAllCitiesUnderState{
    
    [self.view endEditing:YES];
    BOOL isIDAvailable = true;
    NSString *locID;
    if ( NULL_TO_NIL([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eState]])) {
        
        NSDictionary *country = [dictProfileInfo objectForKey:[NSNumber numberWithInteger:eState]];
        if ([country objectForKey:@"id"]) {
            
            locID = [country objectForKey:@"id"];
            isIDAvailable = true;
        }
    }
    
    if (!isIDAvailable){
        [[[UIAlertView alloc] initWithTitle:@"City" message:@"Please choose a State." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    if (!locationsListView) {
        
        locationsListView = [LocationListView new];
        [self.view addSubview:locationsListView];
        locationsListView.delegate = self;
        locationsListView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[locationsListView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(locationsListView)]];
        
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // animate it to the identity transform (100% scale)
            locationsListView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished){
            // if you want to do something once the animation finishes, put it here
        }];
        
    }
    
    [locationsListView setUp];
    locationsListView.selectedID = locID;
    [locationsListView loadLocationsOn:eCityList];
    
}

-(void)closeLocationListingPopUpAfterADelay:(float)delay{
    
    [UIView animateWithDuration:0.2 delay:delay options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        locationsListView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        [locationsListView removeFromSuperview];
        locationsListView = nil;
    }];
}

-(void)sendSelectedLocationsWith:(NSDictionary*)locationInfo type:(eLocationList)locType{
    
    if ( NULL_TO_NIL( [locationInfo objectForKey:@"name"])) {
        [dictProfileInfo setObject:locationInfo forKey:[NSNumber numberWithInteger:activeTextField.tag]];
    }
    
    if (locType == eStateList) {
        
        if ( NULL_TO_NIL([dictProfileInfo objectForKey:[NSNumber numberWithInteger:eCity]]))
            [dictProfileInfo removeObjectForKey:[NSNumber numberWithInteger:eCity]];
        
    }
    
    
    [tableView reloadData];
}





-(IBAction)goBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
